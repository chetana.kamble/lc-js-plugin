<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Tr:qz4>jIKKR7#maC-,/@6NCX@jJ]&kYi-q^;sq(C .76aLDQL=1nW._>2pVN)kw' );
define( 'SECURE_AUTH_KEY',  '@9&rM!4T%J45D0m)<Y:Ff{xT]Tm4[^OxmfiB.?14R8tx9{R~TtI5kWFCXhKF#1+7' );
define( 'LOGGED_IN_KEY',    'K7&hx,{te_A=k5*#KI[PlS.*`f/R@ J|-p9T|X|kP5dTmWXVM;_BWZqEEi[2-b D' );
define( 'NONCE_KEY',        '`%x(t{h8P:78&]gp~&%R+OiZ|&EC}l#1PlF:7#>BVC0|F&A,R-)z*vPDP@C)To34' );
define( 'AUTH_SALT',        'd{3E!7U<3N:8WPVKaODnxf+oQQsVpv4%n~~^$X!_-OSel20D7#L06:DLK}Yh|buH' );
define( 'SECURE_AUTH_SALT', '?57rgl&ri!=tl1Czb.3oL(;3>D;tpDoOaxp@0YgI;u_.Apg=0p+Uf~W|&F$?G_zq' );
define( 'LOGGED_IN_SALT',   '09SwZ .#%oBJt([Gx7v92LkPa`Z$ptTIuv}HkYa1EU<)X/LA/`W<svCM(z8voq#K' );
define( 'NONCE_SALT',       'S.oI5A<y0b-Se2g,F|x:*>#RT7~Ue?:6VMB>HFL &a,DTO#a%o5T=L^L(35@@-A ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'lc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
