<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<style>
body{
     background: #202020!important;
     
}

.lc-platform{
    background-color: #fecc00;
    font-size: 27px;
    font-weight: bold;
    border-radius: 100px;
    padding: 0 18px;
    line-height: 1.7em;
    white-space: nowrap;
    border-color: #000;
    color: #000!important;
    margin: 0 30px 20px 0;
    display: inline-block;
   
}
.listing-box a
{
  color: #fecc00!important;
}
.lc-platform a{
     color: #000!important;
}

.t-white{
     color: #fff!important;
     font-size: 13px;
    line-height: 1.5;
}
.listing-box
{
  padding: 50px;
}
.bla-yellow
{
  border-color: #fecc00;
  border-style: solid;
  color: aliceblue;
  border-radius: 30px;
}

.set-box{
text-align:center;
}
.logo-box{

height: 238px;
background: transparent url('<?php echo plugins_url().'/LC-JS/includes/images/Mask_Group_2.png'; ?>') 0% 0% no-repeat padding-box;
}
.lazy-logo{
position:relative;
top: 60px;
left: 45px;
width: 408px;
height: 84px;
background: transparent url('<?php echo plugins_url().'/LC-JS/includes/images/logo.png'; ?>') 0% 0% no-repeat padding-box;
opacity: 1;
}
</style>
<div class="lc-main-header">
  <div class="row">
    <div class="col-md-8 logo-box"> 
        <div class="lazy-logo"></div>
        <span class="tag-line-span">The highest-performance <span class="js-tag">JavaScript</span> charting library focusing on real-time data visualization</span>
    </div>    
  </div>
</div>  


<div class="row">
    

  <div class="col-md-12 listing-box">
  <div class="col-md-2"> </div>
    <div class="col-md-8 bla-yellow license-key-section">
            <div class="bg-key">
              <h1>Set License Key</h1>
                          
              <?php 
              $user = wp_get_current_user();
              $User_id = $user->ID;
              $prod_key1 =  get_user_meta( $User_id, 'prod_key' , true );
              if($prod_key1 != null ){
                  //$prod_key1;
                  $update_style = 'style="display:block"';
                  $submit_style = 'style="display:none"';
                  $read_only = 'disabled';
              }else{
                  $update_style = 'style="display:none"';
                  $submit_style = 'style="display:block"';
                  $read_only = '';
              }
              ?>
            
              <script>
              function set_license_key()
              {
                
                  document.getElementById("prod_key").removeAttribute("disabled");
                  //document.getElementById("prod_key").setAttribute("disabled", false);
                  jQuery('#set-sub-btn').show();
                  jQuery('#set-upd-btn').hide();
              }
              </script>
              <?php
             $meassage = '';
             if(isset($_GET['addmeassage'])&& $_GET['addmeassage']=='success')
             {
               $meassage = "License key is set successfully.";
             } 
             /*
             if($meassage != '')
             {
               
               ?>
               <div id="alert-box" class="alert  alert-box">
                 <span><?php echo $meassage; ?> </span>
                 </div>
             <?php } */
              ?>
              
            <div class="set-box"> 
             <form action="#" method="POST" id="myForm">
                   <div class="form-group"> 
                    <input type="text" id="prod_key" name="prod_key" class="prod_key" value="<?php echo $prod_key1; ?>"  style="width:75%;" <?php echo $read_only; ?> >
                    </div>
                    <div class="form-group" style="display: flex;align-items: center;justify-content: center;"> 
                
                <input type="submit" id="set-sub-btn" class="lc-platform" name="submit"   <?php echo $submit_style; ?> >
                <input type="button" id="set-upd-btn" class="lc-platform" onclick="set_license_key();" value="Edit"  <?php echo $update_style; ?> >  
                </div>
                    
                </form>
              </div>
              </div>
              <div class="col-md-12 about-license">
                  <h2>Non-commercial, Community License</h2>
                      <p>When the library is used without a license, it will run with a LightningChart JS logo on the chart. 
                      The logo is a link to the LightningChart JS product page.
                      There is a small performance drop when the chart is running without a license key compared to running with a valid license.</p>

                      <h2>Development License</h2>
                      <p>A development license is required for development in a commercial environment. 
                      The license is verified with a license server. Internet connection is required for license verification. 
                      Each developer requires own development license. See "Using a license" for how to use the development license.
                      When using a development key, the chart will just like it will in production..</p>


                      <h2>Deployment License</h2>
                      <p>When the library is used without a license, it will run with a LightningChart JS logo on the chart. 
                      A deployment license is required for commercial use. The deployment license is provided the same way as a development license. 
                      See "Using a license" for how to use the deployment license.
                      When using a deployment license, the chart will render without the LightningChart JS logo. 
                      The deployment license supports a "Deployment Test" domain. If the domain that the library is currently running in matches the deployment test domain specified with the deployment license, the chart will render with a "Deployment Test" text on top of the chart. This domain is meant to support using a staging environment and having the ability to switch to the production version without changing the license..</p>
                      <h2>How to use license key?</h2>
                      <p>
                      Using a license
                    To use your LightningChart JS license, you must pass it as an argument to lightningChart, like so:
              </br>
              // Create an instance of lightningChart for creation of charts.
              </br>
              <span class="const-lc">const lc = lightningChart(license_key)</span>
                      </p>
              </div>

            </div>
            <div class="col-md-2"> </div>
    </div>
  <div class="col-md-2"> </div>  
    

</div>
</div>
</div>
<?php
global $wpdb;
    if(isset($_POST['submit'])){
       if(isset($_POST['prod_key']))
       {
        $prod_key =  sanitize_text_field($_POST['prod_key']);
       }
       
        $user = wp_get_current_user();
        $User_id = $user->ID;
        
        
        if($prod_key != null)
        {
       
          $exit_prod_key1 =  get_user_meta( $User_id, 'prod_key' , true );
         
          if($exit_prod_key1)
          {
           
            update_user_meta( $User_id,'prod_key',$prod_key, false ); 
         
          }else
          {
           
            add_user_meta( $User_id, 'prod_key', $prod_key, false ); 
           
          }
       
         
        }else
        {
         
          delete_user_meta($User_id,'prod_key');
           
        }
       
        $referer = $_SERVER['HTTP_REFERER'];
        header("Location: $referer"."&addmeassage=success");
    }
        
?>
