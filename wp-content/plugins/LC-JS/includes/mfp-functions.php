<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<?php

/*
 * Add my new menu to the Admin Control Panel
 */
 
// Hook the 'admin_menu' action hook, run the function named 'mfp_Add_My_Admin_Link()'
add_action( 'admin_menu', 'lcjs_Add_My_Admin_Link' );
 
// Add a new top level menu link to the ACP
function lcjs_Add_My_Admin_Link()
{
      add_menu_page(
        'My First Page', // Title of the page
        'LC JS Charts', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        'LC-JS/includes/mfp-chart-listing-page.php',
        '',
        'dashicons-chart-pie' // The 'slug' - file to display when clicking the link
    );
    add_submenu_page(
        'LC-JS/includes/mfp-chart-listing-page.php',
        'My subMenu Page', // Title of the page
        'Resources', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        'LC-JS/includes/get_help.php' // The 'slug' - file to display when clicking the link
    );
  
    add_submenu_page(
        'LC-JS/includes/mfp-chart-listing-page.php',
        'My subMenu Page', // Title of the page
        'License', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        'LC-JS/includes/License.php' // The 'slug' - file to display when clicking the link
    );

}

function get_help_data()
{
    global $wpdb;
    //echo "get help";
    include 'get-help.php';
    /*
    $mydb = new wpdb('root','','arctionusr_ecommerce_1','localhost');
    $rows = $mydb->get_results("select * from arc_license");
    */
   
}
add_action('init', 'lcjs_chart_init');
function lcjs_chart_init() {
  $labels = array(
      'name'                  => _x( 'LC-JS Charts', 'Post type general name', 'textdomain' ),
      'singular_name'         => _x( 'LC-JS Charts', 'Post type singular name', 'textdomain' ),
      'menu_name'             => _x( 'LC-JS Charts', 'Admin Menu text', 'textdomain' ),
      'name_admin_bar'        => _x( 'LC-JS Charts', 'Add New on Toolbar', 'textdomain' ),
      'add_new'               => __( 'Add New', 'textdomain' ),
      'add_new_item'          => __( 'Add New Charts', 'textdomain' ),
      'new_item'              => __( 'New Charts', 'textdomain' ),
      'edit_item'             => __( 'Edit Charts', 'textdomain' ),
      'view_item'             => __( 'View Charts', 'textdomain' ),
      'all_items'             => __( 'All Charts', 'textdomain' ),
      'search_items'          => __( 'Search Charts', 'textdomain' ),
      'parent_item_colon'     => __( 'Parent Charts:', 'textdomain' ),
      'not_found'             => __( 'No Charts found.', 'textdomain' ),
      'not_found_in_trash'    => __( 'No Charts found in Trash.', 'textdomain' ),
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => false,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'LC_JS_Charts' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields' ),
  );

  register_post_type( 'LC-JS-Charts', $args );
}
global $lccharts_scripts;
 
$lccharts_scripts = array();


function lc_charts_enqueue_javascript($js) {
  global $lccharts_scripts;
 
$lccharts_scripts = array();
  if ( !in_array( $js, $lccharts_scripts ) ) {
    $lccharts_scripts[] = $js;
  }
}



add_action( 'admin_footer', 'lc_charts_wp_footer', 1000 );
add_action( 'wp_footer', 'lc_charts_wp_footer', 1000 );

function lc_charts_wp_footer() {
  global $lccharts_scripts;
 
  echo '<script  src="'.plugins_url().'/LC-JS/includes/js/lcjs.iife.js"></script>';
  echo '<script  src="'.plugins_url().'/LC-JS/includes/js/xydata.iife.js"></script>';
  //echo '<script  src="http://localhost/lcjs/wp-content/plugins/LC-JS/includes/commo.js"></script>';
  include 'licensekey.php';
 
  if ( sizeof( $lccharts_scripts ) ) { ?>
    <script>
      <?php echo implode( "\n", $lccharts_scripts ); ?>
    </script>
    <?php
  }
}



function lcjs_global_vars() {
  
    $additional =array();
    global $chart_types;
    global $abc;
    $abc = array (
        'dashboard' =>array('Example_1' => array (
            'chart_name' => 'Dashboard Multi-Direction',
            'icon' => plugins_url().'/LC-JS/images/icons/dashboardMulti.png',
            'html_code' => '<div  id="target" class="row content"></div>',
            'javascript_code' => "
                                const {
                                    lightningChart,
                                    SolidFill,
                                    ColorRGBA,
                                    AxisScrollStrategies,
                                    DataPatterns,
                                    Themes
                                } = lcjs
                                
                                // Import data-generators from 'xydata'-library.
                                const {
                                    createProgressiveTraceGenerator,
                                    createTraceGenerator
                                } = xydata;
                                
                                // Create a 3x3 dashboard.
                                const grid = lightningChart(license_key).Dashboard({
                                    // theme: Themes.dark 
                                    container: target,
                                    numberOfRows: 3,
                                    numberOfColumns: 3
                                })
                                
                                // Add charts to dashboard.
                                const cells = [
                                    { row: 1, col: 0 },
                                    { row: 2, col: 1 },
                                    { row: 1, col: 2 },
                                    { row: 0, col: 1 },
                                    { row: 1, col: 1 }
                                ]
                                const chooseRandom = (options) => options[Math.round(Math.random() * (options.length - 1))]
                                const createCell = (cell) => {
                                    const chart = grid.createChartXY({
                                        columnIndex: cell.col,
                                        rowIndex: cell.row,
                                        columnSpan: 1,
                                        rowSpan: 1
                                    })
                                    // Add a random omni-directional series.
                                    const type = chooseRandom(['PointSeries', 'LineSeries'])
                                    // Setup data-generation for series.
                                    if (cell.row == cell.col) {
                                        const series = chart['add' + type]()
                                        // Random trace
                                        createTraceGenerator()
                                            .setNumberOfPoints(100000)
                                            .generate()
                                            .setStreamInterval(50)
                                            .setStreamBatchSize(10)
                                            .setStreamRepeat(true)
                                            .toStream()
                                            .forEach(point => series.add(point))
                                    } else {
                                        // Random progressive trace with mapped direction.
                                        const flipPlane = cell.col == 1
                                        const mul = { x: cell.col == 0 ? -1 : 1, y: cell.row == 0 ? 1 : -1 }
                                        // Configure axes.
                                        let axisX = chart.getDefaultAxisX(), axisY = chart.getDefaultAxisY()
                                        if (cell.row == cells.reduce((prev, cell) => Math.max(prev, cell.row), 0)) {
                                            axisX.dispose()
                                            axisX = chart.addAxisX(true)
                                        }
                                        if (cell.col == 0) {
                                            axisY.dispose()
                                            axisY = chart.addAxisY(true)
                                        }
                                        if (mul.x < 0) {
                                            axisX
                                                .setInterval(-100, 0)
                                                .setScrollStrategy(flipPlane ? AxisScrollStrategies.fitting : AxisScrollStrategies.regressive)
                                        } else
                                            axisX
                                                .setInterval(0, 100)
                                                .setScrollStrategy(flipPlane ? AxisScrollStrategies.fitting : AxisScrollStrategies.progressive)
                                
                                        if (mul.y < 0) {
                                            axisY
                                                .setInterval(-100, 0)
                                                .setScrollStrategy(flipPlane ? AxisScrollStrategies.regressive : AxisScrollStrategies.fitting)
                                        } else
                                            axisY
                                                .setInterval(0, 100)
                                                .setScrollStrategy(flipPlane ? AxisScrollStrategies.progressive : AxisScrollStrategies.fitting)
                                
                                        const series = chart['add' + type](axisX, axisY)
                                        createProgressiveTraceGenerator()
                                            .setNumberOfPoints(100000)
                                            .generate()
                                            .setStreamInterval(50)
                                            .setStreamBatchSize(2)
                                            .setStreamRepeat(true)
                                            .toStream()
                                            .forEach(point => series.add({ x: (flipPlane ? point.y : point.x) * mul.x, y: (flipPlane ? point.x : point.y) * mul.y }))
                                    }
                                    return chart.setTitle(type)
                                }
                                cells.map(createCell)"
        ),
                   
                    'Example_2' => array (
                        'chart_name' => 'Dashboard 5 Channels 1000 Pps',
                        'icon' => plugins_url().'/LC-JS/images/icons/dashboard5ch.png',
                        'html_code' => ' <div id="target" class="row content"></div>',
                        'javascript_code' => "const {
                            lightningChart,
                            AxisScrollStrategies,
                            emptyFill,
                            DataPatterns,
                            Themes
                        } = lcjs
                        
                        // Import data-generator from 'xydata'-library.
                        const {
                            createProgressiveRandomGenerator
                        } = xydata;
                        
                        const channels = ['Ch 1', 'Ch 2', 'Ch 3', 'Ch 4', 'Ch 5']
                        const channelCount = channels.length
                        
                        // Create Dashboard.
                        const grid = lightningChart(license_key).Dashboard({
                            // theme: Themes.dark 
                            numberOfRows: channelCount,
                            container: 'target',
                            numberOfColumns: 1
                        })
                        
                        // Map XY-charts to Dashboard for each channel.
                        const charts = channels.map((channelName, i) => {
                            const chart = grid.createChartXY({
                                columnIndex: 0,
                                rowIndex: i,
                                columnSpan: 1,
                                rowSpan: 1
                            })
                                // Hide titles because we have very little space.
                                .setTitleFillStyle(emptyFill)
                        
                            // Configure X-axis of chart to be progressive and have nice interval.
                            chart.getDefaultAxisX()
                                .setScrollStrategy(AxisScrollStrategies.progressive)
                                .setInterval(0, 10000) // 10,000 millisecond axis
                        
                            return chart
                        })
                        
                        // Map progressive line series for each chart.
                        const series = charts.map((chart, i) =>
                            chart.addLineSeries({ dataPattern: DataPatterns.horizontalProgressive })
                                // Destroy automatically outscrolled data (old data becoming out of X axis range) 
                                .setMaxPointCount(10000)
                                .setStrokeStyle((lineStyle) => lineStyle.setThickness(1.0))
                        )
                        
                        // Configure a common data-generator for all channels.
                        const dataGenerator = createProgressiveRandomGenerator()
                            .setNumberOfPoints(3600)
                        
                        // Setup a continuous data-stream for each series.
                        series.forEach((value, i) =>
                            dataGenerator
                                .generate()
                                .setStreamRepeat(true)
                                // Use 1000 points / sec data rate (1 millisecond interval for data points). 
                                // Requesting generator to give an array of data points after every 15 ms. 
                                // As using 1 millisecond data point interval, it means 15 datapoints
                                // have to be generated every round, 1 for each millisecond. 
                                .setStreamBatchSize(15) // 15 points per batch 
                                .setStreamInterval(15) // interval in milliseconds  
                                .toStream()
                                .forEach((data) => value.add(data))
                        ) "),
                        'Example_3' => array (
                            'chart_name' => 'Zoom Band Chart',
                            'icon' => plugins_url().'/LC-JS/images/icons/zoomBandChart.png',
                            'html_code' => ' <div id="target" class="row content"></div>',
                            'javascript_code' => "const {
                                lightningChart,
                                OHLCSeries,
                                SolidFill,
                                SolidLine,
                                PointSeries,
                                ColorHEX,
                                LegendBoxBuilders,
                                UIOrigins,
                                Themes
                            } = lcjs
                            
                            // Extract required parts from XYData Generator.
                            const {
                                createProgressiveTraceGenerator,
                                createOHLCGenerator,
                                createProgressiveRandomGenerator
                            } = xydata
                            
                            // Create a Dashboard, with a single column and two rows.
                            const dashboard = lightningChart(license_key).Dashboard({
                                // theme: Themes.dark 
                                numberOfColumns: 1,
                                container: 'target',
                                numberOfRows: 2
                            })
                                // Set the row height for the top Cell in Dashboard.
                                // As the bottom row is default (1), the top row height will be 3/4 of the
                                // available Dashboard height.
                                .setRowHeight(0, 3)
                            
                            // Add XY Chart to top Cell in Dashboard.
                            const chart = dashboard.createChartXY({
                                columnIndex: 0,
                                columnSpan: 1,
                                rowIndex: 0,
                                rowSpan: 1
                            })
                            
                            // Add Zoom Band Chart to bottom Cell in Dashboard.
                            const zoomBandChart = dashboard.createZoomBandChart({
                                columnIndex: 0,
                                columnSpan: 1,
                                rowIndex: 1,
                                rowSpan: 1,
                                // Specify the Axis for the Zoom Band Chart to follow.
                                // The Zoom Band Chart will imitate all Series present in that Axis.
                                axis: chart.getDefaultAxisX()
                            })
                                // Modify the styling of the Series in Zoom Band Chart.
                                .setSeriesStyle((zoomBandSeries, ref) => {
                                    // Style the 'OHLC Series' in Zoom Band Chart.
                                    if (ref instanceof OHLCSeries) {
                                        (zoomBandSeries).setStrokeStyle(new SolidLine({
                                            thickness: 1,
                                            fillStyle: new SolidFill({ color: ColorHEX('#0f0') })
                                        }))
                                    }
                                    // Style the 'Point Series' in Zoom Band Chart.
                                    if (ref instanceof PointSeries) {
                                        (zoomBandSeries).setStrokeStyle(new SolidLine({
                                            thickness: 1,
                                            fillStyle: new SolidFill({ color: ColorHEX('#bd3d17') })
                                        }))
                                    }
                                })
                            
                            // Do not animate Y Axis Scale changes on either Charts.
                            chart.getDefaultAxisY()
                                .setAnimationScroll(undefined)
                            zoomBandChart.getDefaultAxisY()
                                .setAnimationScroll(undefined)
                            zoomBandChart.band.setValueStart(300)
                            zoomBandChart.band.setValueEnd(500)
                            
                            // Add different Series to the XY Chart.
                            const line = chart.addLineSeries()
                            const ohlc = chart.addOHLCSeries()
                            const points = chart.addPointSeries()
                                .setPointSize(2)
                                .setPointFillStyle(new SolidFill({ color: ColorHEX('#bd3d17') }))
                            const areaRange = chart.addAreaRangeSeries()
                            
                            // Fill the Line Series with arbitrary data.
                            createProgressiveTraceGenerator()
                                .setNumberOfPoints(1000)
                                .generate()
                                .toPromise()
                                .then((data) => {
                                    // Offset the Y value of each point, then push to the Series.
                                    line.add(data.map((point) => ({ x: point.x, y: point.y * .1 + 100 })))
                                })
                            
                            // Fill the OHLC Series with arbitrary data.
                            createOHLCGenerator()
                                .setNumberOfPoints(1000)
                                .generate()
                                .toPromise()
                                .then((data) => {
                                    ohlc.add(data)
                                })
                            
                            // Fill the Point Series with arbitrary data.
                            createProgressiveRandomGenerator()
                                .setNumberOfPoints(1000)
                                .generate()
                                .toPromise()
                                .then((data) => {
                                    // Offset the Y value of each point, then push to the Series.
                                    points.add(data.map((point) => ({ x: point.x, y: point.y * 5 + 95 })))
                                })
                            
                            // Fill the Area Series with arbitrary data. 
                            Promise.all([
                                createProgressiveRandomGenerator()
                                    .setNumberOfPoints(1000)
                                    .generate()
                                    .toPromise(),
                                createProgressiveRandomGenerator()
                                    .setNumberOfPoints(1000)
                                    .generate()
                                    .toPromise()
                            ]).then((data) => {
                                // Offset the high and low values for each point, then push to the Series.
                                areaRange.add(data[0].map((high, i) => ({
                                    position: high.x,
                                    high: high.y + 92,
                                    low: data[1][i].y + 90
                                })))
                            })
                            
                            // Add LegendBox to the XY Chart. Note that hiding a Series in XY Chart will also
                            // hide corresponding Series in the Zoom Band Chart.
                            chart.addLegendBox(LegendBoxBuilders.VerticalLegendBox)
                                .setPosition({ x: 2, y: 100 })
                                .setOrigin(UIOrigins.LeftTop)
                                .add(chart)"),    
                    ),
          'radial_charts' => array(
                      'Example_1' => array (
                        'chart_name' => 'Donut',
                        'icon' => plugins_url().'/LC-JS/images/icons/donutChart.png',
                        'html_code' => ' <div id="target" class="row content"></div>',
                        'javascript_code' => "/*
                        * LightningChartJS example that shows the creation and styling of a donut chart.
                        */
                       
                       // Extract required parts from LightningChartJS.
                       const {
                           lightningChart,
                           PieChartTypes,
                           SolidLine,
                           UIElementBuilders,
                           LegendBoxBuilders,
                           SolidFill,
                           ColorRGBA,
                           UIDraggingModes,
                           SolidFillPalette,
                           SliceLabelFormatters,
                           UIOrigins,
                           Themes
                       } = lcjs
                       
                       const donut = lightningChart(license_key).Pie({
                           // theme: Themes.dark ,
                           container: 'target',
                           type: PieChartTypes.LabelsInsideSlices
                       })
                           .setTitle('Inter Hotels - hotel visitors in June 2016')
                           .setPadding({ top: 40 })
                           .setAnimationsEnabled(true)
                           .setMultipleSliceExplosion(false)
                           .setInnerRadius(60)
                       
                       // ----- Cache stroke style used for Slice borders. -----
                       const customStrokeStyle = new SolidLine({ fillStyle: new SolidFill({ color: ColorRGBA(50, 70, 80) }), thickness: 5 })
                       
                       // ----- Static data -----
                       const data = {
                           country: ['US', 'Canada', 'Greece', 'UK', 'Finland', 'Denmark'],
                           values: [15000, 20030, 8237, 16790, 9842, 4300]
                       }
                       // Preparing data for each Slice
                       const processedData = []
                       let totalVisitor = 0
                       for (let i = 0; i < data.values.length; i++) {
                           totalVisitor += data.values[i]
                           processedData.push({ name: data.country[i], value: data.values[i] })
                       }
                       
                       // ----- Create custom Palette for Donut (defines color of Slice filling) ----
                       const colorArray = [
                           ColorRGBA(219, 155, 36, 255),
                           ColorRGBA(219, 102, 36, 255),
                           ColorRGBA(173, 21, 74, 255),
                           ColorRGBA(173, 168, 21, 255),
                           ColorRGBA(223, 64, 64, 255),
                           ColorRGBA(173, 100, 21, 255)
                       ]
                       
                       // The color palette should be a function that returns a specific color - in this case, just return a color from a specific index in the array
                       const colorPalette = (length) => (index) => {
                           return colorArray[index]
                       }
                       
                       // Use the SolidFillPalette to create a fillStyle palette which the chart can use to easily assign fillStyles using colors from the given color palette
                       const fillStylePalette = SolidFillPalette(colorPalette, data.values.length)
                       // Set the custom fillStyle for the Donut Chart
                       donut.setSliceFillStyle(fillStylePalette)
                       
                       // ----- Create Slices -----
                       processedData.map((item) => donut.addSlice(item.name, item.value))
                       donut.setLabelFormatter(SliceLabelFormatters.NamePlusValue).setSliceStrokeStyle(customStrokeStyle)
                       // ----- Add LegendBox -----
                       donut.addLegendBox(LegendBoxBuilders.HorizontalLegendBox)
                           .setPosition({ x: 1, y: 99 })
                           .setOrigin(UIOrigins.LeftTop)
                           .setMargin(5)
                           .add(donut)
                       
                       // ----- Add TextBox -----
                       donut.addUIElement(UIElementBuilders.TextBox.addStyler(
                           textBox =>
                               textBox.setFont(fontSettings => fontSettings.setSize(25)).setText(`Total: ${totalVisitor} visitors`)
                       ))
                           .setPosition({ x: 50, y: 50 })
                           .setOrigin(UIOrigins.CenterTop)
                           .setDraggingMode(UIDraggingModes.notDraggable)
                           .setMargin(5)
                       "),
                              'Example_2' => array (
                                 'chart_name' => 'Radar',
                                  'icon' => plugins_url().'/LC-JS/images/icons/spiderAnimated.png',
                                  'html_code' => '<div id="target" class="row content"></div>',
                                  'javascript_code' => "const {
                                                      lightningChart,
                                                      SpiderWebMode,
                                                      Themes
                                                  } = lcjs
                                                  
                                                  // Create a circular spider chart and add a series to it.
                                                  const chart = lightningChart(license_key).Spider({
                                                      // theme: Themes.dark ,
                                                      container: 'target'
                                                  })
                                                      .setTitle('Animated Radar Chart')
                                                      .setAxisInterval(100)
                                                      // Configure spider to be circular (like a traditional Radar Chart).
                                                      .setWebMode(SpiderWebMode.Circle)
                                                  const series = chart.addSeries()
                                                  
                                                  // Set initial series values, creating axes.
                                                  const categories = ['Category A', 'Category B', 'Category C', 'Category D', 'Category E']
                                                  series.addPoints(
                                                      { axis: categories[0], value: 100 },
                                                      { axis: categories[1], value: 100 },
                                                      { axis: categories[2], value: 100 },
                                                      { axis: categories[3], value: 100 },
                                                      { axis: categories[4], value: 100 }
                                                  )
                                                  
                                                  // Setup randomization of series values at regular intervals.
                                                  const randomizePoints = () => {
                                                      for (const category of categories) {
                                                          const value = Math.random() * 100
                                                          series.addPoints({ axis: category, value })
                                                      }
                                                  }
                                                  // Randomize points every other second (2000 ms).
                                                  setInterval(randomizePoints, 2000)"
                                              ),
                              'Example_3' => array (
                              'chart_name' => 'Pie Chart',
                              'icon' => plugins_url().'/LC-JS/images/icons/pieChart.png',
                              'html_code' => '<div id="target" class="row content"></div>',
                              'javascript_code' => "const {
                                                             lightningChart,
                                                             PieChartTypes,
                                                             LegendBoxBuilders,
                                                             SliceLabelFormatters,
                                                             SolidFillPalette,
                                                             ColorPalettes,
                                                             SolidFill,
                                                             SolidLine,
                                                             ColorRGBA,
                                                             Themes
                                                          } = lcjs
                                                                                
                                                          const pieType = window.innerWidth > 599 ? PieChartTypes.LabelsOnSides : PieChartTypes.LabelsInsideSlices
                                                                                
                                                          const pie = lightningChart(license_key).Pie({
                                                                       // theme: Themes.dark 
                                                                        container: 'target',
                                                                        type: pieType
                                                                      })
                                                                        .setTitle('Project Time Division')
                                                                        .setAnimationsEnabled(true)
                                                                        .setMultipleSliceExplosion(true)
                                                                                
                                                                        // ----- User defined data -----
                                                                                const data = [
                                                                                    {
                                                                                        name: 'Planning',
                                                                                        value: 40
                                                                                    },
                                                                                    {
                                                                                        name: 'Development',
                                                                                        value: 120
                                                                                    },
                                                                                    {
                                                                                        name: 'Testing',
                                                                                        value: 60
                                                                                    },
                                                                                    {
                                                                                        name: 'Review',
                                                                                        value: 24
                                                                                    },
                                                                                    {
                                                                                        name: 'Bug Fixing',
                                                                                        value: 90
                                                                                    }
                                                                                ]
                                                                                
                                                                        // ----- Create Slices -----
                                                                        const slices = data.map((item) => pie.addSlice(item.name, item.value))
                                                                                
                                                                        // Specify function which generates text for Slice Labels(LabelFormatter).
                                                                                
                                                                         pie.setLabelFormatter(SliceLabelFormatters.NamePlusRelativeValue)
                                                                                
                                                                                
                                                                         // ----- Add LegendBox -----
                                                                         pie.addLegendBox(LegendBoxBuilders.VerticalLegendBox)
                                                                                    .setPosition({ x: 0, y: 0 })
                                                                                    .setOrigin({ x: -1, y: -1 })
                                                                                    .setMargin({ bottom: 5, left: 5 })
                                                                                    .add(pie)
                                                                                
                                                                          // ----- Create custom Palette for Pie (defines color of Slice filling) ----
                                                                          const palette = SolidFillPalette(ColorPalettes.sector(180, 320, 0.7, 0.7), 5)
                                                                                
                                                                          // --------- Create line around slices -----
                                                                          const customStrokeStyle = new SolidLine({ fillStyle: new SolidFill({ color: ColorRGBA(160, 160, 160) }), thickness: 2 })
                                                                       
                                                                          pie.setSliceFillStyle(palette)
                                                                          .setSliceStrokeStyle(customStrokeStyle)"
                                                  ),
                                  ),
        'line_series' => array(
                      'Example_1' => array (
                          'chart_name' => 'Diesel and Gasoline Price Comparison',
                          'icon' => plugins_url().'/LC-JS/images/icons/lineSeries.png',
                          'html_code' => '<div id="target" class="row content"></div>',
                          'javascript_code' => "
                                            const {
                                                lightningChart,
                                                SolidLine,
                                                SolidFill,
                                                ColorRGBA,
                                                AxisTickStrategies,
                                                UIOrigins,
                                                DataPatterns,
                                                Themes
                                            } = lcjs
                                            
                                            // Decide on an origin for DateTime axis.
                                            const dateOrigin = new Date(2018, 8, 1)
                                            
                                            // ----- Cache used styles -----
                                            const customStrokeStyle = new SolidLine({ fillStyle: new SolidFill({ color: ColorRGBA(200, 50, 50) }), thickness: 2 })
                                            
                                            // Create a XY Chart.
                                            const chart = lightningChart(license_key).ChartXY({
                                                // theme: Themes.dark
                                                container: 'target',
                                            })
                                            // Modify the default X Axis to use DateTime TickStrategy, and set the origin for the DateTime Axis.
                                            chart.getDefaultAxisX().setTickStrategy(AxisTickStrategies.DateTime, (tickStrategy) => tickStrategy.setDateOrigin(dateOrigin))
                                            
                                            chart.setPadding({
                                                right: 50
                                            })
                                                .setTitle('Diesel and Gasoline Price Comparison')
                                            
                                            const diesel = [
                                                { x: 0, y: 1.52 },
                                                { x: 1, y: 1.52 },
                                                { x: 2, y: 1.52 },
                                                { x: 3, y: 1.58 },
                                                { x: 4, y: 2.00 },
                                                { x: 5, y: 2.00 },
                                                { x: 6, y: 2.00 },
                                                { x: 7, y: 2.00 },
                                                { x: 8, y: 2.26 },
                                                { x: 9, y: 1.90 },
                                                { x: 10, y: 1.90 },
                                                { x: 11, y: 1.90 },
                                                { x: 12, y: 1.90 },
                                                { x: 13, y: 1.60 },
                                                { x: 14, y: 1.60 },
                                                { x: 15, y: 1.60 },
                                                { x: 16, y: 1.00 },
                                                { x: 17, y: 1.00 },
                                                { x: 18, y: 1.00 },
                                                { x: 19, y: 1.74 },
                                                { x: 20, y: 1.47 },
                                                { x: 21, y: 1.47 },
                                                { x: 22, y: 1.47 },
                                                { x: 23, y: 1.74 },
                                                { x: 24, y: 1.74 },
                                                { x: 25, y: 1.74 },
                                                { x: 27, y: 1.5 },
                                                { x: 28, y: 1.5 },
                                                { x: 29, y: 1.5 }
                                            ]
                                            
                                            const gasoline = [
                                                { x: 0, y: 1.35 },
                                                { x: 1, y: 1.35 },
                                                { x: 2, y: 1.35 },
                                                { x: 3, y: 1.35 },
                                                { x: 4, y: 1.90 },
                                                { x: 5, y: 1.90 },
                                                { x: 6, y: 1.90 },
                                                { x: 7, y: 1.92 },
                                                { x: 8, y: 1.50 },
                                                { x: 9, y: 1.50 },
                                                { x: 10, y: 1.3 },
                                                { x: 11, y: 1.3 },
                                                { x: 12, y: 1.3 },
                                                { x: 13, y: 1.3 },
                                                { x: 14, y: 1.3 },
                                                { x: 15, y: 1.32 },
                                                { x: 16, y: 1.40 },
                                                { x: 17, y: 1.44 },
                                                { x: 18, y: 1.02 },
                                                { x: 19, y: 1.02 },
                                                { x: 20, y: 1.02 },
                                                { x: 21, y: 1.02 },
                                                { x: 22, y: 1.02 },
                                                { x: 23, y: 1.02 },
                                                { x: 24, y: 1.02 },
                                                { x: 25, y: 1.02 },
                                                { x: 27, y: 1.30 },
                                                { x: 28, y: 1.30 },
                                                { x: 29, y: 1.30 }
                                            ]
                                            
                                            // Add two line series.
                                            const lineSeries = chart.addLineSeries({ dataPattern: DataPatterns.horizontalProgressive })
                                                .setName('Diesel')
                                            
                                            const lineSeries2 = chart.addLineSeries({ dataPattern: DataPatterns.horizontalProgressive })
                                                .setName('Gasoline')
                                                .setStrokeStyle(customStrokeStyle)
                                            
                                            // Set the correct value to use for the data frequency.
                                            // 1000ms * 60s * 60min * 24h
                                            const dataFrequency = 1000 * 60 * 60 * 24
                                            
                                            // Add the points to each Series - the X values are multiplied by dataFrequency to set the values properly on the X Axis.
                                            lineSeries2.add(diesel.map((point) => ({ x: point.x * dataFrequency, y: point.y })))
                                            lineSeries.add(gasoline.map((point) => ({ x: point.x * dataFrequency, y: point.y })))
                                            
                                            // Setup view nicely.
                                            chart.getDefaultAxisY()
                                                .setTitle('$/litre')
                                                .setInterval(0, 3, false, true)
                                            
                                            // Enable AutoCursor auto-fill.
                                            chart.setAutoCursor(cursor => cursor
                                                .setResultTableAutoTextStyle(true)
                                                .disposeTickMarkerX()
                                                .setTickMarkerYAutoTextStyle(true)
                                            )
                                            const legend = chart.addLegendBox()
                                                .setOrigin(UIOrigins.RightTop)
                                                .setPosition({ x: 90, y: 90 })
                                                .setMargin({ left: 10, right: 10, top: 10, bottom: 10 })
                                            
                                            // Add Chart to LegendBox.
                                            legend.add(chart)"
                        
                                            ),
                        'Example_2' => array (
                            'chart_name' => 'ECG',
                            'icon' => plugins_url().'/LC-JS/images/icons/ecg.png',
                            'html_code' => '<div id="target" class="row content"></div>',
                            'javascript_code' => "const {
                                lightningChart,
                                DataPatterns,
                                AxisScrollStrategies,
                                SolidLine,
                                SolidFill,
                                ColorHEX,
                                AutoCursorModes,
                                Themes
                            } = lcjs
                            
                            // Import data-generators from 'xydata'-library.
                            const {
                                createSampledDataGenerator
                            } = xydata
                            
                            // Create a XY Chart.
                            const chart = lightningChart(license_key).ChartXY({
                                // theme: Themes.dark 
                                container: 'target'
                            }).setTitle('ECG')
                            
                            // Add line series to visualize the data received
                            const series = chart.addLineSeries({ dataPattern: DataPatterns.horizontalProgressive })
                            // Style the series
                            series
                                .setStrokeStyle(new SolidLine({
                                    thickness: 2,
                                    fillStyle: new SolidFill({ color: ColorHEX('#5aafc7') })
                                }))
                                .setMouseInteractions(false)
                            
                            chart.setAutoCursorMode(AutoCursorModes.disabled)
                            
                            // Setup view nicely.
                            chart.getDefaultAxisY()
                                .setTitle('mV')
                                .setInterval(-1600, 1000)
                                .setScrollStrategy(AxisScrollStrategies.expansion)
                            
                            chart.getDefaultAxisX()
                                .setTitle('milliseconds')
                                .setInterval(0, 2500)
                                .setScrollStrategy(AxisScrollStrategies.progressive)
                            
                            // Points that are used to generate a continuous stream of data.
                            const point = [
                                { x: 2, y: 81 },
                                { x: 3, y: 83 },
                                { x: 4, y: 88 },
                                { x: 5, y: 98 },
                                { x: 6, y: 92 },
                                { x: 7, y: 85 },
                                { x: 8, y: 73 },
                                { x: 9, y: 71 },
                                { x: 10, y: 70 },
                                { x: 11, y: 83 },
                                { x: 12, y: 73 },
                                { x: 13, y: 79 },
                                { x: 14, y: 84 },
                                { x: 15, y: 78 },
                                { x: 16, y: 67 },
                                { x: 17, y: 71 },
                                { x: 18, y: 76 },
                                { x: 19, y: 77 },
                                { x: 20, y: 64 },
                                { x: 21, y: 53 },
                                { x: 22, y: 0 },
                                { x: 23, y: 41 },
                                { x: 24, y: 51 },
                                { x: 25, y: 3 },
                                { x: 26, y: 31 },
                                { x: 27, y: 37 },
                                { x: 28, y: 35 },
                                { x: 29, y: 48 },
                                { x: 30, y: 40 },
                                { x: 31, y: 42 },
                                { x: 32, y: 42 },
                                { x: 33, y: 32 },
                                { x: 34, y: 21 },
                                { x: 35, y: 41 },
                                { x: 36, y: 48 },
                                { x: 37, y: 47 },
                                { x: 38, y: 45 },
                                { x: 39, y: 42 },
                                { x: 40, y: 28 },
                                { x: 41, y: 15 },
                                { x: 42, y: 1 },
                                { x: 43, y: -12 },
                                { x: 44, y: -4 },
                                { x: 45, y: 15 },
                                { x: 46, y: 23 },
                                { x: 47, y: 22 },
                                { x: 48, y: 40 },
                                { x: 49, y: 46 },
                                { x: 50, y: 49 },
                                { x: 51, y: 48 },
                                { x: 52, y: 43 },
                                { x: 53, y: 52 },
                                { x: 54, y: 49 },
                                { x: 55, y: 44 },
                                { x: 56, y: 41 },
                                { x: 57, y: 41 },
                                { x: 58, y: 45 },
                                { x: 59, y: 57 },
                                { x: 60, y: 67 },
                                { x: 61, y: 65 },
                                { x: 62, y: 58 },
                                { x: 63, y: 47 },
                                { x: 64, y: 34 },
                                { x: 65, y: 35 },
                                { x: 66, y: 23 },
                                { x: 67, y: 11 },
                                { x: 68, y: 7 },
                                { x: 69, y: 14 },
                                { x: 70, y: 23 },
                                { x: 71, y: 18 },
                                { x: 72, y: 31 },
                                { x: 73, y: 35 },
                                { x: 74, y: 44 },
                                { x: 75, y: 49 },
                                { x: 76, y: 34 },
                                { x: 77, y: 7 },
                                { x: 78, y: -3 },
                                { x: 79, y: -8 },
                                { x: 80, y: -11 },
                                { x: 81, y: -20 },
                                { x: 82, y: -28 },
                                { x: 83, y: -4 },
                                { x: 84, y: 15 },
                                { x: 85, y: 20 },
                                { x: 86, y: 26 },
                                { x: 87, y: 26 },
                                { x: 88, y: 24 },
                                { x: 89, y: 34 },
                                { x: 90, y: 35 },
                                { x: 91, y: 30 },
                                { x: 92, y: 22 },
                                { x: 93, y: 12 },
                                { x: 94, y: 15 },
                                { x: 95, y: 18 },
                                { x: 96, y: 24 },
                                { x: 97, y: 18 },
                                { x: 98, y: 26 },
                                { x: 99, y: 25 },
                                { x: 100, y: 13 },
                                { x: 101, y: 2 },
                                { x: 102, y: 1 },
                                { x: 103, y: -10 },
                                { x: 104, y: -10 },
                                { x: 105, y: -4 },
                                { x: 106, y: 8 },
                                { x: 107, y: 15 },
                                { x: 108, y: 15 },
                                { x: 109, y: 15 },
                                { x: 110, y: 15 },
                                { x: 111, y: 18 },
                                { x: 112, y: 19 },
                                { x: 113, y: 3 },
                                { x: 114, y: -12 },
                                { x: 115, y: -14 },
                                { x: 116, y: -10 },
                                { x: 117, y: -22 },
                                { x: 118, y: -24 },
                                { x: 119, y: -29 },
                                { x: 120, y: -21 },
                                { x: 121, y: -19 },
                                { x: 122, y: -26 },
                                { x: 123, y: -9 },
                                { x: 124, y: -10 },
                                { x: 125, y: -6 },
                                { x: 126, y: -8 },
                                { x: 127, y: -31 },
                                { x: 128, y: -52 },
                                { x: 129, y: -57 },
                                { x: 130, y: -40 },
                                { x: 131, y: -20 },
                                { x: 132, y: 7 },
                                { x: 133, y: 14 },
                                { x: 134, y: 10 },
                                { x: 135, y: 6 },
                                { x: 136, y: 12 },
                                { x: 137, y: -5 },
                                { x: 138, y: -2 },
                                { x: 139, y: 9 },
                                { x: 140, y: 23 },
                                { x: 141, y: 36 },
                                { x: 142, y: 52 },
                                { x: 143, y: 61 },
                                { x: 144, y: 56 },
                                { x: 145, y: 48 },
                                { x: 146, y: 48 },
                                { x: 147, y: 38 },
                                { x: 148, y: 29 },
                                { x: 149, y: 33 },
                                { x: 150, y: 20 },
                                { x: 151, y: 1 },
                                { x: 152, y: -7 },
                                { x: 153, y: -9 },
                                { x: 154, y: -4 },
                                { x: 155, y: -12 },
                                { x: 156, y: -3 },
                                { x: 157, y: 5 },
                                { x: 158, y: -3 },
                                { x: 159, y: 12 },
                                { x: 160, y: 6 },
                                { x: 161, y: -10 },
                                { x: 162, y: -2 },
                                { x: 163, y: 15 },
                                { x: 164, y: 17 },
                                { x: 165, y: 21 },
                                { x: 166, y: 22 },
                                { x: 167, y: 15 },
                                { x: 168, y: 16 },
                                { x: 169, y: 1 },
                                { x: 170, y: -2 },
                                { x: 171, y: -9 },
                                { x: 172, y: -16 },
                                { x: 173, y: -18 },
                                { x: 174, y: -15 },
                                { x: 175, y: -4 },
                                { x: 176, y: 0 },
                                { x: 177, y: -1 },
                                { x: 178, y: -1 },
                                { x: 179, y: -3 },
                                { x: 180, y: -12 },
                                { x: 181, y: -15 },
                                { x: 182, y: -13 },
                                { x: 183, y: -16 },
                                { x: 184, y: -29 },
                                { x: 185, y: -34 },
                                { x: 186, y: -28 },
                                { x: 187, y: -29 },
                                { x: 188, y: -27 },
                                { x: 189, y: -25 },
                                { x: 190, y: -25 },
                                { x: 191, y: -33 },
                                { x: 192, y: -38 },
                                { x: 193, y: -36 },
                                { x: 194, y: -12 },
                                { x: 195, y: -7 },
                                { x: 196, y: -20 },
                                { x: 197, y: -21 },
                                { x: 198, y: -14 },
                                { x: 199, y: -7 },
                                { x: 200, y: 7 },
                                { x: 201, y: 14 },
                                { x: 202, y: 18 },
                                { x: 203, y: 28 },
                                { x: 204, y: 27 },
                                { x: 205, y: 38 },
                                { x: 206, y: 33 },
                                { x: 207, y: 24 },
                                { x: 208, y: 20 },
                                { x: 209, y: 15 },
                                { x: 210, y: 6 },
                                { x: 211, y: 0 },
                                { x: 212, y: -2 },
                                { x: 213, y: 2 },
                                { x: 214, y: 0 },
                                { x: 215, y: -2 },
                                { x: 216, y: -12 },
                                { x: 217, y: -10 },
                                { x: 218, y: 20 },
                                { x: 219, y: 41 },
                                { x: 220, y: 35 },
                                { x: 221, y: 27 },
                                { x: 222, y: 12 },
                                { x: 223, y: -1 },
                                { x: 224, y: -15 },
                                { x: 225, y: -20 },
                                { x: 226, y: -23 },
                                { x: 227, y: 0 },
                                { x: 228, y: 24 },
                                { x: 229, y: 36 },
                                { x: 230, y: 52 },
                                { x: 231, y: 61 },
                                { x: 232, y: 67 },
                                { x: 233, y: 73 },
                                { x: 234, y: 88 },
                                { x: 235, y: 85 },
                                { x: 236, y: 71 },
                                { x: 237, y: 74 },
                                { x: 238, y: 67 },
                                { x: 239, y: 41 },
                                { x: 240, y: 26 },
                                { x: 241, y: 13 },
                                { x: 242, y: 10 },
                                { x: 243, y: 1 },
                                { x: 244, y: -10 },
                                { x: 245, y: -26 },
                                { x: 246, y: -33 },
                                { x: 247, y: -23 },
                                { x: 248, y: -25 },
                                { x: 249, y: -24 },
                                { x: 250, y: -24 },
                                { x: 251, y: -28 },
                                { x: 252, y: -24 },
                                { x: 253, y: -25 },
                                { x: 254, y: -21 },
                                { x: 255, y: -8 },
                                { x: 256, y: -5 },
                                { x: 257, y: -4 },
                                { x: 258, y: -13 },
                                { x: 259, y: -29 },
                                { x: 260, y: -42 },
                                { x: 261, y: -52 },
                                { x: 262, y: -52 },
                                { x: 263, y: -40 },
                                { x: 264, y: -40 },
                                { x: 265, y: -34 },
                                { x: 266, y: -28 },
                                { x: 267, y: -30 },
                                { x: 268, y: -37 },
                                { x: 269, y: -40 },
                                { x: 270, y: -38 },
                                { x: 271, y: -41 },
                                { x: 272, y: -39 },
                                { x: 273, y: -46 },
                                { x: 274, y: -48 },
                                { x: 275, y: -48 },
                                { x: 276, y: -40 },
                                { x: 277, y: -40 },
                                { x: 278, y: -45 },
                                { x: 279, y: -57 },
                                { x: 280, y: -61 },
                                { x: 281, y: -63 },
                                { x: 282, y: -78 },
                                { x: 283, y: -81 },
                                { x: 284, y: -87 },
                                { x: 285, y: -82 },
                                { x: 286, y: -88 },
                                { x: 287, y: -100 },
                                { x: 288, y: -100 },
                                { x: 289, y: -97 },
                                { x: 290, y: -104 },
                                { x: 291, y: -102 },
                                { x: 292, y: -79 },
                                { x: 293, y: -72 },
                                { x: 294, y: -72 },
                                { x: 295, y: -63 },
                                { x: 296, y: -35 },
                                { x: 297, y: -22 },
                                { x: 298, y: -10 },
                                { x: 299, y: 2 },
                                { x: 300, y: 5 },
                                { x: 301, y: 9 },
                                { x: 302, y: -10 },
                                { x: 303, y: -16 },
                                { x: 304, y: -16 },
                                { x: 305, y: -10 },
                                { x: 306, y: -4 },
                                { x: 307, y: -1 },
                                { x: 308, y: 2 },
                                { x: 309, y: 14 },
                                { x: 310, y: 21 },
                                { x: 311, y: 23 },
                                { x: 312, y: 17 },
                                { x: 313, y: 13 },
                                { x: 314, y: 10 },
                                { x: 315, y: 0 },
                                { x: 316, y: -6 },
                                { x: 317, y: -5 },
                                { x: 318, y: 11 },
                                { x: 319, y: 22 },
                                { x: 320, y: 28 },
                                { x: 321, y: 31 },
                                { x: 322, y: 33 },
                                { x: 323, y: 29 },
                                { x: 324, y: 26 },
                                { x: 325, y: 27 },
                                { x: 326, y: 28 },
                                { x: 327, y: 26 },
                                { x: 328, y: 35 },
                                { x: 329, y: 44 },
                                { x: 330, y: 52 },
                                { x: 331, y: 80 },
                                { x: 332, y: 100 },
                                { x: 333, y: 101 },
                                { x: 334, y: 111 },
                                { x: 335, y: 120 },
                                { x: 336, y: 128 },
                                { x: 337, y: 150 },
                                { x: 338, y: 174 },
                                { x: 339, y: 201 },
                                { x: 340, y: 232 },
                                { x: 341, y: 278 },
                                { x: 342, y: 350 },
                                { x: 343, y: 422 },
                                { x: 344, y: 510 },
                                { x: 345, y: 580 },
                                { x: 346, y: 662 },
                                { x: 347, y: 738 },
                                { x: 348, y: 806 },
                                { x: 349, y: 869 },
                                { x: 350, y: 907 },
                                { x: 351, y: 939 },
                                { x: 352, y: 954 },
                                { x: 353, y: 971 },
                                { x: 354, y: 961 },
                                { x: 355, y: 912 },
                                { x: 356, y: 826 },
                                { x: 357, y: 713 },
                                { x: 358, y: 553 },
                                { x: 359, y: 382 },
                                { x: 360, y: 166 },
                                { x: 361, y: -56 },
                                { x: 362, y: -275 },
                                { x: 363, y: -518 },
                                { x: 364, y: -824 },
                                { x: 365, y: -1122 },
                                { x: 366, y: -1325 },
                                { x: 367, y: -1453 },
                                { x: 368, y: -1507 },
                                { x: 369, y: -1547 },
                                { x: 370, y: -1568 },
                                { x: 371, y: -1559 },
                                { x: 372, y: -1553 },
                                { x: 373, y: -1537 },
                                { x: 374, y: -1499 },
                                { x: 375, y: -1447 },
                                { x: 376, y: -1424 },
                                { x: 377, y: -1398 },
                                { x: 378, y: -1352 },
                                { x: 379, y: -1291 },
                                { x: 380, y: -1189 },
                                { x: 381, y: -1085 },
                                { x: 382, y: -977 },
                                { x: 383, y: -852 },
                                { x: 384, y: -736 },
                                { x: 385, y: -649 },
                                { x: 386, y: -603 },
                                { x: 387, y: -576 },
                                { x: 388, y: -454 },
                                { x: 389, y: -443 },
                                { x: 390, y: -332 },
                                { x: 391, y: -264 },
                                { x: 392, y: -209 },
                                { x: 393, y: -153 },
                                { x: 394, y: -105 },
                                { x: 395, y: -61 },
                                { x: 396, y: -16 },
                                { x: 397, y: 37 },
                                { x: 398, y: 96 },
                                { x: 399, y: 150 },
                                { x: 400, y: 198 },
                                { x: 401, y: 238 },
                                { x: 402, y: 265 },
                                { x: 403, y: 294 },
                                { x: 404, y: 324 },
                                { x: 405, y: 351 },
                                { x: 406, y: 367 },
                                { x: 407, y: 376 },
                                { x: 408, y: 378 },
                                { x: 409, y: 391 },
                                { x: 410, y: 406 },
                                { x: 411, y: 427 },
                                { x: 412, y: 433 },
                                { x: 413, y: 448 },
                                { x: 414, y: 440 },
                                { x: 415, y: 429 },
                                { x: 416, y: 429 },
                                { x: 417, y: 420 },
                                { x: 418, y: 413 },
                                { x: 419, y: 420 },
                                { x: 420, y: 411 },
                                { x: 421, y: 408 },
                                { x: 422, y: 404 },
                                { x: 423, y: 398 },
                                { x: 424, y: 401 },
                                { x: 425, y: 412 },
                                { x: 426, y: 389 },
                                { x: 427, y: 367 },
                                { x: 428, y: 357 },
                                { x: 429, y: 359 },
                                { x: 430, y: 351 },
                                { x: 431, y: 345 },
                                { x: 432, y: 341 },
                                { x: 433, y: 345 },
                                { x: 434, y: 346 },
                                { x: 435, y: 340 },
                                { x: 436, y: 334 },
                                { x: 437, y: 323 },
                                { x: 438, y: 319 },
                                { x: 439, y: 314 },
                                { x: 440, y: 284 },
                                { x: 441, y: 263 },
                                { x: 442, y: 261 },
                                { x: 443, y: 248 },
                                { x: 444, y: 234 },
                                { x: 445, y: 236 },
                                { x: 446, y: 236 },
                                { x: 447, y: 248 },
                                { x: 448, y: 252 },
                                { x: 449, y: 251 },
                                { x: 450, y: 237 },
                                { x: 451, y: 230 },
                                { x: 452, y: 238 },
                                { x: 453, y: 227 },
                                { x: 454, y: 207 },
                                { x: 455, y: 188 },
                                { x: 456, y: 163 },
                                { x: 457, y: 155 },
                                { x: 458, y: 152 },
                                { x: 459, y: 153 },
                                { x: 460, y: 156 },
                                { x: 461, y: 171 },
                                { x: 462, y: 162 },
                                { x: 463, y: 155 },
                                { x: 464, y: 148 },
                                { x: 465, y: 139 },
                                { x: 466, y: 154 },
                                { x: 467, y: 158 },
                                { x: 468, y: 155 },
                                { x: 469, y: 159 },
                                { x: 470, y: 147 },
                                { x: 471, y: 143 },
                                { x: 472, y: 133 },
                                { x: 473, y: 118 },
                                { x: 474, y: 118 },
                                { x: 475, y: 121 },
                                { x: 476, y: 130 },
                                { x: 477, y: 133 },
                                { x: 478, y: 133 },
                                { x: 479, y: 128 },
                                { x: 480, y: 120 },
                                { x: 481, y: 97 },
                                { x: 482, y: 91 },
                                { x: 483, y: 88 },
                                { x: 484, y: 85 },
                                { x: 485, y: 84 },
                                { x: 486, y: 74 },
                                { x: 487, y: 44 },
                                { x: 488, y: 32 },
                                { x: 489, y: 10 },
                                { x: 490, y: -2 },
                                { x: 491, y: -9 },
                                { x: 492, y: -4 },
                                { x: 493, y: -5 },
                                { x: 494, y: 1 },
                                { x: 495, y: 5 },
                                { x: 496, y: 21 },
                                { x: 497, y: 41 },
                                { x: 498, y: 44 },
                                { x: 499, y: 39 },
                                { x: 500, y: 24 },
                                { x: 501, y: 22 },
                                { x: 502, y: 37 },
                                { x: 503, y: 44 },
                                { x: 504, y: 35 },
                                { x: 505, y: 31 },
                                { x: 506, y: 35 },
                                { x: 507, y: 20 },
                                { x: 508, y: 15 },
                                { x: 509, y: 7 },
                                { x: 510, y: 4 },
                                { x: 511, y: 9 },
                                { x: 512, y: 0 },
                                { x: 513, y: -15 },
                                { x: 514, y: -21 },
                                { x: 515, y: -31 },
                                { x: 516, y: -32 },
                                { x: 517, y: -48 },
                                { x: 518, y: -53 },
                                { x: 519, y: -29 },
                                { x: 520, y: -14 },
                                { x: 521, y: -6 },
                                { x: 522, y: 1 },
                                { x: 523, y: 4 },
                                { x: 524, y: -4 },
                                { x: 525, y: -3 },
                                { x: 526, y: 2 },
                                { x: 527, y: 1 },
                                { x: 528, y: -12 },
                                { x: 529, y: -37 },
                                { x: 530, y: -29 },
                                { x: 531, y: -25 },
                                { x: 532, y: -18 },
                                { x: 533, y: -31 },
                                { x: 534, y: -42 },
                                { x: 535, y: -26 },
                                { x: 536, y: -22 },
                                { x: 537, y: -18 },
                                { x: 538, y: -25 },
                                { x: 539, y: -16 },
                                { x: 540, y: -13 },
                                { x: 541, y: -23 },
                                { x: 542, y: -15 },
                                { x: 543, y: 0 },
                                { x: 544, y: 8 },
                                { x: 545, y: 14 },
                                { x: 546, y: 34 },
                                { x: 547, y: 39 },
                                { x: 548, y: 33 },
                                { x: 549, y: 22 },
                                { x: 550, y: 18 },
                                { x: 551, y: 20 },
                                { x: 552, y: 23 },
                                { x: 553, y: 16 },
                                { x: 554, y: 11 },
                                { x: 555, y: 1 },
                                { x: 556, y: 6 },
                                { x: 557, y: 11 },
                                { x: 558, y: 7 },
                                { x: 559, y: 14 },
                                { x: 560, y: 22 },
                                { x: 561, y: 14 },
                                { x: 562, y: 14 },
                                { x: 563, y: 5 },
                                { x: 564, y: -6 },
                                { x: 565, y: -14 },
                                { x: 566, y: -27 },
                                { x: 567, y: -28 },
                                { x: 568, y: -21 },
                                { x: 569, y: -16 },
                                { x: 570, y: -8 },
                                { x: 571, y: -5 },
                                { x: 572, y: -8 },
                                { x: 573, y: 3 },
                                { x: 574, y: 22 },
                                { x: 575, y: 29 },
                                { x: 576, y: 27 },
                                { x: 577, y: 23 },
                                { x: 578, y: 22 },
                                { x: 579, y: 25 },
                                { x: 580, y: 34 },
                                { x: 581, y: 36 },
                                { x: 582, y: 39 },
                                { x: 583, y: 44 },
                                { x: 584, y: 55 },
                                { x: 585, y: 54 },
                                { x: 586, y: 44 },
                                { x: 587, y: 39 },
                                { x: 588, y: 41 },
                                { x: 589, y: 49 },
                                { x: 590, y: 44 },
                                { x: 591, y: 33 },
                                { x: 592, y: 27 },
                                { x: 593, y: 23 },
                                { x: 594, y: 20 },
                                { x: 595, y: 18 },
                                { x: 596, y: 20 },
                                { x: 597, y: 19 },
                                { x: 598, y: 8 },
                                { x: 599, y: 7 },
                                { x: 600, y: 2 },
                                { x: 601, y: 4 },
                                { x: 602, y: -3 },
                                { x: 603, y: -16 },
                                { x: 604, y: -16 },
                                { x: 605, y: -19 },
                                { x: 606, y: -28 },
                                { x: 607, y: -37 },
                                { x: 608, y: -26 },
                                { x: 609, y: -14 },
                                { x: 610, y: -31 },
                                { x: 611, y: -45 },
                                { x: 612, y: -45 },
                                { x: 613, y: -43 },
                                { x: 614, y: -50 },
                                { x: 615, y: -59 },
                                { x: 616, y: -73 },
                                { x: 617, y: -79 },
                                { x: 618, y: -88 },
                                { x: 619, y: -92 },
                                { x: 620, y: -95 },
                                { x: 621, y: -101 },
                                { x: 622, y: -104 },
                                { x: 623, y: -124 },
                                { x: 624, y: -150 },
                                { x: 625, y: -152 },
                                { x: 626, y: -153 },
                                { x: 627, y: -174 },
                                { x: 628, y: -205 },
                                { x: 629, y: -215 },
                                { x: 630, y: -211 },
                                { x: 631, y: -214 },
                                { x: 632, y: -211 },
                                { x: 633, y: -222 },
                                { x: 634, y: -218 },
                                { x: 635, y: -211 },
                                { x: 636, y: -200 },
                                { x: 637, y: -200 },
                                { x: 638, y: -196 },
                                { x: 639, y: -184 },
                                { x: 640, y: -189 },
                                { x: 641, y: -202 },
                                { x: 642, y: -203 },
                                { x: 643, y: -202 },
                                { x: 644, y: -200 },
                                { x: 645, y: -205 },
                                { x: 646, y: -211 },
                                { x: 647, y: -226 },
                                { x: 648, y: -241 },
                                { x: 649, y: -242 },
                                { x: 650, y: -252 },
                                { x: 651, y: -273 },
                                { x: 652, y: -279 },
                                { x: 653, y: -288 },
                                { x: 654, y: -291 },
                                { x: 655, y: -289 },
                                { x: 656, y: -286 },
                                { x: 657, y: -269 },
                                { x: 658, y: -266 },
                                { x: 659, y: -280 },
                                { x: 660, y: -287 },
                                { x: 661, y: -277 },
                                { x: 662, y: -260 },
                                { x: 663, y: -271 },
                                { x: 664, y: -269 },
                                { x: 665, y: -271 },
                                { x: 666, y: -287 },
                                { x: 667, y: -299 },
                                { x: 668, y: -297 },
                                { x: 669, y: -288 },
                                { x: 670, y: -287 },
                                { x: 671, y: -287 },
                                { x: 672, y: -289 },
                                { x: 673, y: -287 },
                                { x: 674, y: -286 },
                                { x: 675, y: -276 },
                                { x: 676, y: -271 },
                                { x: 677, y: -266 },
                                { x: 678, y: -260 },
                                { x: 679, y: -252 },
                                { x: 680, y: -236 },
                                { x: 681, y: -223 },
                                { x: 682, y: -215 },
                                { x: 683, y: -213 },
                                { x: 684, y: -224 },
                                { x: 685, y: -230 },
                                { x: 686, y: -220 },
                                { x: 687, y: -209 },
                                { x: 688, y: -207 },
                                { x: 689, y: -194 },
                                { x: 690, y: -182 },
                                { x: 691, y: -181 },
                                { x: 692, y: -186 },
                                { x: 693, y: -189 },
                                { x: 694, y: -186 },
                                { x: 695, y: -174 },
                                { x: 696, y: -167 },
                                { x: 697, y: -161 },
                                { x: 698, y: -158 },
                                { x: 699, y: -155 },
                                { x: 700, y: -153 },
                                { x: 701, y: -139 },
                                { x: 702, y: -135 },
                                { x: 703, y: -130 },
                                { x: 704, y: -129 },
                                { x: 705, y: -116 },
                                { x: 706, y: -107 },
                                { x: 707, y: -98 },
                                { x: 708, y: -84 },
                                { x: 709, y: -85 },
                                { x: 710, y: -92 },
                                { x: 711, y: -100 },
                                { x: 712, y: -105 },
                                { x: 713, y: -97 },
                                { x: 714, y: -81 },
                                { x: 715, y: -72 },
                                { x: 716, y: -58 },
                                { x: 717, y: -49 },
                                { x: 718, y: -35 },
                                { x: 719, y: -33 },
                                { x: 720, y: -28 },
                                { x: 721, y: -13 },
                                { x: 722, y: -7 },
                                { x: 723, y: -9 },
                                { x: 724, y: -6 },
                                { x: 725, y: 10 },
                                { x: 726, y: 22 },
                                { x: 727, y: 16 },
                                { x: 728, y: 5 },
                                { x: 729, y: -12 },
                                { x: 730, y: -12 },
                                { x: 731, y: 1 },
                                { x: 732, y: 6 },
                                { x: 733, y: 17 },
                                { x: 734, y: 41 },
                                { x: 735, y: 52 },
                                { x: 736, y: 54 },
                                { x: 737, y: 57 },
                                { x: 738, y: 63 },
                                { x: 739, y: 81 },
                                { x: 740, y: 96 },
                                { x: 741, y: 107 },
                                { x: 742, y: 118 },
                                { x: 743, y: 133 },
                                { x: 744, y: 123 },
                                { x: 745, y: 121 },
                                { x: 746, y: 129 },
                                { x: 747, y: 128 },
                                { x: 748, y: 127 },
                                { x: 749, y: 112 },
                                { x: 750, y: 89 },
                                { x: 751, y: 0 },
                                { x: 752, y: 123 },
                                { x: 753, y: 42 },
                                { x: 754, y: 98 },
                                { x: 755, y: 109 },
                                { x: 756, y: 109 },
                                { x: 757, y: 108 },
                                { x: 758, y: 113 },
                                { x: 759, y: 121 },
                                { x: 760, y: 119 },
                                { x: 761, y: 119 },
                                { x: 762, y: 114 },
                                { x: 763, y: 112 },
                                { x: 764, y: 109 },
                                { x: 765, y: 107 },
                                { x: 766, y: 105 },
                                { x: 767, y: 114 },
                                { x: 768, y: 122 },
                                { x: 769, y: 130 },
                                { x: 770, y: 134 },
                                { x: 771, y: 121 },
                                { x: 772, y: 113 },
                                { x: 773, y: 100 },
                                { x: 774, y: 94 },
                                { x: 775, y: 114 },
                                { x: 776, y: 112 },
                                { x: 777, y: 108 },
                                { x: 778, y: 116 },
                                { x: 779, y: 114 },
                                { x: 780, y: 112 },
                                { x: 781, y: 118 },
                                { x: 782, y: 119 },
                                { x: 783, y: 116 },
                                { x: 784, y: 109 },
                                { x: 785, y: 110 },
                                { x: 786, y: 108 },
                                { x: 787, y: 113 },
                                { x: 788, y: 116 },
                                { x: 789, y: 118 },
                                { x: 790, y: 107 },
                                { x: 791, y: 103 },
                                { x: 792, y: 109 },
                                { x: 793, y: 110 },
                                { x: 794, y: 103 },
                                { x: 795, y: 106 },
                                { x: 796, y: 104 },
                                { x: 797, y: 93 },
                                { x: 798, y: 86 },
                                { x: 799, y: 77 },
                                { x: 800, y: 83 },
                                { x: 801, y: 87 },
                                { x: 802, y: 80 },
                                { x: 803, y: 95 },
                                { x: 804, y: 100 },
                                { x: 805, y: 88 },
                                { x: 806, y: 102 },
                                { x: 807, y: 87 },
                                { x: 808, y: 77 },
                                { x: 809, y: 88 },
                                { x: 810, y: 81 },
                                { x: 811, y: 71 },
                                { x: 812, y: 59 },
                                { x: 813, y: 61 },
                                { x: 814, y: 67 },
                                { x: 815, y: 76 },
                                { x: 816, y: 91 },
                                { x: 817, y: 94 },
                                { x: 818, y: 93 },
                                { x: 819, y: 89 },
                                { x: 820, y: 94 },
                                { x: 821, y: 98 },
                                { x: 822, y: 103 },
                                { x: 823, y: 95 },
                                { x: 824, y: 83 },
                                { x: 825, y: 89 },
                                { x: 826, y: 88 },
                                { x: 827, y: 96 },
                                { x: 828, y: 97 },
                                { x: 829, y: 97 },
                                { x: 830, y: 92 },
                                { x: 831, y: 88 },
                                { x: 832, y: 86 },
                                { x: 833, y: 84 },
                                { x: 834, y: 84 },
                                { x: 835, y: 76 },
                                { x: 836, y: 65 },
                                { x: 837, y: 52 },
                                { x: 838, y: 45 },
                                { x: 839, y: 47 },
                                { x: 840, y: 36 },
                                { x: 841, y: 33 },
                                { x: 842, y: 46 },
                                { x: 843, y: 46 },
                                { x: 844, y: 57 },
                                { x: 845, y: 53 },
                                { x: 846, y: 52 },
                                { x: 847, y: 56 },
                                { x: 848, y: 61 },
                                { x: 849, y: 64 },
                                { x: 850, y: 65 },
                                { x: 851, y: 59 },
                                { x: 852, y: 55 },
                                { x: 853, y: 60 },
                                { x: 854, y: 59 },
                                { x: 855, y: 61 },
                                { x: 856, y: 55 },
                                { x: 857, y: 51 },
                                { x: 858, y: 48 },
                                { x: 859, y: 46 },
                                { x: 860, y: 49 },
                                { x: 861, y: 47 },
                                { x: 862, y: 46 },
                                { x: 863, y: 44 },
                                { x: 864, y: 43 },
                                { x: 865, y: 46 },
                                { x: 866, y: 47 },
                                { x: 867, y: 45 },
                                { x: 868, y: 28 },
                                { x: 869, y: 17 },
                                { x: 870, y: 20 },
                                { x: 871, y: 24 },
                                { x: 872, y: 22 },
                                { x: 873, y: 38 },
                                { x: 874, y: 29 },
                                { x: 875, y: 23 },
                                { x: 876, y: 23 },
                                { x: 877, y: 9 },
                                { x: 878, y: 1 },
                                { x: 879, y: 15 },
                                { x: 880, y: 32 },
                                { x: 881, y: 38 },
                                { x: 882, y: 37 },
                                { x: 883, y: 38 },
                                { x: 884, y: 31 },
                                { x: 885, y: 18 },
                                { x: 886, y: 11 },
                                { x: 887, y: 5 },
                                { x: 888, y: 5 },
                                { x: 889, y: -1 },
                                { x: 890, y: -6 },
                                { x: 891, y: -8 },
                                { x: 892, y: -6 },
                                { x: 893, y: 5 },
                                { x: 894, y: 14 },
                                { x: 895, y: 8 },
                                { x: 896, y: 21 },
                                { x: 897, y: 35 },
                                { x: 898, y: 35 },
                                { x: 899, y: 32 },
                                { x: 900, y: 26 },
                                { x: 901, y: 28 },
                                { x: 902, y: 26 },
                                { x: 903, y: 24 },
                                { x: 904, y: 23 },
                                { x: 905, y: 28 },
                                { x: 906, y: 26 },
                                { x: 907, y: 27 },
                                { x: 908, y: 23 },
                                { x: 909, y: 32 },
                                { x: 910, y: 30 },
                                { x: 911, y: 19 },
                                { x: 912, y: 16 },
                                { x: 913, y: 25 },
                                { x: 914, y: 32 },
                                { x: 915, y: 20 },
                                { x: 916, y: 12 },
                                { x: 917, y: 8 },
                                { x: 918, y: 7 },
                                { x: 919, y: 14 },
                                { x: 920, y: 14 },
                                { x: 921, y: 11 },
                                { x: 922, y: 15 },
                                { x: 923, y: 4 },
                                { x: 924, y: -5 },
                                { x: 925, y: -3 },
                                { x: 926, y: -3 },
                                { x: 927, y: -11 },
                                { x: 928, y: -2 },
                                { x: 929, y: 18 },
                                { x: 930, y: 11 },
                                { x: 931, y: -2 },
                                { x: 932, y: 1 },
                                { x: 933, y: -9 },
                                { x: 934, y: -21 },
                                { x: 935, y: -13 },
                                { x: 936, y: -16 },
                                { x: 937, y: -4 },
                                { x: 938, y: 15 },
                                { x: 939, y: 31 },
                                { x: 940, y: 55 },
                                { x: 941, y: 52 },
                                { x: 942, y: 35 },
                                { x: 943, y: 23 },
                                { x: 944, y: 24 },
                                { x: 945, y: 20 },
                                { x: 946, y: 19 },
                                { x: 947, y: 18 },
                                { x: 948, y: 13 },
                                { x: 949, y: 6 },
                                { x: 950, y: 7 },
                                { x: 951, y: 12 },
                                { x: 952, y: 12 },
                                { x: 953, y: 3 },
                                { x: 954, y: 2 },
                                { x: 955, y: -4 },
                                { x: 956, y: -11 },
                                { x: 957, y: -12 },
                                { x: 958, y: -9 },
                                { x: 959, y: -17 },
                                { x: 960, y: -6 },
                                { x: 961, y: 1 },
                                { x: 962, y: -2 },
                                { x: 963, y: -6 },
                                { x: 964, y: -18 },
                                { x: 965, y: -17 },
                                { x: 966, y: -14 },
                                { x: 967, y: -13 },
                                { x: 968, y: -11 },
                                { x: 969, y: 9 },
                                { x: 970, y: 9 },
                                { x: 971, y: 2 },
                                { x: 972, y: -2 },
                                { x: 973, y: -14 },
                                { x: 974, y: -27 },
                                { x: 975, y: -24 },
                                { x: 976, y: -16 },
                                { x: 977, y: -10 },
                                { x: 978, y: -3 },
                                { x: 979, y: 2 },
                                { x: 980, y: 7 },
                                { x: 981, y: 16 },
                                { x: 982, y: 29 },
                                { x: 983, y: 40 },
                                { x: 984, y: 47 },
                                { x: 985, y: 46 },
                                { x: 986, y: 30 },
                                { x: 987, y: 19 },
                                { x: 988, y: 20 },
                                { x: 989, y: 21 },
                                { x: 990, y: 22 },
                                { x: 991, y: 12 },
                                { x: 992, y: 0 },
                                { x: 993, y: -6 },
                                { x: 994, y: -6 },
                                { x: 995, y: -11 },
                                { x: 996, y: -9 },
                                { x: 997, y: -5 },
                                { x: 998, y: -9 },
                                { x: 999, y: -15 },
                                { x: 1000, y: -18 },
                                { x: 1001, y: -21 },
                                { x: 1002, y: -19 },
                                { x: 1003, y: -27 },
                                { x: 1004, y: -31 },
                                { x: 1005, y: -32 },
                                { x: 1006, y: -35 },
                                { x: 1007, y: -31 },
                                { x: 1008, y: -26 },
                                { x: 1009, y: -26 },
                                { x: 1010, y: -19 },
                                { x: 1011, y: -6 },
                                { x: 1012, y: 0 },
                                { x: 1013, y: -3 },
                                { x: 1014, y: -16 },
                                { x: 1015, y: -16 },
                                { x: 1016, y: -3 },
                                { x: 1017, y: 5 },
                                { x: 1018, y: 13 },
                                { x: 1019, y: 6 },
                                { x: 1020, y: 9 },
                                { x: 1021, y: 18 },
                                { x: 1022, y: 40 },
                                { x: 1023, y: 54 },
                                { x: 1024, y: 64 },
                                { x: 1025, y: 68 },
                                { x: 1026, y: 57 },
                                { x: 1027, y: 47 },
                                { x: 1028, y: 41 },
                                { x: 1029, y: 41 },
                                { x: 1030, y: 50 },
                                { x: 1031, y: 54 },
                                { x: 1032, y: 35 },
                                { x: 1033, y: 33 },
                                { x: 1034, y: 33 },
                                { x: 1035, y: 27 },
                                { x: 1036, y: 26 },
                                { x: 1037, y: 19 },
                                { x: 1038, y: 16 },
                                { x: 1039, y: 28 },
                                { x: 1040, y: 44 },
                                { x: 1041, y: 38 },
                                { x: 1042, y: 42 },
                                { x: 1043, y: 57 },
                                { x: 1044, y: 61 },
                                { x: 1045, y: 65 },
                                { x: 1046, y: 55 },
                                { x: 1047, y: 45 },
                                { x: 1048, y: 33 },
                                { x: 1049, y: 21 },
                                { x: 1050, y: 11 },
                                { x: 1051, y: 5 },
                                { x: 1052, y: -14 },
                                { x: 1053, y: -30 },
                                { x: 1054, y: -35 },
                                { x: 1055, y: -31 },
                                { x: 1056, y: -32 },
                                { x: 1057, y: -33 },
                                { x: 1058, y: -25 },
                                { x: 1059, y: -19 },
                                { x: 1060, y: -18 },
                                { x: 1061, y: -30 },
                                { x: 1062, y: -42 },
                                { x: 1063, y: -38 },
                                { x: 1064, y: -44 },
                                { x: 1065, y: -49 },
                                { x: 1066, y: -43 },
                                { x: 1067, y: -41 },
                                { x: 1068, y: -30 },
                                { x: 1069, y: -26 },
                                { x: 1070, y: -29 },
                                { x: 1071, y: -33 },
                                { x: 1072, y: -53 },
                                { x: 1073, y: -58 },
                                { x: 1074, y: -58 },
                                { x: 1075, y: -45 },
                                { x: 1076, y: -37 },
                                { x: 1077, y: -39 },
                                { x: 1078, y: -51 },
                                { x: 1079, y: -50 },
                                { x: 1080, y: -52 },
                                { x: 1081, y: -53 },
                                { x: 1082, y: -36 },
                                { x: 1083, y: -27 },
                                { x: 1084, y: -29 },
                                { x: 1085, y: -24 },
                                { x: 1086, y: -27 },
                                { x: 1087, y: -34 },
                                { x: 1088, y: -46 },
                                { x: 1089, y: -49 },
                                { x: 1090, y: -42 },
                                { x: 1091, y: -50 },
                                { x: 1092, y: -49 },
                                { x: 1093, y: -50 },
                                { x: 1094, y: -42 },
                                { x: 1095, y: -35 },
                                { x: 1096, y: -24 },
                                { x: 1097, y: -33 },
                                { x: 1098, y: -40 },
                                { x: 1099, y: -36 },
                                { x: 1100, y: -37 },
                                { x: 1101, y: -38 },
                                { x: 1102, y: -51 },
                                { x: 1103, y: -61 },
                                { x: 1104, y: -67 },
                                { x: 1105, y: -75 },
                                { x: 1106, y: -81 },
                                { x: 1107, y: -70 },
                                { x: 1108, y: -66 },
                                { x: 1109, y: -71 },
                                { x: 1110, y: -72 },
                                { x: 1111, y: -57 },
                                { x: 1112, y: -48 },
                                { x: 1113, y: -40 },
                                { x: 1114, y: -31 },
                                { x: 1115, y: 0 },
                                { x: 1116, y: 31 },
                                { x: 1117, y: -63 },
                                { x: 1118, y: -16 },
                                { x: 1119, y: -22 },
                                { x: 1120, y: -30 },
                                { x: 1121, y: -36 },
                                { x: 1122, y: -37 },
                                { x: 1123, y: -42 },
                                { x: 1124, y: -40 },
                                { x: 1125, y: -47 },
                                { x: 1126, y: -38 },
                                { x: 1127, y: -5 },
                                { x: 1128, y: 2 },
                                { x: 1129, y: -9 },
                                { x: 1130, y: -2 },
                                { x: 1131, y: 7 },
                                { x: 1132, y: 11 },
                                { x: 1133, y: 12 },
                                { x: 1134, y: 22 },
                                { x: 1135, y: 26 },
                                { x: 1136, y: 29 },
                                { x: 1137, y: 21 },
                                { x: 1138, y: 25 },
                                { x: 1139, y: 32 },
                                { x: 1140, y: 35 },
                                { x: 1141, y: 36 },
                                { x: 1142, y: 48 },
                                { x: 1143, y: 74 },
                                { x: 1144, y: 79 },
                                { x: 1145, y: 78 },
                                { x: 1146, y: 92 },
                                { x: 1147, y: 108 },
                                { x: 1148, y: 120 },
                                { x: 1149, y: 143 },
                                { x: 1150, y: 172 },
                                { x: 1151, y: 201 },
                                { x: 1152, y: 232 },
                                { x: 1153, y: 285 },
                                { x: 1154, y: 363 },
                                { x: 1155, y: 447 },
                                { x: 1156, y: 514 },
                                { x: 1157, y: 573 },
                                { x: 1158, y: 663 },
                                { x: 1159, y: 754 },
                                { x: 1160, y: 815 },
                                { x: 1161, y: 859 },
                                { x: 1162, y: 895 },
                                { x: 1163, y: 940 },
                                { x: 1164, y: 977 },
                                { x: 1165, y: 972 },
                                { x: 1166, y: 945 },
                                { x: 1167, y: 898 },
                                { x: 1168, y: 808 },
                                { x: 1169, y: 686 },
                                { x: 1170, y: 532 },
                                { x: 1171, y: 360 },
                                { x: 1172, y: 167 },
                                { x: 1173, y: -33 },
                                { x: 1174, y: -232 },
                                { x: 1175, y: -472 },
                                { x: 1176, y: -766 },
                                { x: 1177, y: -1082 },
                                { x: 1178, y: -1295 },
                                { x: 1179, y: -1438 },
                                { x: 1180, y: -1509 },
                                { x: 1181, y: -1567 },
                                { x: 1182, y: -1594 },
                                { x: 1183, y: -1583 },
                                { x: 1184, y: -1569 },
                                { x: 1185, y: -1547 },
                                { x: 1186, y: -1504 },
                                { x: 1187, y: -1457 },
                                { x: 1188, y: -1432 },
                                { x: 1189, y: -1403 },
                                { x: 1190, y: -1335 },
                                { x: 1191, y: -1249 },
                                { x: 1192, y: -1157 },
                                { x: 1193, y: -1058 },
                                { x: 1194, y: -957 },
                                { x: 1195, y: -835 },
                                { x: 1196, y: -733 },
                                { x: 1197, y: -650 },
                                { x: 1198, y: -567 },
                                { x: 1199, y: -508 },
                                { x: 1200, y: -446 },
                                { x: 1201, y: -378 },
                                { x: 1202, y: -304 },
                                { x: 1203, y: -240 },
                                { x: 1204, y: -180 },
                                { x: 1205, y: -123 },
                                { x: 1206, y: -63 },
                                { x: 1207, y: -11 },
                                { x: 1208, y: 46 },
                                { x: 1209, y: 112 },
                                { x: 1210, y: 181 },
                                { x: 1211, y: 221 },
                                { x: 1212, y: 256 },
                                { x: 1213, y: 283 },
                                { x: 1214, y: 318 },
                                { x: 1215, y: 348 },
                                { x: 1216, y: 371 },
                                { x: 1217, y: 397 },
                                { x: 1218, y: 410 },
                                { x: 1219, y: 409 },
                                { x: 1220, y: 424 },
                                { x: 1221, y: 440 },
                                { x: 1222, y: 443 },
                                { x: 1223, y: 429 },
                                { x: 1224, y: 420 },
                                { x: 1225, y: 424 },
                                { x: 1226, y: 429 },
                                { x: 1227, y: 415 },
                                { x: 1228, y: 394 },
                                { x: 1229, y: 391 },
                                { x: 1230, y: 402 },
                                { x: 1231, y: 410 },
                                { x: 1232, y: 410 },
                                { x: 1233, y: 408 },
                                { x: 1234, y: 408 },
                                { x: 1235, y: 405 },
                                { x: 1236, y: 399 },
                                { x: 1237, y: 392 },
                                { x: 1238, y: 383 },
                                { x: 1239, y: 376 },
                                { x: 1240, y: 375 },
                                { x: 1241, y: 368 },
                                { x: 1242, y: 366 },
                                { x: 1243, y: 363 },
                                { x: 1244, y: 353 },
                                { x: 1245, y: 345 },
                                { x: 1246, y: 334 },
                                { x: 1247, y: 326 },
                                { x: 1248, y: 317 },
                                { x: 1249, y: 313 },
                                { x: 1250, y: 320 },
                                { x: 1251, y: 318 },
                                { x: 1252, y: 300 },
                                { x: 1253, y: 275 },
                                { x: 1254, y: 262 },
                                { x: 1255, y: 252 },
                                { x: 1256, y: 239 },
                                { x: 1257, y: 236 },
                                { x: 1258, y: 231 },
                                { x: 1259, y: 236 },
                                { x: 1260, y: 240 },
                                { x: 1261, y: 235 },
                                { x: 1262, y: 222 },
                                { x: 1263, y: 220 },
                                { x: 1264, y: 223 },
                                { x: 1265, y: 228 },
                                { x: 1266, y: 222 },
                                { x: 1267, y: 205 },
                                { x: 1268, y: 197 },
                                { x: 1269, y: 191 },
                                { x: 1270, y: 180 },
                                { x: 1271, y: 185 },
                                { x: 1272, y: 174 },
                                { x: 1273, y: 170 },
                                { x: 1274, y: 166 },
                                { x: 1275, y: 161 },
                                { x: 1276, y: 151 },
                                { x: 1277, y: 148 },
                                { x: 1278, y: 142 },
                                { x: 1279, y: 136 },
                                { x: 1280, y: 131 },
                                { x: 1281, y: 127 },
                                { x: 1282, y: 118 },
                                { x: 1283, y: 111 },
                                { x: 1284, y: 114 },
                                { x: 1285, y: 110 },
                                { x: 1286, y: 97 },
                                { x: 1287, y: 85 },
                                { x: 1288, y: 72 },
                                { x: 1289, y: 65 },
                                { x: 1290, y: 61 },
                                { x: 1291, y: 62 },
                                { x: 1292, y: 64 },
                                { x: 1293, y: 61 },
                                { x: 1294, y: 59 },
                                { x: 1295, y: 56 },
                                { x: 1296, y: 64 },
                                { x: 1297, y: 55 },
                                { x: 1298, y: 56 },
                                { x: 1299, y: 73 },
                                { x: 1300, y: 63 },
                                { x: 1301, y: 56 },
                                { x: 1302, y: 54 },
                                { x: 1303, y: 35 },
                                { x: 1304, y: 12 },
                                { x: 1305, y: -3 },
                                { x: 1306, y: -2 },
                                { x: 1307, y: -9 },
                                { x: 1308, y: -15 },
                                { x: 1309, y: -13 },
                                { x: 1310, y: 1 },
                                { x: 1311, y: 27 },
                                { x: 1312, y: 48 },
                                { x: 1313, y: 53 },
                                { x: 1314, y: 55 },
                                { x: 1315, y: 54 },
                                { x: 1316, y: 27 },
                                { x: 1317, y: 20 },
                                { x: 1318, y: 14 },
                                { x: 1319, y: 10 },
                                { x: 1320, y: 3 },
                                { x: 1321, y: 9 },
                                { x: 1322, y: 21 },
                                { x: 1323, y: 15 },
                                { x: 1324, y: 9 },
                                { x: 1325, y: 5 },
                                { x: 1326, y: 0 },
                                { x: 1327, y: 0 },
                                { x: 1328, y: -1 },
                                { x: 1329, y: 3 },
                                { x: 1330, y: 4 },
                                { x: 1331, y: 2 },
                                { x: 1332, y: -4 },
                                { x: 1333, y: -5 },
                                { x: 1334, y: -12 },
                                { x: 1335, y: -14 },
                                { x: 1336, y: -20 },
                                { x: 1337, y: -23 },
                                { x: 1338, y: -21 },
                                { x: 1339, y: -20 },
                                { x: 1340, y: -28 },
                                { x: 1341, y: -25 },
                                { x: 1342, y: -25 },
                                { x: 1343, y: -30 },
                                { x: 1344, y: -34 },
                                { x: 1345, y: -43 },
                                { x: 1346, y: -39 },
                                { x: 1347, y: -36 },
                                { x: 1348, y: -44 },
                                { x: 1349, y: -28 },
                                { x: 1350, y: -22 },
                                { x: 1351, y: -11 },
                                { x: 1352, y: -12 },
                                { x: 1353, y: -7 },
                                { x: 1354, y: -14 },
                                { x: 1355, y: -11 },
                                { x: 1356, y: -13 },
                                { x: 1357, y: -19 },
                                { x: 1358, y: -19 },
                                { x: 1359, y: -13 },
                                { x: 1360, y: 4 },
                                { x: 1361, y: 19 },
                                { x: 1362, y: 16 },
                                { x: 1363, y: 19 },
                                { x: 1364, y: 21 },
                                { x: 1365, y: 22 },
                                { x: 1366, y: 5 },
                                { x: 1367, y: -12 },
                                { x: 1368, y: -27 },
                                { x: 1369, y: -24 },
                                { x: 1370, y: -16 },
                                { x: 1371, y: -15 },
                                { x: 1372, y: -2 },
                                { x: 1373, y: 8 },
                                { x: 1374, y: -1 },
                                { x: 1375, y: -7 },
                                { x: 1376, y: -1 },
                                { x: 1377, y: 12 },
                                { x: 1378, y: 26 },
                                { x: 1379, y: 27 },
                                { x: 1380, y: 32 },
                                { x: 1381, y: 27 },
                                { x: 1382, y: 19 },
                                { x: 1383, y: 22 },
                                { x: 1384, y: 30 },
                                { x: 1385, y: 36 },
                                { x: 1386, y: 38 },
                                { x: 1387, y: 43 },
                                { x: 1388, y: 46 },
                                { x: 1389, y: 40 },
                                { x: 1390, y: 35 },
                                { x: 1391, y: 27 },
                                { x: 1392, y: 24 },
                                { x: 1393, y: 23 },
                                { x: 1394, y: 16 },
                                { x: 1395, y: 7 },
                                { x: 1396, y: 7 },
                                { x: 1397, y: 11 },
                                { x: 1398, y: 16 },
                                { x: 1399, y: 10 },
                                { x: 1400, y: 6 },
                                { x: 1401, y: 3 },
                                { x: 1402, y: 3 },
                                { x: 1403, y: 12 },
                                { x: 1404, y: 12 },
                                { x: 1405, y: 8 },
                                { x: 1406, y: 5 },
                                { x: 1407, y: 4 },
                                { x: 1408, y: 4 },
                                { x: 1409, y: 4 },
                                { x: 1410, y: 12 },
                                { x: 1411, y: 22 },
                                { x: 1412, y: 9 },
                                { x: 1413, y: 0 },
                                { x: 1414, y: -6 },
                                { x: 1415, y: -23 },
                                { x: 1416, y: -25 },
                                { x: 1417, y: -25 },
                                { x: 1418, y: -31 },
                                { x: 1419, y: -45 },
                                { x: 1420, y: -51 },
                                { x: 1421, y: -50 },
                                { x: 1422, y: -46 },
                                { x: 1423, y: -55 },
                                { x: 1424, y: -59 },
                                { x: 1425, y: -60 },
                                { x: 1426, y: -63 },
                                { x: 1427, y: -62 },
                                { x: 1428, y: -62 },
                                { x: 1429, y: -63 },
                                { x: 1430, y: -75 },
                                { x: 1431, y: -85 },
                                { x: 1432, y: -92 },
                                { x: 1433, y: -94 },
                                { x: 1434, y: -87 },
                                { x: 1435, y: -85 },
                                { x: 1436, y: -77 },
                                { x: 1437, y: -91 },
                                { x: 1438, y: -106 },
                                { x: 1439, y: -114 },
                                { x: 1440, y: -121 },
                                { x: 1441, y: -133 },
                                { x: 1442, y: -146 },
                                { x: 1443, y: -159 },
                                { x: 1444, y: -168 },
                                { x: 1445, y: -167 },
                                { x: 1446, y: -179 },
                                { x: 1447, y: -190 },
                                { x: 1448, y: -200 },
                                { x: 1449, y: -205 },
                                { x: 1450, y: -210 },
                                { x: 1451, y: -213 },
                                { x: 1452, y: -210 },
                                { x: 1453, y: -217 },
                                { x: 1454, y: -219 },
                                { x: 1455, y: -225 },
                                { x: 1456, y: -239 },
                                { x: 1457, y: -246 },
                                { x: 1458, y: -257 },
                                { x: 1459, y: -276 },
                                { x: 1460, y: -298 },
                                { x: 1461, y: -297 },
                                { x: 1462, y: -305 },
                                { x: 1463, y: -312 },
                                { x: 1464, y: -305 },
                                { x: 1465, y: -300 },
                                { x: 1466, y: -312 },
                                { x: 1467, y: -321 },
                                { x: 1468, y: -318 },
                                { x: 1469, y: -306 },
                                { x: 1470, y: -302 },
                                { x: 1471, y: -296 },
                                { x: 1472, y: -297 },
                                { x: 1473, y: -294 },
                                { x: 1474, y: -287 },
                                { x: 1475, y: -290 },
                                { x: 1476, y: -301 },
                                { x: 1477, y: -310 },
                                { x: 1478, y: -312 },
                                { x: 1479, y: 0 },
                                { x: 1480, y: -265 },
                                { x: 1481, y: -362 },
                                { x: 1482, y: -303 },
                                { x: 1483, y: -302 },
                                { x: 1484, y: -293 },
                                { x: 1485, y: -296 },
                                { x: 1486, y: -295 },
                                { x: 1487, y: -286 },
                                { x: 1488, y: -291 },
                                { x: 1489, y: -287 },
                                { x: 1490, y: -268 },
                                { x: 1491, y: -243 },
                                { x: 1492, y: -231 },
                                { x: 1493, y: -229 },
                                { x: 1494, y: -228 },
                                { x: 1495, y: -229 },
                                { x: 1496, y: -236 },
                                { x: 1497, y: -243 },
                                { x: 1498, y: -242 },
                                { x: 1499, y: -217 },
                                { x: 1500, y: -206 },
                                { x: 1501, y: -199 },
                                { x: 1502, y: -189 },
                                { x: 1503, y: -187 },
                                { x: 1504, y: -178 },
                                { x: 1505, y: -163 },
                                { x: 1506, y: -152 },
                                { x: 1507, y: -150 },
                                { x: 1508, y: -149 },
                                { x: 1509, y: -144 },
                                { x: 1510, y: -137 },
                                { x: 1511, y: -121 },
                                { x: 1512, y: -119 },
                                { x: 1513, y: -132 },
                                { x: 1514, y: -126 },
                                { x: 1515, y: -123 },
                                { x: 1516, y: -104 },
                                { x: 1517, y: -96 },
                                { x: 1518, y: -97 },
                                { x: 1519, y: -82 },
                                { x: 1520, y: -56 },
                                { x: 1521, y: -42 },
                                { x: 1522, y: -47 },
                                { x: 1523, y: -40 },
                                { x: 1524, y: -33 },
                                { x: 1525, y: -37 },
                                { x: 1526, y: -43 },
                                { x: 1527, y: -51 },
                                { x: 1528, y: -51 },
                                { x: 1529, y: -35 },
                                { x: 1530, y: -19 },
                                { x: 1531, y: -12 },
                                { x: 1532, y: -11 },
                                { x: 1533, y: -9 },
                                { x: 1534, y: -2 },
                                { x: 1535, y: 12 },
                                { x: 1536, y: 24 },
                                { x: 1537, y: 24 },
                                { x: 1538, y: 32 },
                                { x: 1539, y: 42 },
                                { x: 1540, y: 39 },
                                { x: 1541, y: 45 },
                                { x: 1542, y: 55 },
                                { x: 1543, y: 43 },
                                { x: 1544, y: 42 },
                                { x: 1545, y: 45 },
                                { x: 1546, y: 46 },
                                { x: 1547, y: 51 },
                                { x: 1548, y: 58 },
                                { x: 1549, y: 61 },
                                { x: 1550, y: 57 },
                                { x: 1551, y: 55 },
                                { x: 1552, y: 46 },
                                { x: 1553, y: 29 },
                                { x: 1554, y: 23 },
                                { x: 1555, y: 24 },
                                { x: 1556, y: 21 },
                                { x: 1557, y: 27 },
                                { x: 1558, y: 45 },
                                { x: 1559, y: 56 },
                                { x: 1560, y: 79 },
                                { x: 1561, y: 102 },
                                { x: 1562, y: 103 },
                                { x: 1563, y: 114 },
                                { x: 1564, y: 129 },
                                { x: 1565, y: 116 },
                                { x: 1566, y: 112 },
                                { x: 1567, y: 127 },
                                { x: 1568, y: 130 },
                                { x: 1569, y: 135 },
                                { x: 1570, y: 143 },
                                { x: 1571, y: 144 },
                                { x: 1572, y: 157 },
                                { x: 1573, y: 153 },
                                { x: 1574, y: 140 },
                                { x: 1575, y: 135 },
                                { x: 1576, y: 129 },
                                { x: 1577, y: 111 },
                                { x: 1578, y: 103 },
                                { x: 1579, y: 106 },
                                { x: 1580, y: 108 },
                                { x: 1581, y: 94 },
                                { x: 1582, y: 96 },
                                { x: 1583, y: 89 },
                                { x: 1584, y: 85 },
                                { x: 1585, y: 89 },
                                { x: 1586, y: 94 },
                                { x: 1587, y: 82 }
                            
                            ]
                            // Create a data generator to supply a continuous stream of data.
                            createSampledDataGenerator(point, 1, 10)
                                .setSamplingFrequency(1)
                                .setInputData(point)
                                .generate()
                                .setStreamBatchSize(48)
                                .setStreamInterval(50)
                                .setStreamRepeat(true)
                                .toStream()
                                .forEach(point => {
                                    // Push the created points to the series.
                                    series.add({ x: point.timestamp, y: point.data.y })
                                })"),
                'Example_2' => array (
                    'chart_name' => 'Markers',
                    'icon' => plugins_url().'/LC-JS/images/icons/markers.png',
                    'html_code' => '<div id="target" class="row content"></div>',
                    'javascript_code' => " const {
                        lightningChart,
                        AutoCursorModes,
                        ColorRGBA,
                        UIVisibilityModes,
                        MarkerBuilders,
                        UIBackgrounds,
                        UIDirections,
                        UIOrigins,
                        DataPatterns,
                        UIElementBuilders,
                        UIDraggingModes,
                        Themes
                    } = lcjs
                    
                    // Import data-generator from 'xydata'-library.
                    const {
                        createProgressiveTraceGenerator
                    } = xydata
                    
                    const chartTitle = 'Markers'
                    
                    // Create a XY Chart.
                    const chart = lightningChart(license_key).ChartXY({
                        // theme: Themes.dark 
                        container: 'target'
                    })
                        .setTitle(chartTitle)
                        // Disable AutoCursor just for focusing on Markers.
                        .setAutoCursorMode(AutoCursorModes.disabled)
                        // Preventing ResultTable from getting cut at the edge
                        .setPadding({
                            right: 50
                        })
                    
                    // Add a progressive line series.
                    const series = chart.addLineSeries({ dataPattern: DataPatterns.horizontalProgressive })
                    
                    // Generate random progressive points using 'xydata'-library.
                    createProgressiveTraceGenerator()
                        .setNumberOfPoints(100)
                        .generate()
                        .toPromise()
                        .then(data => {
                            const axisYAvg = (data[0].y + data[data.length - 1].y) / 2;
                            series.add(data)
                            // ----- ChartMarker -----
                    
                            // Add a ChartMarker to the chart.
                            const chartMarker = chart.addChartMarkerXY()
                                .setPosition({ x: 60, y: axisYAvg })
                    
                            // Style ChartMarker.
                            chartMarker
                                .setResultTableVisibility(UIVisibilityModes.always)
                                .setResultTable((table) => table
                                    .setContent([
                                        ['ChartMarker']
                                    ])
                                )
                                .setGridStrokeXVisibility(UIVisibilityModes.whenDragged)
                                .setGridStrokeYVisibility(UIVisibilityModes.whenDragged)
                                .setTickMarkerXVisibility(UIVisibilityModes.whenDragged)
                                .setTickMarkerYVisibility(UIVisibilityModes.whenDragged)
                        })
                    
                    // ----- SeriesMarker -----
                    
                    // Create a builder for SeriesMarker to allow for full modification of its structure.
                    const SeriesMarkerBuilder = MarkerBuilders.XY
                        .setPointMarker(UIBackgrounds.Circle)
                        .setResultTableBackground(UIBackgrounds.Pointer)
                        .addStyler(marker => marker
                            .setPointMarker(point => point
                                .setSize({ x: 5, y: 5 })
                            )
                            .setResultTable(table => table
                                .setOrigin(UIOrigins.CenterBottom)
                                .setMargin({ bottom: 0 })
                                .setBackground(arrow => arrow
                                    .setDirection(UIDirections.Down)
                                    .setPointerAngle(80)
                                    .setPointerLength(20)
                                )
                            )
                            .setGridStrokeXCut(true)
                            .setAutoFitStrategy(undefined)
                        )
                    
                    // Add a SeriesMarker to the series.
                    const seriesMarker = series.addMarker(SeriesMarkerBuilder)
                        .setPosition({ x: 50, y: 0 })
                    
                    // Currently the only way to affect the text of Markers ResultTables,
                    // is to completely override the series parser for it.
                    series.setResultTableFormatter((tableBuilder, series, x, y) => tableBuilder
                        .addRow('SeriesMarker')
                        .addRow('X', x.toFixed(1))
                        .addRow('Y', y.toFixed(1))
                    )
                    // ... However, this will also apply to AutoCursor.
                    
                    // Add download button to save chart frame
                    chart.addUIElement(UIElementBuilders.ButtonBox.setBackground(UIBackgrounds.Rectangle))
                        .setPosition({ x: 99, y: 99 })
                        .setOrigin(UIOrigins.RightTop)
                        .setText('Download PNG Image')
                        .setPadding({ top: 5, right: 20, bottom: 5, left: 20 })
                        .setButtonOffSize(0)
                        .setButtonOnSize(0)
                        .setDraggingMode(UIDraggingModes.notDraggable)
                        .onMouseClick((event) => {
                            chart.saveToFile(chartTitle + ' - Screenshot')
                        })"),
                        'Example_3' => array (
                            'chart_name' => 'Bands And Constant Lines',
                            'icon' => plugins_url().'/LC-JS/images/icons/bandsConstantLines.png',
                            'html_code' => '<div id="target" class="row content"></div>',
                            'javascript_code' => " const {
                                lightningChart,
                                DataPatterns,
                                UIOrigins,
                                ColorHEX,
                                SolidLine,
                                SolidFill,
                                Themes
                            } = lcjs
                            
                            // Import data-generator from 'xydata'-library.
                            const {
                                createProgressiveTraceGenerator
                            } = xydata
                            const chartTitle = 'Bands and Constantlines'
                            
                            // Create a XY Chart.
                            const chart = lightningChart(license_key).ChartXY({
                                // theme: Themes.dark,
                                container: 'target',
                            })
                                .setTitle(chartTitle)
                            
                            // Add a progressive line series.
                            const series = chart.addLineSeries({ dataPattern: DataPatterns.horizontalProgressive })
                            
                            // Generate random progressive points using 'xydata'-library.
                            createProgressiveTraceGenerator()
                                .setNumberOfPoints(100)
                                .generate()
                                .toPromise()
                                .then(data => {
                                    series.add(data)
                                })
                            
                            // Get the default X and Y Axis
                            const xAxis = chart.getDefaultAxisX()
                            const yAxis = chart.getDefaultAxisY()
                                // Set the interval for Y Axis.
                                .setInterval(-10, 10, true, true)
                            
                            // Add a Constantline to the X Axis
                            const xAxisConstantline = xAxis.addConstantLine()
                            // Position the Constantline in the Axis Scale
                            xAxisConstantline.setValue(80)
                            // The name of the Constantline will be shown in the LegendBox
                            xAxisConstantline.setName('X Axis Constantline')
                            
                            // Add a Band to the X Axis
                            const xAxisBand = xAxis.addBand()
                            // Set the start and end values of the Band.
                            xAxisBand
                                .setValueStart(10)
                                .setValueEnd(25)
                                // Set the name of the Band
                                .setName('X Axis Band')
                            
                            // Add Band and ConstantLine to the Y Axis
                            
                            // If 'false' is given as argument here, the Constantline will be rendered behind
                            // all the Series in the Chart.
                            const yAxisConstantLine = yAxis.addConstantLine(false)
                            yAxisConstantLine.setName('Y Axis Constantline')
                            // Giving 'false' as argument here makes sure the Band is rendered behind all
                            // the Series in the Chart.
                            const yAxisBand = yAxis.addBand(false)
                            yAxisBand.setName('Y Axis Band')
                            
                            // Position the Y Axis ConstantLine along the visible Scale of the Axis.
                            yAxisConstantLine.setValue(9)
                            
                            // Position the Y Axis Band along the visible Scale of the Axis.
                            yAxisBand
                                .setValueEnd(2)
                                .setValueStart(-3)
                            
                            // Style the Y Axis Band
                            yAxisBand
                                .setStrokeStyle(
                                    new SolidLine({
                                        thickness: 3,
                                        fillStyle: new SolidFill({ color: ColorHEX('#6a05') })
                                    })
                                )
                            yAxisBand
                                .setFillStyle(
                                    new SolidFill({ color: ColorHEX('#5b19') })
                                )
                            
                            // Style the Y Axis Constantline
                            yAxisConstantLine.setStrokeStyle(
                                new SolidLine({
                                    thickness: 6,
                                    fillStyle: new SolidFill({ color: ColorHEX('#9c5') })
                                }))
                            
                            // Add a LegendBox, add the Chart in it and position it.
                            chart.addLegendBox()
                                .setPosition({ x: 5, y: 95 })
                                .setOrigin(UIOrigins.LeftTop)
                                .add(chart)"),
        ),
        'trading' =>array(
            'Example_1' => array(
              'chart_name' => 'OHLC Chart',
              'icon' => plugins_url().'/LC-JS/images/icons/ohlc.png',
              'html_code' => '<div id="target" class="row content"></div>',
              'javascript_code' => "const {
                lightningChart,
                AxisTickStrategies,
                OHLCFigures,
                AxisScrollStrategies,
                emptyLine,
                Themes
            } = lcjs
            
            // Create a XY Chart.
            const chart = lightningChart(license_key).ChartXY({
                // theme: Themes.dark
                container: 'target',
            })
            // Use DateTime TickStrategy for the X Axis, set current date as the origin.
            chart
                .getDefaultAxisX()
                .setTickStrategy(
                    AxisTickStrategies.DateTime,
                    (tickStrategy) => tickStrategy.setDateOrigin(new Date())
                )
            
            chart.setTitle('Open-High-Low-Close')
            // Modify AutoCursor to only show TickMarker and GridLine over the X Axis.
            chart.setAutoCursor(cursor => {
                cursor.disposeTickMarkerY()
                cursor.setGridStrokeYStyle(emptyLine)
            })
            
            // Add a OHLC Series with Bar as type of figures.
            const series = chart.addOHLCSeries({ positiveFigure: OHLCFigures.Bar })
                .setName('Open-High-Low-Close')
            // Create an array of XOHLC tuples (See API documentation) to use with the OHLC Series
            //The XOHLC tuple consists of five different values; position on X Axis (in this case, the date of each entry), the open, high, low and close values.
            const data = [
                [new Date(2019, 6, 29).getTime(), 208.46, 210.64, 208.44, 209.68],
                [new Date(2019, 6, 30).getTime(), 208.76, 210.16, 207.31, 208.78],
                [new Date(2019, 6, 31).getTime(), 216.42, 221.37, 211.3, 213.04],
                [new Date(2019, 7, 1).getTime(), 213.9, 218.03, 206.74, 208.43],
                [new Date(2019, 7, 2).getTime(), 205.53, 206.43, 201.63, 204.02],
                [new Date(2019, 7, 5).getTime(), 197.99, 198.65, 192.58, 193.34],
                [new Date(2019, 7, 6).getTime(), 196.31, 198.07, 194.04, 197],
                [new Date(2019, 7, 7).getTime(), 195.41, 199.56, 193.82, 199.04],
                [new Date(2019, 7, 8).getTime(), 200.2, 203.53, 199.39, 203.43],
                [new Date(2019, 7, 9).getTime(), 201.3, 202.76, 199.29, 200.99],
                [new Date(2019, 7, 12).getTime(), 199.62, 202.05, 199.15, 200.48],
                [new Date(2019, 7, 13).getTime(), 201.02, 212.14, 200.48, 208.97],
                [new Date(2019, 7, 14).getTime(), 203.16, 206.44, 202.59, 202.75],
                [new Date(2019, 7, 15).getTime(), 203.46, 205.14, 199.67, 201.74],
                [new Date(2019, 7, 16).getTime(), 204.28, 207.16, 203.84, 206.5],
                [new Date(2019, 7, 19).getTime(), 210.62, 212.73, 210.03, 210.35],
                [new Date(2019, 7, 20).getTime(), 210.88, 213.35, 210.32, 210.36],
                [new Date(2019, 7, 21).getTime(), 212.99, 213.65, 211.6, 212.64],
                [new Date(2019, 7, 22).getTime(), 213.19, 214.44, 210.75, 212.46],
                [new Date(2019, 7, 23).getTime(), 209.43, 212.05, 201, 202.64]
            ]
            // Modify the bars for better visibility
            series
                .setPositiveStyle((figure) => figure
                    .setStrokeStyle((stroke) => stroke.setThickness(2))
                )
                .setNegativeStyle((figure) => figure
                    .setStrokeStyle((stroke) => stroke.setThickness(2))
                )
                .setFigureWidth(10)
            // Add the created data array to the OHLC series.
            series.add(data)
            // Add a single data entry to the array.
            series.add([new Date(2019, 7, 26).getTime(), 205.86, 207.19, 205.06, 206.49])
            // Change the title and behavior of the default Y Axis
            chart.getDefaultAxisY()
                .setTitle('USD')
                .setInterval(195, 220)
                .setScrollStrategy(AxisScrollStrategies.expansion)
            // Fit the X Axis around the given data. Passing false skips animating the fitting.
            chart.getDefaultAxisX()
                .fit(false)"),
        'Example_2' => array(
            'chart_name' => 'candleSticks Chart',
            'icon' => plugins_url().'/LC-JS/images/icons/candleSticks.png',
            'html_code' => '<div id="target" class="row content"></div>',
            'javascript_code' => "
                            const {
                                lightningChart,
                                AxisTickStrategies,
                                OHLCFigures,
                                emptyLine,
                                AxisScrollStrategies,
                                Themes
                            } = lcjs
                            
                            // Import data-generator from 'xydata'-library.
                            const {
                                createOHLCGenerator
                            } = xydata
                            
                            // Decide on an origin for DateTime axis.
                            const dateOrigin = new Date(2018, 0, 1)
                            
                            // Create a XY Chart.
                            const chart = lightningChart(license_key).ChartXY({
                                // theme: Themes.dark
                                container: 'target'
                            })
                            // Use DateTime X-axis using with above defined origin.
                            chart
                                .getDefaultAxisX()
                                .setTickStrategy(
                                    AxisTickStrategies.DateTime,
                                    (tickStrategy) => tickStrategy.setDateOrigin(dateOrigin)
                                )
                            
                            chart.setTitle('Candlesticks Chart')
                            // Style AutoCursor using preset.
                            chart.setAutoCursor(cursor => {
                                cursor.disposeTickMarkerY()
                                cursor.setGridStrokeYStyle(emptyLine)
                            })
                            chart.setPadding({ right: 40 })
                            
                            // Change the title and behavior of the default Y Axis
                            chart.getDefaultAxisY()
                                .setTitle('USD')
                                .setInterval(90, 110)
                                .setScrollStrategy(AxisScrollStrategies.expansion)
                            
                            // Add a OHLC series with Candlestick as type of figures.
                            const series = chart.addOHLCSeries({ positiveFigure: OHLCFigures.Candlestick })
                            // Generate some points using 'xydata'-library.
                            const dataSpan = 10 * 24 * 60 * 60 * 1000
                            const dataFrequency = 1000 * 60
                            createOHLCGenerator()
                                .setNumberOfPoints(dataSpan / dataFrequency)
                                .setDataFrequency(dataFrequency)
                                .setStart(100)
                                .generate()
                                .toPromise()
                                .then(data => {
                                    series.add(data)
                                })"
            ),
            'Example_3' => array(
                'chart_name' => 'Dashboard Trading',
                'icon' => plugins_url().'/LC-JS/images/icons/dashboardTrading.png',
                'html_code' => '<div id="target" class="row content"></div>',
                'javascript_code' => "const {
                    lightningChart,
                    AxisTickStrategies,
                    LegendBoxBuilders,
                    SolidFill,
                    SolidLine,
                    emptyLine,
                    ColorRGBA,
                    UIOrigins,
                    Themes
                } = lcjs
                
                // Import data-generator from 'xydata'-library.
                const {
                    createOHLCGenerator,
                    createProgressiveTraceGenerator
                } = xydata
                
                // Create dashboard to house two charts
                const db = lightningChart(license_key).Dashboard({
                    // theme: Themes.dark 
                    numberOfRows: 2,
                    container: 'target',
                    numberOfColumns: 1
                })
                
                // Decide on an origin for DateTime axis.
                const dateOrigin = new Date(2018, 0, 1)
                // Chart that contains the OHLC candle stick series and Bollinger band
                const chartOHLC = db.createChartXY({
                    columnIndex: 0,
                    rowIndex: 0,
                    columnSpan: 1,
                    rowSpan: 1
                })
                // Use DateTime TickStrategy with custom date origin for X Axis.
                chartOHLC
                    .getDefaultAxisX()
                    .setTickStrategy(
                        AxisTickStrategies.DateTime,
                        (tickStrategy) => tickStrategy.setDateOrigin(dateOrigin)
                    )
                // Modify Chart.
                chartOHLC
                    .setTitle('Trading dashboard')
                    //Style AutoCursor.
                    .setAutoCursor(cursor => {
                        cursor.disposeTickMarkerY()
                        cursor.setGridStrokeYStyle(emptyLine)
                    })
                    .setPadding({ right: 40 })
                
                // The top chart should have 66% of view height allocated to it. By giving the first row a height of 2, the relative
                // height of the row becomes 2/3 of the whole view (default value for row height / column width is 1)
                db.setRowHeight(0, 2)
                
                // Create a LegendBox for Candle-Stick and Bollinger Band
                const legendBoxOHLC = chartOHLC.addLegendBox(LegendBoxBuilders.HorizontalLegendBox)
                    .setPosition({ x: 100, y: 100 })
                    .setOrigin(UIOrigins.RightTop)
                
                // Define function which sets Y axis intervals nicely.
                let setViewNicely
                
                // Create OHLC Figures and Area-range.
                //#region
                
                // Get Y-axis for series (view is set manually).
                const stockAxisY = chartOHLC.getDefaultAxisY()
                    .setScrollStrategy(undefined)
                    .setTitle('USD')
                // Add series.
                const areaRangeFill = new SolidFill().setColor(ColorRGBA(100, 149, 237, 50))
                const areaRangeStroke = new SolidLine()
                    .setFillStyle(new SolidFill().setColor(ColorRGBA(100, 149, 237)))
                    .setThickness(1)
                const areaRange = chartOHLC.addAreaRangeSeries({ yAxis: stockAxisY })
                    .setName('Bollinger band')
                    .setHighFillStyle(areaRangeFill)
                    .setLowFillStyle(areaRangeFill)
                    .setHighStrokeStyle(areaRangeStroke)
                    .setLowStrokeStyle(areaRangeStroke)
                    .setMouseInteractions(false)
                    .setCursorEnabled(false)
                
                const stockFigureWidth = 5.0
                const borderBlack = new SolidLine().setFillStyle(new SolidFill().setColor(ColorRGBA(0, 0, 0))).setThickness(1.0)
                const fillBrightRed = new SolidFill().setColor(ColorRGBA(255, 0, 0))
                const fillDimRed = new SolidFill().setColor(ColorRGBA(128, 0, 0))
                const fillBrightGreen = new SolidFill().setColor(ColorRGBA(0, 255, 0))
                const fillDimGreen = new SolidFill().setColor(ColorRGBA(0, 128, 0))
                const stock = chartOHLC.addOHLCSeries({ yAxis: stockAxisY })
                    .setName('Candle-Sticks')
                    // Setting width of figures
                    .setFigureWidth(stockFigureWidth)
                    // Styling positive candlestick
                    .setPositiveStyle(candlestick => candlestick
                        // Candlestick body fill style
                        .setBodyFillStyle(fillBrightGreen)
                        // Candlestick body fill style
                        .setBodyStrokeStyle(borderBlack)
                        // Candlestick stroke style
                        .setStrokeStyle((strokeStyle) => strokeStyle.setFillStyle(fillDimGreen))
                    )
                    .setNegativeStyle(candlestick => candlestick
                        .setBodyFillStyle(fillBrightRed)
                        .setBodyStrokeStyle(borderBlack)
                        .setStrokeStyle((strokeStyle) => strokeStyle.setFillStyle(fillDimRed))
                    )
                
                // Make function that handles adding OHLC segments to series.
                const add = (ohlc) => {
                    // ohlc is equal to [x, open, high, low, close]
                    stock.add(ohlc)
                    // AreaRange looks better if it extends a bit further than the actual OHLC values.
                    const areaOffset = 0.2
                    areaRange.add(
                        {
                            position: ohlc[0],
                            high: ohlc[2] - areaOffset,
                            low: ohlc[3] + areaOffset,
                
                        }
                    )
                }
                
                // Generate some static data.
                createOHLCGenerator()
                    .setNumberOfPoints(100)
                    .setDataFrequency(24 * 60 * 60 * 1000)
                    .generate()
                    .toPromise()
                    .then(data => {
                        data.forEach(add)
                        setViewNicely()
                    })
                
                //#endregion
                
                // Create volume.
                //#region
                const chartVolume = db.createChartXY({
                    columnIndex: 0,
                    rowIndex: 1,
                    columnSpan: 1,
                    rowSpan: 1
                })
                // Use DateTime TickStrategy with custom date origin for X Axis.
                chartVolume
                    .getDefaultAxisX()
                    .setTickStrategy(
                        AxisTickStrategies.DateTime,
                        (tickStrategy) => tickStrategy.setDateOrigin(dateOrigin)
                    )
                // Modify Chart.
                chartVolume
                    .setTitle('Volume')
                    .setPadding({ right: 40 })
                // Create a LegendBox as part of the chart.
                const legendBoxVolume = chartVolume.addLegendBox(LegendBoxBuilders.HorizontalLegendBox)
                    .setPosition({ x: 100, y: 100 })
                    .setOrigin(UIOrigins.RightTop)
                
                // Create Y-axis for series (view is set manually).
                const volumeAxisY = chartVolume.getDefaultAxisY()
                    .setTitle('USD')
                    // Modify TickStyle to hide gridstrokes.
                    .setTickStrategy(
                        // Base TickStrategy that should be styled.
                        AxisTickStrategies.Numeric,
                        // Modify the the tickStyles through a mutator.
                        (tickStrat) => tickStrat
                            // Modify the Major tickStyle to not render the grid strokes.
                            .setMajorTickStyle(
                                tickStyle => tickStyle.setGridStrokeStyle(emptyLine)
                            )
                            // Modify the Minor tickStyle to not render the grid strokes.
                            .setMinorTickStyle(
                                tickStyle => tickStyle.setGridStrokeStyle(emptyLine)
                            )
                    )
                const volumeFillStyle = new SolidFill().setColor(ColorRGBA(0, 128, 128, 60))
                const volumeStrokeStyle = new SolidLine()
                    .setFillStyle(volumeFillStyle.setA(255))
                    .setThickness(1)
                const volume = chartVolume.addAreaSeries({ yAxis: volumeAxisY })
                    .setName('Volume')
                    .setFillStyle(volumeFillStyle)
                    .setStrokeStyle(volumeStrokeStyle)
                
                createProgressiveTraceGenerator()
                    .setNumberOfPoints(990)
                    .generate()
                    .toPromise()
                    .then(data => {
                        volume.add(data.map(point => ({ x: point.x * 2.4 * 60 * 60 * 1000, y: Math.abs(point.y) * 10 })))
                        setViewNicely()
                    })
                
                //#endregion
                
                
                // Add series to LegendBox and style entries.
                const entries1 = legendBoxOHLC.add(chartOHLC)
                entries1[0]
                    .setButtonOnFillStyle(areaRangeStroke.getFillStyle())
                    .setButtonOnStrokeStyle(emptyLine)
                
                const entries2 = legendBoxVolume.add(chartVolume)
                entries2[0]
                    .setButtonOnFillStyle(volumeStrokeStyle.getFillStyle())
                    .setButtonOnStrokeStyle(emptyLine)
                
                setViewNicely = () => {
                    const yBoundsStock = { min: areaRange.getYMin(), max: areaRange.getYMax(), range: areaRange.getYMax() - areaRange.getYMin() }
                    const yBoundsVolume = { min: volume.getYMin(), max: volume.getYMax(), range: volume.getYMax() - volume.getYMin() }
                    // Set Y axis intervals so that series don't overlap and volume is under stocks.
                    volumeAxisY.setInterval(yBoundsVolume.min, yBoundsVolume.max)
                    stockAxisY.setInterval(yBoundsStock.min - yBoundsStock.range * .33, yBoundsStock.max)
                }
                
                stock.setResultTableFormatter((builder, series, segment) => {
                    return builder
                        .addRow(series.getName())
                        .addRow(series.axisX.formatValue(segment.getPosition()))
                        .addRow('Open ' + segment.getOpen().toFixed(2))
                        .addRow('High ' + segment.getHigh().toFixed(2))
                        .addRow('Low ' + segment.getLow().toFixed(2))
                        .addRow('Close ' + segment.getClose().toFixed(2))
                })
                
                volume.setResultTableFormatter((builder, series, position, high, low) => {
                    return builder
                        .addRow(series.getName())
                        .addRow(series.axisX.formatValue(position))
                        .addRow('Value ' + Math.round(high))
                        .addRow('Base ' + Math.round(low))
                })
                "),
        ),
        'statistics' => array(
            'Example_1' => array(
              'chart_name' => 'Point Clusters',
              'icon' => plugins_url().'/LC-JS/images/icons/pointClusters.png',
              'html_code' => '<div id="target" class="row content"></div>',
              'javascript_code' => "const {
                lightningChart,
                SolidFill,
                SolidLine,
                ColorPalettes,
                emptyFill,
                AxisTickStrategies,
                PointShape,
                Themes
            } = lcjs
            
            // ----- Cache used styles -----
            const pointSize = 10
            const palette = ColorPalettes.fullSpectrum(10)
            const colors = [0, 6].map(palette)
            const fillStyles = colors.map(color => new SolidFill({ color }))
            // Decide on an origin for DateTime axis.
            const dateOrigin = new Date(2018, 8, 1)
            
            // Create a XY Chart.
            const chart = lightningChart(license_key).ChartXY({
                // theme: Themes.dark
                container: 'target',
            })
                .setTitle('Salary differences between Kuopio and Helsinki')
                .setPadding({
                    right: 50
                })
            
            // Modify the default X Axis to use DateTime TickStrategy, and set the origin for the DateTime Axis.
            chart.getDefaultAxisX()
                .setTickStrategy(
                    AxisTickStrategies.DateTime,
                    (tickStrategy) => tickStrategy.setDateOrigin(dateOrigin)
                )
            
            // Add a series for each cluster of points
            const fstClusterSeries = chart.addPointSeries({ pointShape: PointShape.Circle })
                .setName('Kuopio')
                .setPointFillStyle(fillStyles[0])
                .setPointSize(pointSize)
            const sndClusterSeries = chart.addPointSeries({ pointShape: PointShape.Triangle })
                .setName('Helsinki')
                .setPointFillStyle(fillStyles[1])
                .setPointSize(pointSize)
            
            // The point supplied to series will have their X values multiplied by this value (for easier addition of DateTime-values).
            const dataFrequency = 1000 * 60 * 60 * 24
            const kuopioPoints = [
                { x: 12.152641878669275, y: 5335.336538461539 },
                { x: 11.62426614481409, y: 5259.615384615385 },
                { x: 10.919765166340508, y: 5082.932692307692 },
                { x: 10.156555772994128, y: 4923.076923076923 },
                { x: 9.334637964774949, y: 4796.875 },
                { x: 8.101761252446183, y: 4704.326923076923 },
                { x: 6.634050880626223, y: 4620.192307692308 },
                { x: 3.933463796477495, y: 4418.2692307692305 },
                { x: 3.111545988258317, y: 4342.548076923077 },
                { x: 2.8180039138943247, y: 4199.5192307692305 },
                { x: 2.8180039138943247, y: 4014.423076923077 },
                { x: 2.4657534246575334, y: 3930.2884615384614 },
                { x: 2.407045009784736, y: 3745.1923076923076 },
                { x: 2.935420743639921, y: 3408.653846153846 },
                { x: 3.6399217221135025, y: 3307.6923076923076 },
                { x: 5.107632093933463, y: 3181.4903846153848 },
                { x: 6.868884540117416, y: 3181.4903846153848 },
                { x: 7.749510763209393, y: 3198.3173076923076 },
                { x: 9.217221135029353, y: 3316.1057692307695 },
                { x: 10.215264187866929, y: 3475.9615384615386 },
                { x: 11.037181996086105, y: 3585.3365384615386 },
                { x: 12.035225048923678, y: 3719.951923076923 },
                { x: 12.798434442270057, y: 3778.846153846154 },
                { x: 16.027397260273972, y: 3820.9134615384614 },
                { x: 22.544031311154598, y: 3896.6346153846152 },
                { x: 24.187866927592953, y: 3963.9423076923076 },
                { x: 24.83365949119374, y: 4325.721153846154 },
                { x: 24.65753424657534, y: 4435.096153846154 },
                { x: 24.422700587084147, y: 4603.365384615385 },
                { x: 24.129158512720156, y: 4754.807692307692 },
                { x: 22.4853228962818, y: 5082.932692307692 },
                { x: 21.78082191780822, y: 5167.067307692308 },
                { x: 19.080234833659492, y: 5562.5 },
                { x: 17.31898238747554, y: 5722.3557692307695 },
                { x: 16.262230919765166, y: 5865.384615384615 },
                { x: 15.264187866927593, y: 5924.278846153846 },
                { x: 14.324853228962818, y: 5966.346153846154 },
                { x: 11.859099804305282, y: 5915.865384615385 },
                { x: 10.919765166340508, y: 5865.384615384615 },
                { x: 7.749510763209393, y: 5579.326923076923 },
                { x: 6.164383561643835, y: 5419.471153846154 },
                { x: 5.048923679060665, y: 5108.173076923077 },
                { x: 3.5812133072407044, y: 4460.336538461539 },
                { x: 2.8767123287671232, y: 4174.278846153846 },
                { x: 0.3522504892367906, y: 4342.548076923077 },
                { x: 0.3522504892367906, y: 4056.4903846153848 },
                { x: 0.23483365949119372, y: 3551.6826923076924 },
                { x: 0.410958904109589, y: 3282.4519230769233 },
                { x: 0.23483365949119372, y: 3046.875 },
                { x: 0.410958904109589, y: 2887.019230769231 },
                { x: 0.6457925636007827, y: 2693.5096153846152 },
                { x: 2.759295499021526, y: 2643.028846153846 },
                { x: 3.7573385518590996, y: 2643.028846153846 },
                { x: 5.98825831702544, y: 2668.269230769231 },
                { x: 9.510763209393346, y: 2693.5096153846152 },
                { x: 10.391389432485322, y: 2802.8846153846157 },
                { x: 11.037181996086105, y: 3030.048076923077 },
                { x: 11.741682974559687, y: 3274.038461538462 },
                { x: 12.857142857142856, y: 3475.9615384615386 },
                { x: 14.383561643835616, y: 3602.1634615384614 },
                { x: 15.205479452054796, y: 3652.6442307692305 },
                { x: 16.43835616438356, y: 3711.5384615384614 },
                { x: 17.96477495107632, y: 3804.0865384615386 },
                { x: 19.256360078277883, y: 4090.1442307692305 },
                { x: 19.021526418786692, y: 4233.173076923077 },
                { x: 17.201565557729943, y: 4384.615384615385 },
                { x: 15.088062622309199, y: 4435.096153846154 },
                { x: 12.270058708414872, y: 4376.201923076923 },
                { x: 10.626223091976517, y: 4207.932692307692 },
                { x: 8.336594911937379, y: 3862.9807692307695 },
                { x: 7.808219178082191, y: 3770.4326923076924 },
                { x: 5.283757338551859, y: 4485.576923076923 },
                { x: 4.87279843444227, y: 3896.6346153846152 },
                { x: 4.285714285714286, y: 3669.471153846154 },
                { x: 2.172211350293542, y: 4578.125 },
                { x: 3.933463796477495, y: 4981.971153846154 },
                { x: 5.518590998043054, y: 5335.336538461539 },
                { x: 7.749510763209393, y: 5798.076923076923 },
                { x: 10.09784735812133, y: 5688.701923076923 },
                { x: 9.628180039138943, y: 5352.163461538461 },
                { x: 13.209393346379647, y: 5570.913461538461 },
                { x: 12.68101761252446, y: 5772.836538461539 },
                { x: 14.794520547945204, y: 5688.701923076923 },
                { x: 16.203522504892366, y: 5461.538461538461 },
                { x: 18.493150684931507, y: 5183.8942307692305 },
                { x: 20.19569471624266, y: 4998.798076923077 },
                { x: 21.42857142857143, y: 4788.461538461539 },
                { x: 22.07436399217221, y: 4443.509615384615 },
                { x: 22.720156555772995, y: 4123.798076923077 },
                { x: 21.722113502935418, y: 4039.6634615384614 },
                { x: 19.608610567514678, y: 4443.509615384615 },
                { x: 18.786692759295498, y: 4586.538461538461 },
                { x: 16.731898238747554, y: 4822.115384615385 },
                { x: 14.500978473581215, y: 5032.451923076923 },
                { x: 13.972602739726026, y: 5099.759615384615 },
                { x: 12.38747553816047, y: 4754.807692307692 },
                { x: 14.911937377690801, y: 4687.5 },
                { x: 14.500978473581215, y: 4822.115384615385 },
                { x: 16.027397260273972, y: 5125 },
                { x: 15.029354207436398, y: 5394.2307692307695 },
                { x: 13.737769080234832, y: 5436.298076923077 },
                { x: 15.322896281800393, y: 5242.788461538461 },
                { x: 17.495107632093934, y: 5183.8942307692305 },
                { x: 18.904109589041095, y: 4838.942307692308 },
                { x: 20.900195694716242, y: 4603.365384615385 },
                { x: 22.133072407045006, y: 4620.192307692308 },
                { x: 22.367906066536204, y: 4737.9807692307695 },
                { x: 21.78082191780822, y: 4889.423076923077 },
                { x: 20.54794520547945, y: 5116.586538461539 },
                { x: 19.49119373776908, y: 5259.615384615385 },
                { x: 18.258317025440316, y: 5411.057692307692 },
                { x: 17.436399217221137, y: 5478.365384615385 },
                { x: 16.497064579256357, y: 5621.3942307692305 },
                { x: 14.383561643835616, y: 5814.903846153846 },
                { x: 13.561643835616438, y: 5840.1442307692305 },
                { x: 12.093933463796477, y: 5646.634615384615 },
                { x: 11.037181996086105, y: 5411.057692307692 },
                { x: 10.861056751467709, y: 5293.2692307692305 },
                { x: 9.921722113502934, y: 5150.240384615385 },
                { x: 8.336594911937379, y: 5301.682692307692 },
                { x: 6.927592954990215, y: 5217.548076923077 },
                { x: 6.34050880626223, y: 5015.625 },
                { x: 5.518590998043054, y: 4889.423076923077 },
                { x: 2.5244618395303324, y: 4679.086538461539 },
                { x: 4.1682974559686885, y: 4780.048076923077 },
                { x: 3.170254403131115, y: 4805.288461538461 },
                { x: 2.407045009784736, y: 4527.6442307692305 },
                { x: 1.8199608610567513, y: 4334.134615384615 },
                { x: 1.467710371819961, y: 4048.076923076923 },
                { x: 1.467710371819961, y: 3888.221153846154 },
                { x: 1.2915851272015655, y: 3711.5384615384614 },
                { x: 1.11545988258317, y: 3484.375 },
                { x: 1.3502935420743638, y: 3223.5576923076924 },
                { x: 1.4090019569471623, y: 3080.528846153846 },
                { x: 1.9960861056751458, y: 3030.048076923077 },
                { x: 4.10958904109589, y: 2929.0865384615386 },
                { x: 5.401174168297455, y: 2903.846153846154 },
                { x: 6.046966731898238, y: 2887.019230769231 },
                { x: 6.75146771037182, y: 2920.673076923077 },
                { x: 8.101761252446183, y: 2929.0865384615386 },
                { x: 9.217221135029353, y: 2996.394230769231 },
                { x: 10.09784735812133, y: 3189.903846153846 },
                { x: 6.34050880626223, y: 3097.3557692307695 },
                { x: 4.050880626223091, y: 3147.8365384615386 },
                { x: 2.8767123287671232, y: 3206.7307692307695 },
                { x: 2.1135029354207435, y: 2811.298076923077 },
                { x: 3.2289628180039136, y: 2929.0865384615386 },
                { x: 2.8180039138943247, y: 3055.288461538462 },
                { x: 4.931506849315069, y: 3265.625 },
                { x: 5.518590998043054, y: 3467.548076923077 },
                { x: 4.637964774951076, y: 3518.028846153846 },
                { x: 3.874755381604696, y: 3534.8557692307695 },
                { x: 2.8180039138943247, y: 3602.1634615384614 },
                { x: 2.23091976516634, y: 3644.2307692307695 },
                { x: 3.7573385518590996, y: 3879.8076923076924 },
                { x: 4.1682974559686885, y: 4098.557692307692 },
                { x: 4.6966731898238745, y: 4191.1057692307695 },
                { x: 5.694716242661448, y: 4258.413461538461 },
                { x: 6.986301369863013, y: 4350.961538461539 },
                { x: 7.86692759295499, y: 4409.8557692307695 },
                { x: 8.864970645792564, y: 4527.6442307692305 },
                { x: 9.217221135029353, y: 4586.538461538461 },
                { x: 9.041095890410958, y: 5141.826923076923 },
                { x: 8.512720156555773, y: 5074.5192307692305 },
                { x: 7.984344422700587, y: 4965.1442307692305 },
                { x: 7.455968688845399, y: 4838.942307692308 },
                { x: 6.457925636007829, y: 4754.807692307692 },
                { x: 5.577299412915851, y: 4679.086538461539 },
                { x: 6.986301369863013, y: 5343.75 },
                { x: 8.63013698630137, y: 5469.951923076923 },
                { x: 10.215264187866929, y: 5503.6057692307695 },
                { x: 11.154598825831703, y: 5537.259615384615 },
                { x: 13.033268101761252, y: 5427.884615384615 },
                { x: 13.09197651663405, y: 5192.307692307692 },
                { x: 12.152641878669275, y: 4990.384615384615 },
                { x: 10.919765166340508, y: 4695.913461538461 },
                { x: 10.450097847358121, y: 4611.778846153846 },
                { x: 9.804305283757339, y: 4477.163461538461 },
                { x: 8.806262230919765, y: 4359.375 },
                { x: 8.043052837573384, y: 4275.240384615385 },
                { x: 7.10371819960861, y: 4174.278846153846 },
                { x: 6.105675146771037, y: 3921.875 },
                { x: 5.636007827788649, y: 3719.951923076923 },
                { x: 6.223091976516634, y: 3568.5096153846152 },
                { x: 6.34050880626223, y: 3375 },
                { x: 6.986301369863013, y: 3324.519230769231 },
                { x: 7.632093933463796, y: 3425.4807692307695 },
                { x: 8.160469667318981, y: 3518.028846153846 },
                { x: 9.217221135029353, y: 3610.576923076923 },
                { x: 10.09784735812133, y: 3694.7115384615386 },
                { x: 11.037181996086105, y: 3787.2596153846152 },
                { x: 11.97651663405088, y: 3862.9807692307695 },
                { x: 12.915851272015654, y: 3947.1153846153848 },
                { x: 13.385518590998043, y: 4073.3173076923076 },
                { x: 14.442270058708415, y: 4115.384615384615 },
                { x: 15.616438356164382, y: 4123.798076923077 },
                { x: 17.201565557729943, y: 4115.384615384615 },
                { x: 18.375733855185906, y: 4048.076923076923 },
                { x: 19.608610567514678, y: 3947.1153846153848 },
                { x: 19.608610567514678, y: 3879.8076923076924 },
                { x: 21.42857142857143, y: 4182.692307692308 },
                { x: 20.958904109589042, y: 4308.894230769231 },
                { x: 23.01369863013699, y: 4275.240384615385 },
                { x: 19.021526418786692, y: 4426.682692307692 },
                { x: 17.377690802348333, y: 4645.432692307692 },
                { x: 17.553816046966734, y: 4931.490384615385 },
                { x: 16.262230919765166, y: 4990.384615384615 },
                { x: 16.203522504892366, y: 4679.086538461539 },
                { x: 14.853228962818003, y: 4468.75 },
                { x: 13.09197651663405, y: 4628.6057692307695 },
                { x: 13.913894324853228, y: 4805.288461538461 },
                { x: 12.622309197651663, y: 4906.25 },
                { x: 11.330724070450097, y: 4847.3557692307695 },
                { x: 11.037181996086105, y: 4199.5192307692305 },
                { x: 9.628180039138943, y: 3989.1826923076924 },
                { x: 9.217221135029353, y: 3930.2884615384614 },
                { x: 13.561643835616438, y: 4157.451923076923 },
                { x: 14.20743639921722, y: 4199.5192307692305 },
                { x: 18.904109589041095, y: 5823.317307692308 },
                { x: 19.608610567514678, y: 5739.182692307692 },
                { x: 20.900195694716242, y: 5713.942307692308 },
                { x: 21.722113502935418, y: 5865.384615384615 },
                { x: 22.720156555772995, y: 5655.048076923077 },
                { x: 23.95303326810176, y: 5528.846153846154 },
                { x: 23.894324853228966, y: 5394.2307692307695 },
                { x: 23.835616438356162, y: 5326.923076923077 },
                { x: 23.835616438356162, y: 5183.8942307692305 },
                { x: 23.894324853228966, y: 5024.038461538461 },
                { x: 24.481409001956948, y: 4872.596153846154 },
                { x: 21.487279843444227, y: 5394.2307692307695 },
                { x: 21.956947162426612, y: 5562.5 },
                { x: 22.367906066536204, y: 5512.0192307692305 },
                { x: 22.89628180039139, y: 5352.163461538461 },
                { x: 20.841487279843445, y: 5444.711538461539 },
                { x: 14.031311154598825, y: 3862.9807692307695 },
                { x: 15.557729941291585, y: 3854.5673076923076 },
                { x: 12.32876712328767, y: 4174.278846153846 },
                { x: 11.859099804305282, y: 4115.384615384615 },
                { x: 11.330724070450097, y: 3921.875 }
            ]
            
            const helsinkiPoints = [
                { x: 6.164383561643835, y: 2314.6634615384614 },
                { x: 6.516634050880624, y: 2351.2019230769233 },
                { x: 7.045009784735812, y: 2479.0865384615386 },
                { x: 7.279843444227005, y: 2543.028846153846 },
                { x: 7.514677103718199, y: 2638.9423076923076 },
                { x: 8.277886497064578, y: 2794.230769230769 },
                { x: 8.63013698630137, y: 2853.605769230769 },
                { x: 10.156555772994128, y: 2972.355769230769 },
                { x: 10.919765166340508, y: 3018.028846153846 },
                { x: 11.800391389432484, y: 3063.7019230769233 },
                { x: 12.798434442270057, y: 3109.375 },
                { x: 14.442270058708415, y: 3155.0480769230767 },
                { x: 16.555772994129157, y: 3228.125 },
                { x: 17.025440313111545, y: 3292.0673076923076 },
                { x: 17.729941291585128, y: 3419.951923076923 },
                { x: 18.610567514677104, y: 3904.086538461538 },
                { x: 18.6692759295499, y: 3753.3653846153848 },
                { x: 18.31702544031311, y: 3616.346153846154 },
                { x: 18.082191780821915, y: 3534.1346153846152 },
                { x: 19.667318982387478, y: 3922.355769230769 },
                { x: 20.782778864970645, y: 3894.951923076923 },
                { x: 22.07436399217221, y: 3858.413461538461 },
                { x: 23.131115459882583, y: 3799.0384615384614 },
                { x: 24.951076320939336, y: 3739.6634615384614 },
                { x: 26.301369863013697, y: 3639.1826923076924 },
                { x: 26.59491193737769, y: 3424.5192307692305 },
                { x: 26.771037181996086, y: 3200.721153846154 },
                { x: 27.76908023483366, y: 2949.5192307692305 },
                { x: 28.12133072407045, y: 2712.0192307692305 },
                { x: 26.53620352250489, y: 2638.9423076923076 },
                { x: 25.655577299412915, y: 2437.980769230769 },
                { x: 15.616438356164382, y: 2150.2403846153848 },
                { x: 19.138943248532293, y: 2227.8846153846152 },
                { x: 20.782778864970645, y: 2300.9615384615386 },
                { x: 23.776908023483365, y: 2469.951923076923 },
                { x: 25.71428571428571, y: 2757.6923076923076 },
                { x: 26.301369863013697, y: 2849.0384615384614 },
                { x: 26.947162426614483, y: 2954.086538461538 },
                { x: 24.951076320939336, y: 3050 },
                { x: 24.83365949119374, y: 3324.0384615384614 },
                { x: 25.949119373776906, y: 3547.836538461538 },
                { x: 27.06457925636008, y: 3598.076923076923 },
                { x: 27.886497064579256, y: 3630.0480769230767 },
                { x: 28.003913894324853, y: 3767.0673076923076 },
                { x: 27.59295499021526, y: 3780.7692307692305 },
                { x: 26.066536203522503, y: 3744.230769230769 },
                { x: 24.36399217221135, y: 3853.8461538461534 },
                { x: 19.080234833659492, y: 3218.9903846153848 },
                { x: 14.442270058708415, y: 2944.951923076923 },
                { x: 10.626223091976517, y: 2821.6346153846152 },
                { x: 9.628180039138943, y: 2670.9134615384614 },
                { x: 8.454011741682974, y: 2492.7884615384614 },
                { x: 9.393346379647749, y: 2469.951923076923 },
                { x: 10.743639921722114, y: 2465.3846153846152 },
                { x: 13.033268101761252, y: 2488.221153846154 },
                { x: 8.21917808219178, y: 2323.798076923077 },
                { x: 7.10371819960861, y: 2273.5576923076924 },
                { x: 6.2818003913894325, y: 2264.423076923077 },
                { x: 6.223091976516634, y: 2186.778846153846 },
                { x: 6.457925636007829, y: 2168.5096153846152 },
                { x: 8.101761252446183, y: 2145.673076923077 },
                { x: 10.567514677103718, y: 2232.4519230769233 },
                { x: 11.448140900195694, y: 2442.548076923077 },
                { x: 12.270058708414872, y: 2661.778846153846 },
                { x: 14.031311154598825, y: 2967.7884615384614 },
                { x: 15.73385518590998, y: 3282.9326923076924 },
                { x: 16.908023483365948, y: 3438.221153846154 },
                { x: 17.61252446183953, y: 3570.6730769230767 },
                { x: 13.50293542074364, y: 3328.605769230769 },
                { x: 11.97651663405088, y: 3155.0480769230767 },
                { x: 10.332681017612524, y: 2958.653846153846 },
                { x: 8.864970645792564, y: 2826.201923076923 },
                { x: 7.162426614481409, y: 2652.644230769231 },
                { x: 10.450097847358121, y: 2702.8846153846152 },
                { x: 13.033268101761252, y: 2903.846153846154 },
                { x: 18.610567514677104, y: 3342.3076923076924 },
                { x: 19.608610567514678, y: 3575.2403846153848 },
                { x: 20.19569471624266, y: 3652.8846153846152 },
                { x: 19.960861056751465, y: 3739.6634615384614 },
                { x: 22.015655577299416, y: 3693.9903846153848 },
                { x: 23.24853228962818, y: 3333.1730769230767 },
                { x: 21.07632093933464, y: 3191.586538461538 },
                { x: 19.726027397260275, y: 3109.375 },
                { x: 18.31702544031311, y: 3086.5384615384614 },
                { x: 15.499021526418783, y: 3013.461538461538 },
                { x: 15.264187866927593, y: 2684.6153846153848 },
                { x: 14.031311154598825, y: 2529.326923076923 },
                { x: 13.561643835616438, y: 2680.048076923077 },
                { x: 16.908023483365948, y: 2775.961538461538 },
                { x: 18.02348336594912, y: 2830.7692307692305 },
                { x: 22.89628180039139, y: 2817.0673076923076 },
                { x: 21.78082191780822, y: 2437.980769230769 },
                { x: 20.724070450097848, y: 2433.4134615384614 },
                { x: 16.908023483365948, y: 2433.4134615384614 },
                { x: 12.915851272015654, y: 2218.75 },
                { x: 11.62426614481409, y: 2223.3173076923076 },
                { x: 13.85518590998043, y: 2364.903846153846 },
                { x: 16.08610567514677, y: 2278.125 },
                { x: 17.260273972602743, y: 2310.096153846154 },
                { x: 18.7279843444227, y: 2406.0096153846152 },
                { x: 19.021526418786692, y: 2465.3846153846152 },
                { x: 18.610567514677104, y: 2575 },
                { x: 17.201565557729943, y: 2588.701923076923 },
                { x: 15.675146771037182, y: 2451.6826923076924 },
                { x: 15.264187866927593, y: 2424.278846153846 },
                { x: 15.557729941291585, y: 2620.673076923077 },
                { x: 18.140900195694716, y: 2725.721153846154 },
                { x: 18.6692759295499, y: 2739.423076923077 },
                { x: 20.900195694716242, y: 2625.2403846153848 },
                { x: 22.309197651663403, y: 2620.673076923077 },
                { x: 23.95303326810176, y: 2570.4326923076924 },
                { x: 25.303326810176124, y: 2538.4615384615386 },
                { x: 25.655577299412915, y: 2538.4615384615386 },
                { x: 25.00978473581213, y: 2652.644230769231 },
                { x: 24.77495107632094, y: 2890.1442307692305 },
                { x: 23.424657534246577, y: 2986.0576923076924 },
                { x: 21.545988258317024, y: 2903.846153846154 },
                { x: 21.135029354207433, y: 2785.096153846154 },
                { x: 20.313111545988257, y: 2849.0384615384614 },
                { x: 19.726027397260275, y: 2972.355769230769 },
                { x: 17.96477495107632, y: 2958.653846153846 },
                { x: 17.553816046966734, y: 2940.3846153846152 },
                { x: 15.557729941291585, y: 2821.6346153846152 },
                { x: 13.620352250489233, y: 2803.3653846153848 },
                { x: 13.50293542074364, y: 2862.7403846153848 },
                { x: 12.38747553816047, y: 3008.8942307692305 },
                { x: 16.731898238747554, y: 3132.211538461538 },
                { x: 19.080234833659492, y: 3465.625 },
                { x: 20.430528375733854, y: 3511.2980769230767 },
                { x: 21.898238747553815, y: 3561.5384615384614 },
                { x: 22.95499021526419, y: 3602.6442307692305 },
                { x: 23.424657534246577, y: 3648.3173076923076 },
                { x: 23.718199608610565, y: 3675.721153846154 },
                { x: 24.77495107632094, y: 3520.4326923076924 },
                { x: 22.544031311154598, y: 3433.6538461538457 },
                { x: 20.078277886497062, y: 3342.3076923076924 },
                { x: 22.95499021526419, y: 3241.826923076923 },
                { x: 24.246575342465754, y: 3187.0192307692305 },
                { x: 22.661448140900195, y: 3109.375 },
                { x: 21.545988258317024, y: 3063.7019230769233 },
                { x: 26.360078277886497, y: 3118.5096153846152 },
                { x: 26.53620352250489, y: 3301.201923076923 },
                { x: 25.655577299412915, y: 3387.980769230769 },
                { x: 24.89236790606654, y: 3429.086538461538 },
                { x: 24.070450097847356, y: 3502.1634615384614 },
                { x: 25.244618395303327, y: 3643.75 },
                { x: 22.837573385518592, y: 3721.3942307692305 },
                { x: 21.135029354207433, y: 3776.201923076923 },
                { x: 19.608610567514678, y: 3821.875 },
                { x: 18.8454011741683, y: 3643.75 },
                { x: 20.665362035225048, y: 3419.951923076923 },
                { x: 21.252446183953033, y: 3319.471153846154 },
                { x: 16.379647749510767, y: 2986.0576923076924 },
                { x: 16.84931506849315, y: 3031.730769230769 },
                { x: 14.677103718199607, y: 3072.836538461538 },
                { x: 12.152641878669275, y: 2807.9326923076924 },
                { x: 11.682974559686889, y: 2593.269230769231 },
                { x: 10.626223091976517, y: 2561.298076923077 },
                { x: 9.686888454011742, y: 2570.4326923076924 },
                { x: 8.63013698630137, y: 2716.586538461538 },
                { x: 11.62426614481409, y: 2332.9326923076924 },
                { x: 6.986301369863013, y: 2369.471153846154 },
                { x: 8.63013698630137, y: 2383.173076923077 },
                { x: 9.334637964774949, y: 2364.903846153846 },
                { x: 9.804305283757339, y: 2328.3653846153848 },
                { x: 9.393346379647749, y: 2287.2596153846152 },
                { x: 9.393346379647749, y: 2246.153846153846 },
                { x: 10.450097847358121, y: 2214.1826923076924 },
                { x: 12.093933463796477, y: 2182.2115384615386 },
                { x: 13.561643835616438, y: 2191.346153846154 },
                { x: 14.383561643835616, y: 2273.5576923076924 },
                { x: 15.264187866927593, y: 2314.6634615384614 },
                { x: 17.201565557729943, y: 2332.9326923076924 },
                { x: 18.7279843444227, y: 2319.2307692307695 },
                { x: 19.78473581213307, y: 2364.903846153846 },
                { x: 19.960861056751465, y: 2606.971153846154 },
                { x: 20.371819960861057, y: 2611.5384615384614 },
                { x: 20.900195694716242, y: 2712.0192307692305 },
                { x: 22.77886497064579, y: 2739.423076923077 },
                { x: 23.894324853228966, y: 2775.961538461538 },
                { x: 21.956947162426612, y: 2976.9230769230767 },
                { x: 18.02348336594912, y: 3209.855769230769 },
                { x: 21.66340508806262, y: 3310.336538461538 },
                { x: 21.36986301369863, y: 3260.096153846154 },
                { x: 22.07436399217221, y: 3205.2884615384614 },
                { x: 23.659491193737768, y: 3100.2403846153843 },
                { x: 25.479452054794518, y: 3205.2884615384614 },
                { x: 26.65362035225049, y: 3036.2980769230767 },
                { x: 25.538160469667318, y: 2995.1923076923076 },
                { x: 25.244618395303327, y: 2954.086538461538 },
                { x: 27.945205479452053, y: 3159.6153846153848 },
                { x: 27.651663405088062, y: 3081.971153846154 },
                { x: 27.651663405088062, y: 3264.6634615384614 },
                { x: 27.945205479452053, y: 3547.836538461538 },
                { x: 27.475538160469664, y: 3511.2980769230767 },
                { x: 28.356164383561644, y: 3456.4903846153848 },
                { x: 28.767123287671232, y: 3438.221153846154 },
                { x: 16.379647749510767, y: 3442.7884615384614 },
                { x: 15.499021526418783, y: 3374.2788461538457 },
                { x: 14.500978473581215, y: 3269.230769230769 },
                { x: 13.268101761252446, y: 3214.4230769230767 },
                { x: 15.968688845401173, y: 3141.346153846154 },
                { x: 15.381604696673193, y: 3200.721153846154 },
                { x: 15.029354207436398, y: 3104.8076923076924 },
                { x: 11.682974559686889, y: 2922.1153846153848 },
                { x: 10.626223091976517, y: 2894.711538461538 },
                { x: 9.628180039138943, y: 2839.903846153846 },
                { x: 9.686888454011742, y: 2762.2596153846152 },
                { x: 11.859099804305282, y: 2734.855769230769 },
                { x: 9.217221135029353, y: 2912.980769230769 },
                { x: 8.277886497064578, y: 2584.1346153846152 },
                { x: 11.272015655577299, y: 2661.778846153846 },
                { x: 14.324853228962818, y: 2780.528846153846 },
                { x: 15.616438356164382, y: 2762.2596153846152 },
                { x: 16.614481409001957, y: 2712.0192307692305 },
                { x: 17.436399217221137, y: 2675.480769230769 },
                { x: 13.79647749510763, y: 2602.403846153846 },
                { x: 16.320939334637963, y: 2538.4615384615386 },
                { x: 17.729941291585128, y: 2511.0576923076924 },
                { x: 14.442270058708415, y: 2465.3846153846152 },
                { x: 12.093933463796477, y: 2529.326923076923 },
                { x: 12.857142857142856, y: 2597.8365384615386 },
                { x: 12.563600782778865, y: 2419.7115384615386 },
                { x: 12.798434442270057, y: 2342.0673076923076 },
                { x: 13.79647749510763, y: 2296.394230769231 },
                { x: 13.033268101761252, y: 2282.6923076923076 },
                { x: 10.567514677103718, y: 2369.471153846154 },
                { x: 9.041095890410958, y: 2433.4134615384614 },
                { x: 7.749510763209393, y: 2442.548076923077 },
                { x: 8.688845401174168, y: 2648.076923076923 },
                { x: 7.690802348336595, y: 2730.2884615384614 },
                { x: 23.01369863013699, y: 2931.25 },
                { x: 29, y: 2931.25 },
                { x: 29, y: 2931.25 },
                { x: 29, y: 2931.25 }
            ]
            
            // Create collection of rectangles which are going to be used as frame for clusters
            const rects = chart.addRectangleSeries()
                .setCursorEnabled(false)
            
            // Base style for strokes of frames. Line-FillStyle will be overridden per each cluster.
            const strokeStyle = new SolidLine()
                .setThickness(2)
            
            // Setup view nicely.
            chart.getDefaultAxisX()
                .setInterval(0 * dataFrequency, 30 * dataFrequency, true, true)
            
            chart.getDefaultAxisY()
                .setTitle('Salary ($)')
                .setInterval(1500, 6500, true, true)
            /**
            * Adds clusters of points to specified series and creates frames for them
            * @param {PointSeries} series Series which should hold the cluster
            * @return Function which receives a cluster of points
            */
            const drawCluster = (series, points) => {
                // Add points to specified series
                series.add(points.map(point => ({ x: point.x * dataFrequency, y: point.y })))
                // Cache top left corner of cluster area
                series.setResultTableFormatter((builder, series, Xvalue, Yvalue) => {
                    return builder
                        .addRow(`{series.getName()}`)
                        .addRow('Date : ' + series.axisX.formatValue(Xvalue))
                        .addRow('Salary : ' + Yvalue.toFixed(0))
                })
                const topLeftCorner = {
                    x: series.getXMin(),
                    y: series.getYMin(),
                }
                // Create frame around cluster
                rects.add({
                    x: topLeftCorner.x,
                    y: topLeftCorner.y,
                    width: series.getXMax() - topLeftCorner.x,
                    height: series.getYMax() - topLeftCorner.y
                })
                    // Disable filling of frame
                    .setFillStyle(emptyFill)
                    // Configure thickness and color of stroke via strokeStyle
                    .setStrokeStyle(strokeStyle.setFillStyle(series.getPointFillStyle()))
            }
            
            drawCluster(fstClusterSeries, kuopioPoints)
            drawCluster(sndClusterSeries, helsinkiPoints)
            
            // Enable AutoCursor auto-fill.
            chart.setAutoCursor(cursor => (cursor)
                .setResultTableAutoTextStyle(true)
                .setTickMarkerXAutoTextStyle(true)
                .setTickMarkerYAutoTextStyle(true)
            )"),
            'Example_2' => array(
                'chart_name' => 'Box And Violin Chart',
                'icon' => plugins_url().'/LC-JS/images/icons/boxPlotAndViolin.png',
                'html_code' => '<div id="target" class="row content"></div>',
                'javascript_code' => "const {
                    ColorPalettes,
                    SolidFill,
                    SolidLine,
                    emptyLine,
                    lightningChart,
                    yDimensionStrategy,
                    LegendBoxBuilders,
                    UIDraggingModes,
                    AxisScrollStrategies,
                    AxisTickStrategies,
                    AutoCursorModes,
                    UIOrigins,
                    Themes
                } = lcjs
                
                // Import data-generator from 'xydata'-library.
                const {
                    createProgressiveFunctionGenerator
                } = xydata
                
                // ----- Cache used styles -----
                const palette = ColorPalettes.arction(10)
                const colors = [2, 4, 0, 0].map(palette)
                const Style = (color) => {
                    const solidFill = new SolidFill({ color })
                    const opaqueFill = new SolidFill({ color: color.setA(100) })
                    const solidLine = new SolidLine({ fillStyle: solidFill, thickness: 2 })
                    return { solidFill, opaqueFill, solidLine }
                }
                const styles = colors.map(Style)
                const medianStrokeStyle = new SolidLine({ fillStyle: new SolidFill({ color: colors[3] }), thickness: 6 })
                
                // Utilities for graphing distribution functions.
                //#region
                const cumulativeDistribution = (
                    probabilityDistributionFunction,
                    rangeMin,
                    rangeMax,
                    step
                ) => {
                    // Simulate values of respective probability density function.
                    const probabilityValues = []
                    for (let x = rangeMin; x <= rangeMax; x += step)
                        probabilityValues.push(probabilityDistributionFunction(x))
                    // Normalize probability values and push them to a cached array.
                    const probabilitySum = probabilityValues.reduce((prev, cur) => prev + cur, 0)
                    const values = []
                    let accumulatedNormProb = 0
                    for (const probabilityValue of probabilityValues) {
                        const normalizedprobabilityValue = probabilityValue / probabilitySum
                        accumulatedNormProb += normalizedprobabilityValue
                        values.push(accumulatedNormProb)
                    }
                    // Return function that returns the closest value (by x) from the cached 'values' array.
                    return (x) => {
                        const xAsIndex = (x - rangeMin) / (rangeMax - rangeMin) * ((rangeMax - rangeMin) / step)
                        // Pick closest index (left/right) that exists
                        const closestIndex = Math.min(Math.max(Math.round(xAsIndex), 0), values.length - 1)
                        return values[closestIndex]
                    }
                }
                const findQuartileX = (
                    yToLookFor,
                    cumulativeDistributionFunction,
                    rangeMin,
                    rangeMax,
                    step
                ) => {
                    // Iterate over possible 'X' values and pick the one where resulting 'Y' is closest to 'yToLookFor'
                    let bestResult
                    for (let x = rangeMin; x <= rangeMax; x += step) {
                        const y = cumulativeDistributionFunction(x)
                        const delta = Math.abs(y - yToLookFor)
                        if (bestResult === undefined || delta < bestResult.delta)
                            bestResult = { x, delta }
                        else if (bestResult !== undefined && delta > bestResult.delta)
                            break
                    }
                    return bestResult.x
                }
                const probabilityDistribution = (mean, variance) =>
                    (x) =>
                        (1 / (variance * Math.sqrt(2 * Math.PI))) * Math.pow(Math.E, -Math.pow((x - mean), 2) / (2 * variance * variance))
                //#endregion
                
                // Make chart with series graphing standard probability density and cumulative distribution functions.
                const chart = lightningChart(license_key).ChartXY({
                    // theme: Themes.dark 
                    container: 'target',
                })
                    .setTitle('Probability distribution + Simulated accumulation and BoxSeries')
                    // Set auto-cursor mode to 'onHover'
                    .setAutoCursorMode(AutoCursorModes.onHover)
                    .setAutoCursor((cursor) => cursor
                        .setResultTableAutoTextStyle(false)
                        .setTickMarkerXAutoTextStyle(false)
                        .setTickMarkerYAutoTextStyle(false)
                    )
                    .setPadding({ right: 20 })
                
                const xBounds = { min: -4, max: 4 }
                const step = 0.02
                // Setup axes.
                const axisDistribution = chart.getDefaultAxisY()
                const axisNormalized = chart.addAxisY()
                const axisX = chart.getDefaultAxisX()
                    .setInterval(xBounds.min, xBounds.max)
                    .setScrollStrategy(undefined)
                
                // Set up the Distribution Axis.
                axisDistribution
                    .setTitle('Distribution function')
                    .setScrollStrategy(AxisScrollStrategies.expansion)
                    // Modify the TickStrategy to remove gridLines from this Y Axis.
                    .setTickStrategy(
                        // Use Numeric TickStrategy as base.
                        AxisTickStrategies.Numeric,
                        // Use mutator to modify the TickStrategy.
                        tickStrategy => tickStrategy
                            // Modify Major Tick Style by using a mutator.
                            .setMajorTickStyle(
                                tickStyle => tickStyle
                                    .setGridStrokeStyle(emptyLine)
                            )
                            // Modify Minor Tick Style by using a mutator.
                            .setMinorTickStyle(
                                tickStyle => tickStyle
                                    .setGridStrokeStyle(emptyLine)
                            )
                    )
                
                // Set up the Normalized Axis.
                axisNormalized
                    .setTitle('Accumulated distribution (%)')
                    .setInterval(0, 1)
                    .setScrollStrategy(undefined)
                    // Modify the TickStrategy to remove gridLines from this Y Axis.
                    .setTickStrategy(
                        // Use Numeric TickStrategy as base.
                        AxisTickStrategies.Numeric,
                        // Use mutator to modify the TickStrategy.
                        tickStrategy => tickStrategy
                            // Modify Major Tick Style by using a mutator.
                            .setMajorTickStyle(
                                tickStyle => tickStyle
                                    .setGridStrokeStyle(emptyLine)
                            )
                            // Modify Minor Tick Style by using a mutator.
                            .setMinorTickStyle(
                                tickStyle => tickStyle
                                    .setGridStrokeStyle(emptyLine)
                            )
                    )
                
                // Cumulative distribution.
                const cumulativeDistributionSeries = chart.addAreaSeries({ yAxis: axisNormalized })
                    .setName('Simulated Cumulative Distribution')
                    .setFillStyle(styles[0].opaqueFill)
                    .setStrokeStyle(styles[0].solidLine)
                
                // Probability distribution.
                const probabilityDistributionSeries = chart.addAreaSeries({ yAxis: axisDistribution })
                    .setName('Probability Distribution')
                    .setFillStyle(styles[1].opaqueFill)
                    .setStrokeStyle(styles[1].solidLine)
                
                // 'Violin' series.
                const violinSeries = chart.addAreaRangeSeries({ yAxis: axisDistribution })
                    .setName('Violin')
                    .setHighFillStyle(styles[2].opaqueFill)
                    .setLowFillStyle(styles[2].opaqueFill)
                    .setHighStrokeStyle(styles[2].solidLine)
                    .setLowStrokeStyle(styles[2].solidLine)
                
                // Box series.
                const boxSeries = chart.addBoxSeries({ yAxis: axisDistribution, dimensionStrategy: yDimensionStrategy })
                    .setName('Box')
                    .setDefaultStyle((boxAndWhiskers) => boxAndWhiskers
                        .setBodyFillStyle(styles[2].opaqueFill)
                        .setBodyStrokeStyle(styles[2].solidLine)
                        .setStrokeStyle(styles[2].solidLine)
                        .setMedianStrokeStyle(medianStrokeStyle)
                        .setTailWidth(0)
                    )
                
                // Drawing logic
                //#region
                const graphDistribution = (mean, variance) => {
                    // Clear points from series.
                    cumulativeDistributionSeries.clear()
                    probabilityDistributionSeries.clear()
                    boxSeries.clear()
                    violinSeries.clear()
                
                    // Generate and stream points.
                    const probabilityDistributionFunction = probabilityDistribution(mean, variance)
                    const cumulativeDistributionFunction = cumulativeDistribution(probabilityDistributionFunction, xBounds.min, xBounds.max, step)
                    const streamDuration = 1500
                    const streamInterval = 30
                
                    // Reset interval if user isn't up to something.
                    if (!axisDistribution.isStopped())
                        axisDistribution.setInterval(0, 1.0)
                
                    createProgressiveFunctionGenerator()
                        .setSamplingFunction(cumulativeDistributionFunction)
                        .setStart(xBounds.min)
                        .setEnd(xBounds.max)
                        .setStep(step)
                        .generate()
                        .setStreamBatchSize(1000 * (xBounds.max - xBounds.min) / (step * streamInterval * streamDuration))
                        .setStreamInterval(streamInterval)
                        .toStream()
                        .forEach((point) => cumulativeDistributionSeries.add(point))
                    createProgressiveFunctionGenerator()
                        .setSamplingFunction(probabilityDistributionFunction)
                        .setStart(xBounds.min)
                        .setEnd(xBounds.max)
                        .setStep(step)
                        .generate()
                        .setStreamBatchSize(1000 * (xBounds.max - xBounds.min) / (step * streamInterval * streamDuration))
                        .setStreamInterval(streamInterval)
                        .toStream()
                        .forEach((point) => {
                            probabilityDistributionSeries.add(point)
                            if (point.y >= 0.001)
                                // Add mirrored area-point to violin point
                                violinSeries.add({
                                    position: point.x,
                                    high: 1.0 + point.y / 2,
                                    low: 1.0 - point.y / 2
                                })
                        })
                
                    // Add box figure after streaming points.
                    setTimeout(() => {
                        // Find quartile values using cumulative distribution function.
                        const q1 = findQuartileX(0.25, cumulativeDistributionFunction, xBounds.min, xBounds.max, step)
                        const q2 = findQuartileX(0.50, cumulativeDistributionFunction, xBounds.min, xBounds.max, step)
                        const q3 = findQuartileX(0.75, cumulativeDistributionFunction, xBounds.min, xBounds.max, step)
                        const iqr = q3 - q1
                        const boxSeriesDimensions = {
                            // Purely visual 'Y' -dimensions
                            start: 0.90,
                            end: 1.10,
                            // Actual data -dimensions along 'X' -axis
                            lowerExtreme: q1 - 1.5 * iqr,
                            lowerQuartile: q1,
                            median: q2,
                            upperQuartile: q3,
                            upperExtreme: q3 + 1.5 * iqr
                        }
                        boxSeries.add(boxSeriesDimensions)
                    }, streamDuration)
                }
                //#endregion
                
                graphDistribution(0, 1)
                
                // Add LegendBox as part of chart.
                const legend = chart.addLegendBox(LegendBoxBuilders.HorizontalLegendBox, { x: chart.uiScale.x, y: chart.pixelScale.y })
                    .setOrigin(UIOrigins.LeftBottom)
                    .setPosition({ x: 15, y: 250 })
                    .setDraggingMode(UIDraggingModes.freelyDraggable)
                legend.add(chart, 'Series')
                
                cumulativeDistributionSeries.setResultTableFormatter((tableBuilder, rangeSeries, position, high, low) => {
                    const x = (position.toFixed(2) == '-0.00') ? '0.00' : position.toFixed(2);
                    return tableBuilder
                        .addRow('Simulated Cumulative Distribution')
                        .addRow('Position ' + x)
                        .addRow('High ' + high.toFixed(2))
                        .addRow('Base ' + low.toFixed(2))
                })
                probabilityDistributionSeries.setResultTableFormatter((tableBuilder, rangeSeries, position, high, low) => {
                    const x = (position.toFixed(2) == '-0.00') ? '0.00' : position.toFixed(2);
                    return tableBuilder
                        .addRow('Probability Distribution')
                        .addRow('Position ' + x)
                        .addRow('Value ' + high.toFixed(2))
                        .addRow('Base ' + low.toFixed(2))
                })
                violinSeries.setResultTableFormatter((tableBuilder, rangeSeries, position, high, low) => {
                    const x = (position.toFixed(2) == '-0.00') ? '0.00' : position.toFixed(2);
                    return tableBuilder
                        .addRow('Violin')
                        .addRow('Position ' + x)
                        .addRow('Value ' + high.toFixed(2))
                        .addRow('Low ' + low.toFixed(2))
                })"
            ),
            'Example_3' => array(
                'chart_name' => 'Stacked Mountains',
                'icon' => plugins_url().'/LC-JS/images/icons/stackedMountains.png',
                'html_code' => '<div id="target" class="row content"></div>',
                'javascript_code' => "const {
                    lightningChart,
                    ColorPalettes,
                    SolidFill,
                    emptyLine,
                    AxisTickStrategies,
                    AutoCursorModes,
                    Themes
                } = lcjs
                
                // ----- Cache styles -----
                const palette = ColorPalettes.fullSpectrum(12)
                const solidFills = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map(palette).map(color => new SolidFill({ color }))
                const opaqueFills = solidFills.map(fill => fill.setA(150))
                
                // Set the origin date to use for the X Axis
                const dateOrigin = new Date(2017, 0, 1)
                // Multiplier used for X Axis values to transform each X value as a month.
                const dataFrequency = 60 * 60 * 24 * 30 * 1000
                // Create a XY Chart.
                const xyChart = lightningChart(license_key).ChartXY({
                    // theme: Themes.dark
                    container: 'target',
                })
                
                // Set up the Chart, disable zooming and panning mouse interactions
                // and set AutoCursor to show when hovering mouse over Series.
                xyChart
                    .setTitle('Product Version Distribution')
                    .setMouseInteractions(false)
                    .setAutoCursorMode(AutoCursorModes.onHover)
                
                // Set up the X and Y Axes for the chart.
                xyChart
                    .getDefaultAxisX()
                    .setTickStrategy(
                        AxisTickStrategies.DateTime,
                        (tickStrategy) => tickStrategy.setDateOrigin(dateOrigin)
                    )
                    .setMouseInteractions(false)
                
                xyChart.getDefaultAxisY()
                    .setTitle('Distribution %')
                    .setInterval(0, 100)
                    .setMouseInteractions(false)
                
                // ---- Add multiple series with different names and values. ----
                const versionName = [
                    'Version 1',
                    'Version 2',
                    'Version 3',
                    'Version 4',
                    'Version 5',
                    'Version 6',
                    'Version 7',
                    'Version 8',
                    'Version 9',
                    'Version 10',
                    'Version 11',
                    'Version 12'
                ]
                // Array to store the created Area Series.
                const version = []
                // Create Series for each version name.
                versionName.forEach((v, k) => {
                    // The first version (data) is drawn at the bottom of the chart, so we can just use a Area Series to render it.
                    if (k == 0) {
                        version[k] = xyChart.addAreaSeries()
                            .setName(v)
                            .setFillStyle(opaqueFills[k])
                            .setStrokeStyle(stroke => stroke.setFillStyle(solidFills[k]))
                    } else {
                        // Rest of the versions (data) are drawn based on the version before, so we'll use Area Range Series to render it.
                        version[k] = xyChart.addAreaRangeSeries()
                            .setName(v)
                            .setHighFillStyle(opaqueFills[k])
                            .setHighStrokeStyle(stroke => stroke.setFillStyle(solidFills[k]))
                            .setLowStrokeStyle(emptyLine)
                    }
                    // Set up how to display the Result Table.
                    version[k].setResultTableFormatter((builder, series, xValue, yValueHigh, yValueLow) => {
                        return builder
                            .addRow(v)
                            .addRow('Date: ' + series.axisX.formatValue(xValue))
                            .addRow('Distribution: ' + (yValueHigh - yValueLow).toFixed(2) + '%')
                    })
                })
                // Create data for each Version.
                const data = [
                    [
                        { x: 0, y: 1.3 },
                        { x: 1, y: 1.2 },
                        { x: 2, y: 1.1 },
                        { x: 3, y: 1.1 },
                        { x: 4, y: 1 },
                        { x: 5, y: 0.9 },
                        { x: 6, y: 0.8 },
                        { x: 7, y: 0.8 },
                        { x: 8, y: 0.7 },
                        { x: 9, y: 0.7 },
                        { x: 10, y: 0.6 },
                        { x: 11, y: 0.6 },
                        { x: 12, y: 0.5 },
                        { x: 13, y: 0.5 },
                        { x: 14, y: 0.5 },
                        { x: 15, y: 0.4 }
                    ],
                    [
                        { x: 0, y: 4.9 },
                        { x: 1, y: 4.5 },
                        { x: 2, y: 4.2 },
                        { x: 3, y: 4 },
                        { x: 4, y: 3.7 },
                        { x: 5, y: 3.5 },
                        { x: 6, y: 3.3 },
                        { x: 7, y: 3.1 },
                        { x: 8, y: 2.9 },
                        { x: 9, y: 2.7 },
                        { x: 10, y: 2.5 },
                        { x: 11, y: 2.3 },
                        { x: 12, y: 2.2 },
                        { x: 13, y: 2 },
                        { x: 14, y: 1.9 },
                        { x: 15, y: 1.7 }
                    ],
                    [
                        { x: 0, y: 6.8 },
                        { x: 1, y: 6.4 },
                        { x: 2, y: 5.9 },
                        { x: 3, y: 5.7 },
                        { x: 4, y: 5.4 },
                        { x: 5, y: 5.2 },
                        { x: 6, y: 4.8 },
                        { x: 7, y: 4.4 },
                        { x: 8, y: 4.1 },
                        { x: 9, y: 3.9 },
                        { x: 10, y: 3.5 },
                        { x: 11, y: 3.3 },
                        { x: 12, y: 3.1 },
                        { x: 13, y: 3 },
                        { x: 14, y: 2.9 },
                        { x: 15, y: 2.6 }
                    ],
                    [
                        { x: 0, y: 3 },
                        { x: 1, y: 2.2 },
                        { x: 2, y: 1.7 },
                        { x: 3, y: 1.6 },
                        { x: 4, y: 1.5 },
                        { x: 5, y: 1.5 },
                        { x: 6, y: 1.3 },
                        { x: 7, y: 1.3 },
                        { x: 8, y: 1.2 },
                        { x: 9, y: 1.1 },
                        { x: 10, y: 1 },
                        { x: 11, y: 1 },
                        { x: 12, y: 0.9 },
                        { x: 13, y: 0.9 },
                        { x: 14, y: 0.8 },
                        { x: 15, y: 0.7 }
                    ],
                    [
                        { x: 0, y: 25.2 },
                        { x: 1, y: 24.5 },
                        { x: 2, y: 23.5 },
                        { x: 3, y: 21.9 },
                        { x: 4, y: 20.8 },
                        { x: 5, y: 20.5 },
                        { x: 6, y: 19.5 },
                        { x: 7, y: 18.6 },
                        { x: 8, y: 17.6 },
                        { x: 9, y: 16.5 },
                        { x: 10, y: 15.3 },
                        { x: 11, y: 14.7 },
                        { x: 12, y: 13.8 },
                        { x: 13, y: 13.4 },
                        { x: 14, y: 12.8 },
                        { x: 15, y: 11.7 }
                    ],
                    [
                        { x: 0, y: 11.3 },
                        { x: 1, y: 10.8 },
                        { x: 2, y: 10.1 },
                        { x: 3, y: 9.8 },
                        { x: 4, y: 9.4 },
                        { x: 5, y: 9.2 },
                        { x: 6, y: 8.7 },
                        { x: 7, y: 8.2 },
                        { x: 8, y: 7.8 },
                        { x: 9, y: 7.4 },
                        { x: 10, y: 7.1 },
                        { x: 11, y: 6.7 },
                        { x: 12, y: 6.4 },
                        { x: 13, y: 6.1 },
                        { x: 14, y: 5.7 },
                        { x: 15, y: 5.4 }
                    ],
                    [
                        { x: 0, y: 22.8 },
                        { x: 1, y: 23.2 },
                        { x: 2, y: 23.3 },
                        { x: 3, y: 23.2 },
                        { x: 4, y: 23.1 },
                        { x: 5, y: 23 },
                        { x: 6, y: 23.3 },
                        { x: 7, y: 22.6 },
                        { x: 8, y: 22.3 },
                        { x: 9, y: 21.8 },
                        { x: 10, y: 21.7 },
                        { x: 11, y: 21.2 },
                        { x: 12, y: 20.8 },
                        { x: 13, y: 20.2 },
                        { x: 14, y: 19.8 },
                        { x: 15, y: 19.2 }
                    ],
                    [
                        { x: 0, y: 24 },
                        { x: 1, y: 26.8 },
                        { x: 2, y: 29.5 },
                        { x: 3, y: 30.7 },
                        { x: 4, y: 31.3 },
                        { x: 5, y: 31.2 },
                        { x: 6, y: 31.2 },
                        { x: 7, y: 31.2 },
                        { x: 8, y: 31.8 },
                        { x: 9, y: 32.3 },
                        { x: 10, y: 32.2 },
                        { x: 11, y: 32 },
                        { x: 12, y: 30.9 },
                        { x: 13, y: 29.7 },
                        { x: 14, y: 28.6 },
                        { x: 15, y: 28.8 }
                    ],
                    [
                        { x: 0, y: 0.7 },
                        { x: 1, y: 0.4 },
                        { x: 2, y: 0.5 },
                        { x: 3, y: 1.7 },
                        { x: 4, y: 3.4 },
                        { x: 5, y: 4.6 },
                        { x: 6, y: 6.6 },
                        { x: 7, y: 9.1 },
                        { x: 8, y: 10.6 },
                        { x: 9, y: 12.3 },
                        { x: 10, y: 14.4 },
                        { x: 11, y: 16 },
                        { x: 12, y: 17.8 },
                        { x: 13, y: 19.3 },
                        { x: 14, y: 21.1 },
                        { x: 15, y: 22.3 }
                    ],
                    [
                        { x: 0, y: 0 },
                        { x: 1, y: 0 },
                        { x: 2, y: 0.2 },
                        { x: 3, y: 0.3 },
                        { x: 4, y: 0.4 },
                        { x: 5, y: 0.4 },
                        { x: 6, y: 0.5 },
                        { x: 7, y: 0.7 },
                        { x: 8, y: 1 },
                        { x: 9, y: 1.3 },
                        { x: 10, y: 1.7 },
                        { x: 11, y: 2 },
                        { x: 12, y: 3.3 },
                        { x: 13, y: 4.4 },
                        { x: 14, y: 5.2 },
                        { x: 15, y: 6.1 }
                    ],
                    [
                        { x: 0, y: 0 },
                        { x: 1, y: 0 },
                        { x: 2, y: 0 },
                        { x: 3, y: 0 },
                        { x: 4, y: 0 },
                        { x: 5, y: 0 },
                        { x: 6, y: 0 },
                        { x: 7, y: 0 },
                        { x: 8, y: 0 },
                        { x: 9, y: 0 },
                        { x: 10, y: 0 },
                        { x: 11, y: 0.2 },
                        { x: 12, y: 0.3 },
                        { x: 13, y: 0.5 },
                        { x: 14, y: 0.5 },
                        { x: 15, y: 0.8 }
                    ],
                    [
                        { x: 0, y: 0 },
                        { x: 1, y: 0 },
                        { x: 2, y: 0 },
                        { x: 3, y: 0 },
                        { x: 4, y: 0 },
                        { x: 5, y: 0 },
                        { x: 6, y: 0 },
                        { x: 7, y: 0 },
                        { x: 8, y: 0 },
                        { x: 9, y: 0 },
                        { x: 10, y: 0 },
                        { x: 11, y: 0 },
                        { x: 12, y: 0 },
                        { x: 13, y: 0 },
                        { x: 14, y: 0.2 },
                        { x: 15, y: 0.3 }
                    ]
                ]
                
                // Function to get the proper High value for a Series.
                const getYHigh = (p, k) => {
                    let sum = 0
                    while (p >= 0) {
                        sum += data[p][k].y
                        p--
                    }
                    return sum
                }
                
                // Function to get the proper Low value for a Series.
                const getYLow = (p, k) => {
                    let sum = 0
                    while (p - 1 >= 0) {
                        sum += data[p - 1][k].y
                        p--
                    }
                    return sum
                }
                
                /**
                 * Fill each Area Series with the data created for them.
                 */
                data[0].forEach((point, i) => {
                    version.forEach((series, index) => {
                        // For the first series, only one Y value is needed.
                        if (index == 0) {
                            version[index].add({
                                x: point.x * dataFrequency,
                                y: point.y
                            })
                            // Rest of the series need both the High and Low values;
                            // Low is the previous Series' High value.
                        } else {
                            version[index].add(
                                {
                                    position: point.x * dataFrequency,
                                    high: getYHigh(index, i),
                                    low: getYLow(index, i)
                                }
                            )
                        }
                    })
                })
                "
            ),
        ),
        'bar_chart' => array(
                            'Example_1' => array(
                              'chart_name' => 'Bar Chart',
                              'icon' => plugins_url().'/LC-JS/images/icons/groupedBars.png',
                              'html_code' => '<div id="target" class="row content"></div>',
                              'javascript_code' => "// Extract required parts from LightningChartJS.
                              const {
                                  lightningChart,
                                  SolidFill,
                                  ColorRGBA,
                                  emptyLine,
                                  emptyFill,
                                  AutoCursorModes,
                                  ColorPalettes,
                                  UIOrigins,
                                  LegendBoxBuilders,
                                  AxisScrollStrategies,
                                  AxisTickStrategies,
                                  Themes
                              } = lcjs
                              
                              const lc = lightningChart(license_key)
                              
                              // Define an interface for creating vertical bars.
                              let barChart
                              {
                                  barChart = (options) => {
                                      const figureThickness = 10
                                      const figureGap = figureThickness * .25
                                      const groupGap = figureGap * 3.0
                                      const groups = []
                                      const categories = []
                              
                                      // Create a XY-Chart and add a RectSeries to it for rendering rectangles.
                                      const chart = lc.ChartXY(options)
                                          .setTitle('Grouped Bars (Employee Count)')
                                          .setAutoCursorMode(AutoCursorModes.onHover)
                                          // Disable mouse interactions (e.g. zooming and panning) of plotting area
                                          .setMouseInteractions(false)
                                          // Temporary fix for library-side bug. Remove after fixed.
                                          .setPadding({ bottom: 30 })
                              
                                      // X-axis of the series
                                      const axisX = chart.getDefaultAxisX()
                                          .setMouseInteractions(false)
                                          .setScrollStrategy(undefined)
                                          // Disable default ticks.
                                          .setTickStrategy(AxisTickStrategies.Empty)
                              
                                      // Y-axis of the series
                                      const axisY = chart.getDefaultAxisY()
                                          .setMouseInteractions(false)
                                          .setTitle('Number of Employees')
                                          .setInterval(0, 70)
                                          .setScrollStrategy(AxisScrollStrategies.fitting)
                              
                                      // cursor
                                      //#region
                                      // Modify AutoCursor.
                                      chart.setAutoCursor(cursor => cursor
                                          .disposePointMarker()
                                          .disposeTickMarkerX()
                                          .disposeTickMarkerY()
                                          .setGridStrokeXStyle(emptyLine)
                                          .setGridStrokeYStyle(emptyLine)
                                          .setResultTable((table) => {
                                              table
                                                  .setOrigin(UIOrigins.CenterBottom)
                                          })
                                      )
                                      // Define function that creates a Rectangle series (for each category), that adds cursor functionality to it
                                      const createSeriesForCategory = (category) => {
                                          const series = chart.addRectangleSeries()
                                          // Change how marker displays its information.
                                          series.setResultTableFormatter((builder, series, figure) => {
                                              // Find cached entry for the figure.
                                              let entry = {
                                                  name: category.name,
                                                  value: category.data[category.figures.indexOf(figure)]
                                              }
                                              // Parse result table content from values of 'entry'.
                                              return builder
                                                  .addRow('Department:', entry.name)
                                                  .addRow('# of employees:', String(entry.value))
                                          })
                                          return series
                                      }
                                      //#endregion
                                      // LegendBox
                                      //#region
                                      const margin = 4
                                      const legendBox = chart.addLegendBox(LegendBoxBuilders.VerticalLegendBox)
                                          .setPosition({ x: 15, y: 90 })
                                          .setOrigin(UIOrigins.LeftTop)
                                          .setMargin(margin)
                              
                                      //#endregion
                                      // Function redraws bars chart based on values of 'groups' and 'categories'
                                      const redraw = () => {
                                          let x = 0
                                          for (let groupIndex = 0; groupIndex < groups.length; groupIndex++) {
                                              const group = groups[groupIndex]
                                              const xStart = x
                                              for (const category of categories) {
                                                  const value = category.data[groupIndex]
                                                  if (value !== undefined) {
                                                      // Position figure of respective value.
                                                      const figure = category.figures[groupIndex]
                                                      figure.setDimensions({
                                                          x,
                                                          y: 0,
                                                          width: figureThickness,
                                                          height: value
                                                      })
                                                      // Figure gap
                                                      x += figureThickness + figureGap
                                                  }
                                              }
                                              // Position CustomTick
                                              group.tick.setValue((xStart + x - figureGap) / 2)
                              
                                              // Group gap
                                              x += groupGap
                                          }
                                          axisX.setInterval(-(groupGap + figureGap), x)
                                      }
                                      const addGroups = (names) => {
                                          for (const name of names)
                                              groups.push({
                                                  name,
                                                  tick: axisX.addCustomTick()
                                                      .setGridStrokeLength(0)
                                                      .setTextFormatter((_) => name)
                                                      .setMarker((marker) => marker
                                                          .setBackground((background) => background
                                                              .setFillStyle(emptyFill)
                                                              .setStrokeStyle(emptyLine)
                                                          )
                                                          .setTextFillStyle(new SolidFill({ color: ColorRGBA(170, 170, 170) }))
                                                      )
                                              })
                                      }
                                      const addCategory = (entry) => {
                                          // Each category has its own series.
                                          const series = createSeriesForCategory(entry)
                                              .setName(entry.name)
                                              .setDefaultStyle(figure => figure.setFillStyle(entry.fill))
                                          entry.figures = entry.data.map((value) => series.add({ x: 0, y: 0, width: 0, height: 0 }))
                                          legendBox.add(series, true, 'Department')
                                          categories.push(entry)
                                          redraw()
                                      }
                                      // Return public methods of a bar chart interface.
                                      return {
                                          addCategory,
                                          addGroups
                                      }
                                  }
                              }
                              
                              // Use bar chart interface to construct series
                              const chart = barChart({
                                  // theme: Themes.dark
                                  container:'target'
                              })
                              
                              // Add groups
                              chart.addGroups(['Finland', 'Germany', 'UK'])
                              
                              // Add categories of bars
                              const categories = ['Engineers', 'Sales', 'Marketing']
                              const palette = ColorPalettes.arctionWarm(categories.length)
                              const fillStyles = categories.map((_, i) => new SolidFill({ color: palette(i) }))
                              const data = [
                                  [48, 27, 24],
                                  [19, 40, 14],
                                  [33, 33, 62]
                              ]
                              data.forEach((data, i) =>
                                  chart.addCategory({
                                      name: categories[i],
                                      data,
                                      fill: fillStyles[i]
                                  })
                              )"
                                              ),
                    'Example_2' => array (
                    'chart_name' => 'Span Chart',
                    'icon' => plugins_url().'/LC-JS/images/icons/spanChart.png',
                    'html_code' => ' <div id="target" class="row content"></div>',
                                            'javascript_code' => "const {
                                                lightningChart,
                                                SolidFill,
                                                ColorRGBA,
                                                emptyLine,
                                                emptyFill,
                                                AxisTickStrategies,
                                                ColorPalettes,
                                                SolidLine,
                                                UIOrigins,
                                                UIElementBuilders,
                                                UILayoutBuilders,
                                                UIDraggingModes,
                                                Themes
                                            } = lcjs
                                            
                                            const lc = lightningChart(license_key)
                                            
                                            // Titles for span
                                            const titles = [
                                                'Certificate exams',
                                                'Interview',
                                                'Trainee training program',
                                                'Department competition training sessions',
                                                'Internet security training',
                                                'Scrum meeting',
                                                'Development meeting',
                                                'First Aid training',
                                                'Conquering the silence - How to improve your marketing'
                                            ]
                                            
                                            // Define an interface for creating span charts.
                                            let spanChart
                                            // User side SpanChart logic.
                                            {
                                                spanChart = () => {
                                                    // Create a XY-Chart and add a RectSeries to it for rendering rectangles.
                                                    const chart = lc.ChartXY({
                                                        // theme: Themes.dark,
                                                        container: 'target', 
                                                    })
                                                        .setTitle('Conference Room Reservations')
                                                        .setMouseInteractions(false)
                                                        // Disable default AutoCursor
                                                        .setAutoCursorMode(0)
                                                        .setPadding({ right: '2' })
                                                    const rectangles = chart.addRectangleSeries()
                                            
                                                    const axisX = chart.getDefaultAxisX()
                                                        .setMouseInteractions(false)
                                                        // Hide default ticks, instead rely on CustomTicks.
                                                        .setTickStrategy(AxisTickStrategies.Empty)
                                            
                                                    const axisY = chart.getDefaultAxisY()
                                                        .setMouseInteractions(false)
                                                        .setTitle('Conference Room')
                                                        // Hide default ticks, instead rely on CustomTicks.
                                                        .setTickStrategy(AxisTickStrategies.Empty)
                                            
                                                    let y = 0
                                                    for (let i = 8; i <= 20; i++) {
                                                        const label = i > 12 ? i - 12 + 'PM' : i + 'AM'
                                                        axisX.addCustomTick()
                                                            .setValue(i)
                                                            .setTickLength(4)
                                                            .setGridStrokeLength(0)
                                                            .setTextFormatter(_ => label)
                                                            .setMarker(marker => marker
                                                                .setBackground(background => background
                                                                    .setFillStyle(emptyFill)
                                                                    .setStrokeStyle(emptyLine)
                                                                )
                                                                .setTextFillStyle(new SolidFill({ color: ColorRGBA(170, 170, 170) }))
                                                            )
                                                    }
                                            
                                            
                                                    const figureHeight = 10
                                                    const figureThickness = 10
                                                    const figureGap = figureThickness * .5
                                                    const fitAxes = () => {
                                                        // Custom fitting for some additional margins
                                                        axisY.setInterval(y, figureHeight * .5)
                                                    }
                                            
                                                    let customYRange = figureHeight + figureGap * 1.6
                                                    const addCategory = (category) => {
                                                        const categoryY = y
                                            
                                                        const addSpan = (i, min, max, index) => {
                                                            // Add rect
                                                            const rectDimensions = {
                                                                x: min,
                                                                y: categoryY - figureHeight,
                                                                width: max - min,
                                                                height: figureHeight
                                                            }
                                                            //Add element for span labels
                                                            const spanText = chart.addUIElement(
                                                                UILayoutBuilders.Row,
                                                                { x: axisX.scale, y: axisY.scale }
                                                            )
                                                                .setOrigin(UIOrigins.Center)
                                                                .setDraggingMode(UIDraggingModes.notDraggable)
                                                                .setPosition({
                                                                    x: (min + max) / 2,
                                                                    y: rectDimensions.y + 5,
                                                                })
                                                                .setBackground(background => background
                                                                    .setStrokeStyle(emptyLine)
                                                                )
                                            
                                                            spanText.addElement(
                                                                UIElementBuilders.TextBox
                                                                    .addStyler(
                                                                        textBox =>
                                                                            textBox.setFont(fontSettings => fontSettings.setSize(10)).setText(titles[index])
                                                                                .setTextFillStyle(new SolidFill().setColor(ColorRGBA(255, 255, 255)))
                                            
                                                                    )
                                            
                                                            )
                                                            if (index != i) {
                                                                customYRange = customYRange + figureHeight + 1
                                            
                                                            }
                                                            fitAxes()
                                                            // Return figure
                                                            return rectangles.add(rectDimensions)
                                                        }
                                            
                                                        // Add custom tick for category
                                                        axisY.addCustomTick()
                                                            .setValue(y - figureHeight * 0.5)
                                                            .setGridStrokeLength(0)
                                                            .setTextFormatter(_ => category)
                                                            .setMarker(marker => marker
                                                                .setPadding(4)
                                                                .setBackground(background => background
                                                                    .setFillStyle(emptyFill)
                                                                    .setStrokeStyle(emptyLine)
                                                                )
                                                                .setTextFillStyle(new SolidFill({ color: ColorRGBA(170, 170, 170) }))
                                                            )
                                                        y -= figureHeight * 1.5
                                            
                                                        fitAxes()
                                                        // Return interface for category.
                                                        return {
                                                            addSpan
                                                        }
                                                    }
                                                    // Return interface for span chart.
                                                    return {
                                                        addCategory
                                                    }
                                                }
                                            }
                                            
                                            // Use the interface for example.
                                            const chart = spanChart()
                                            const categories = ['5 chair room', '5 chair room', '5 chair room', '10 chair room', '10 chair room', '20 chair room', 'Conference Hall'].map((name) => chart.addCategory(name))
                                            const colorPalette = ColorPalettes.flatUI(categories.length)
                                            const fillStyles = categories.map((_, i) => new SolidFill({ color: colorPalette(i) }))
                                            const strokeStyle = new SolidLine({
                                                fillStyle: new SolidFill({ color: ColorRGBA(0, 0, 0) }),
                                                thickness: 2
                                            })
                                            const spans = [
                                                [[10, 13], [16, 18]],
                                                [[8, 17]],
                                                [[12, 20]],
                                                [[9, 17]],
                                                [[10, 12], [15, 19]],
                                                [[11, 16]],
                                                [[9, 18]]
                                            ]
                                            
                                            let index = 0;
                                            spans.forEach((values, i) => {
                                                values.forEach((value, j) => {
                                                    categories[i].addSpan(i, value[0], value[1], index)
                                                        .setFillStyle(fillStyles[i])
                                                        .setStrokeStyle(strokeStyle)
                                                    index = index + 1
                                                }
                                                )
                                            })"
            ),  
                    'Example_3' => array (
                        'chart_name' => 'Mosaic Chart',
                        'icon' => plugins_url().'/LC-JS/images/icons/mosaicChart.png',
                        'html_code' => ' <div id="target" class="row content"></div>',
                                            'javascript_code' => "const {
                                                lightningChart,
                                                SolidFill,
                                                ColorRGBA,
                                                DefaultLibraryStyle,
                                                emptyLine,
                                                emptyFill,
                                                UIElementBuilders,
                                                UIBackgrounds,
                                                UIOrigins,
                                                AxisTickStrategies,
                                                Themes
                                            } = lcjs
                                            
                                            const lc = lightningChart(license_key)
                                            
                                            // Define an interface for creating mosaic charts.
                                            let mosaicChart
                                            // User side MosaicChart logic.
                                            {
                                                mosaicChart = () => {
                                                    // Create a XY-Chart and add a RectSeries to it for rendering rectangles.
                                                    const chart = lc.ChartXY({
                                                        // theme: Themes.dark 
                                                        container: 'target',
                                                    })
                                                        .setTitle('Controlled Group Testing')
                                                        .setMouseInteractions(false)
                                                        // Disable default AutoCursor
                                                        .setAutoCursorMode(0)
                                                    const rectangles = chart.addRectangleSeries()
                                            
                                                    const bottomAxis = chart.getDefaultAxisX()
                                                        .setInterval(0, 100)
                                                        .setScrollStrategy(undefined)
                                                        .setMouseInteractions(false)
                                                        .setTitle('%')
                                                    const leftAxis = chart.getDefaultAxisY()
                                                        .setInterval(0, 100)
                                                        .setMouseInteractions(false)
                                                        // Hide default ticks of left Axis.
                                                        .setTickStrategy(AxisTickStrategies.Empty)
                                                    const rightAxis = chart.addAxisY(true)
                                                        .setInterval(0, 100)
                                                        .setScrollStrategy(undefined)
                                                        .setMouseInteractions(false)
                                                        .setTitle('%')
                                                    const topAxis = chart.addAxisX(true)
                                                        .setInterval(0, 100)
                                                        .setMouseInteractions(false)
                                                        // Hide default ticks of top Axis.
                                                        .setTickStrategy(AxisTickStrategies.Empty)
                                            
                                                    // Create marker for the top of each column.
                                                    const categoryMarkerBuilder = UIElementBuilders.PointableTextBox
                                                        // Set the type of background for the marker.
                                                        .setBackground(UIBackgrounds.Pointer)
                                                        // Style the marker.
                                                        .addStyler((marker) => marker
                                                            // Hide Background. UIBackgrounds.None doesn't work for CustomTicks right now.
                                                            .setBackground((background) => background
                                                                .setFillStyle(emptyFill)
                                                                .setStrokeStyle(emptyLine)
                                                            )
                                                            .setTextFillStyle(new SolidFill({ color: ColorRGBA(170, 170, 170) }))
                                                        )
                                                    // Create text on top of each section.
                                                    const subCategoryLabelBuilder = UIElementBuilders.TextBox
                                                        // Style the label.
                                                        .addStyler(label => label
                                                            // Set the origin point and fillStyle (color) for the label.
                                                            .setOrigin(UIOrigins.Center)
                                                            .setTextFillStyle(new SolidFill().setColor(ColorRGBA(255, 255, 255)))
                                                            .setMouseInteractions(false)
                                                        )
                                            
                                                    const categories = []
                                                    const yCategories = []
                                                    const subCategories = []
                                                    let margin = 4
                                            
                                            
                                                    // Recreate rectangle figures from scratch.
                                                    const _updateChart = () => {
                                                        const px = { x: bottomAxis.scale.getPixelSize(), y: rightAxis.scale.getPixelSize() }
                                                        // Remove already existing figures.
                                                        rectangles.clear()
                                                        // Make new figures from each category.
                                                        const sumCategoryValues = categories.reduce((prev, cur) => prev + cur.value, 0)
                                                        if (sumCategoryValues > 0) {
                                                            let xPos = 0
                                                            // For each category on a single column, recreate the marker to the left of the chart.
                                                            for (const yCategory of yCategories) {
                                                                yCategory.tick
                                                                    .setTextFormatter(_ => yCategory.name)
                                                                    .setValue(yCategory.value)
                                                                    .restoreMarker()
                                                            }
                                                            // For each category (or column)
                                                            for (const category of categories) {
                                                                // Calculate the correct value to display for each category
                                                                const relativeCategoryValue = 100 * category.value / sumCategoryValues
                                                                const sumSubCategoryValues = category.subCategories.reduce((prev, cur) => prev + cur.value, 0)
                                                                // If there are subCategories for the column
                                                                if (sumSubCategoryValues > 0) {
                                                                    // Recreate the tick to display above each category and set the correct value to it
                                                                    category.tick
                                                                        .setTextFormatter(_ => category.name + ' (' + Math.round(relativeCategoryValue) + '%)')
                                                                        .setValue(xPos + relativeCategoryValue / 2)
                                                                        .restoreMarker()
                                                                    let yPos = 0
                                                                    for (const subCategory of category.subCategories) {
                                                                        // Calculate proper value for the subCategory
                                                                        const relativeSubCategoryValue = 100 * subCategory.value / sumSubCategoryValues
                                                                        if (relativeSubCategoryValue > 0) {
                                                                            const rectangleDimensions = {
                                                                                x: xPos + margin * px.x,
                                                                                y: yPos + margin * px.y,
                                                                                width: relativeCategoryValue - 2 * margin * px.x,
                                                                                height: relativeSubCategoryValue - 2 * margin * px.y
                                                                            }
                                                                            // Create a rectangle to represent the subCategory
                                                                            rectangles.add(rectangleDimensions)
                                                                                .setFillStyle(subCategory.subCategory.fillStyle)
                                                                                .setStrokeStyle(emptyLine)
                                                                            // Recreate the label for the subCategory and update the value for it
                                                                            subCategory.label
                                                                                .setText(Math.round(relativeSubCategoryValue) + '%')
                                                                                .setPosition({ x: xPos + relativeCategoryValue / 2, y: yPos + relativeSubCategoryValue / 2 })
                                                                                .restore()
                                                                        } else
                                                                            // The subCategory is not shown, so we can dispose of its label.
                                                                            subCategory.label.dispose()
                                                                        yPos += relativeSubCategoryValue
                                                                    }
                                                                } else {
                                                                    // There are no subCategories for the column, so the elements related to it can be disposed.
                                                                    category.tick
                                                                        .disposeMarker()
                                                                    category.subCategories.forEach(sub => sub.label.dispose())
                                                                }
                                                                xPos += relativeCategoryValue
                                                            }
                                                        }
                                                    }
                                                    // Method to add a new subCategory to the chart.
                                                    const addSubCategory = () => {
                                                        const subCategory = {
                                                            fillStyle: DefaultLibraryStyle.seriesFill,
                                                            setFillStyle(fillStyle) {
                                                                this.fillStyle = fillStyle
                                                                // Refresh the chart.
                                                                _updateChart()
                                                                return this
                                                            }
                                                        }
                                                        subCategories.push(subCategory)
                                                        return subCategory
                                                    }
                                                    // Method to add a new main category to the chart.
                                                    const addCategory = (name) => {
                                                        const category = {
                                                            name,
                                                            value: 0,
                                                            tick: topAxis.addCustomTick(categoryMarkerBuilder)
                                                                .setGridStrokeStyle(emptyLine)
                                                            ,
                                                            subCategories: [],
                                                            setCategoryValue(value) {
                                                                this.value = value
                                                                _updateChart()
                                                                return this
                                                            },
                                                            setSubCategoryValue(subCategory, value) {
                                                                const existing = this.subCategories.find(a => a.subCategory == subCategory)
                                                                if (existing !== undefined) {
                                                                    existing.value = value
                                                                } else {
                                                                    this.subCategories.push({
                                                                        subCategory,
                                                                        value,
                                                                        label: chart.addUIElement(subCategoryLabelBuilder, { x: bottomAxis.scale, y: rightAxis.scale })
                                                                    })
                                                                }
                                                                _updateChart()
                                                                return this
                                                            }
                                                        }
                                                        categories.push(category)
                                                        return category
                                                    }
                                                    // Method to add subCategory markers.
                                                    const addYCategory = (name, value) => {
                                                        const yCategory = {
                                                            name,
                                                            value: value,
                                                            tick: leftAxis.addCustomTick(categoryMarkerBuilder)
                                                                .setGridStrokeStyle(emptyLine),
                                                            setCategoryYValue(value) {
                                                                this.value = value
                                                                _updateChart()
                                                                return this
                                                            }
                                                        }
                                                        yCategories.push(yCategory)
                                                        return yCategory
                                                    }
                                                    // Return interface for mosaic chart
                                                    return {
                                                        addSubCategory,
                                                        addCategory,
                                                        addYCategory
                                                    }
                                                }
                                            }
                                            
                                            // Use the interface for example.
                                            const chart = mosaicChart()
                                            chart.addYCategory('Refreshed', 80)
                                            chart.addYCategory('No Effect', 40)
                                            chart.addYCategory('Caused Exhaustion', 12)
                                            
                                            const subCategory_exhaust = chart.addSubCategory()
                                                .setFillStyle(new SolidFill().setColor(ColorRGBA(200, 0, 0)))
                                            const subCategory_noEffect = chart.addSubCategory()
                                                .setFillStyle(new SolidFill().setColor(ColorRGBA(240, 190, 0)))
                                            const subCategory_refresh = chart.addSubCategory()
                                                .setFillStyle(new SolidFill().setColor(ColorRGBA(0, 180, 0)))
                                            
                                            chart.addCategory('With caffeine')
                                                .setCategoryValue(48)
                                                .setSubCategoryValue(subCategory_exhaust, 25)
                                                .setSubCategoryValue(subCategory_noEffect, 35)
                                                .setSubCategoryValue(subCategory_refresh, 40)
                                            
                                            chart.addCategory('Decaffeinated')
                                                .setCategoryValue(32)
                                                .setSubCategoryValue(subCategory_exhaust, 10)
                                                .setSubCategoryValue(subCategory_noEffect, 45)
                                                .setSubCategoryValue(subCategory_refresh, 45)
                                            
                                            chart.addCategory('Placebo product')
                                                .setCategoryValue(20)
                                                .setSubCategoryValue(subCategory_exhaust, 20)
                                                .setSubCategoryValue(subCategory_noEffect, 50)
                                                .setSubCategoryValue(subCategory_refresh, 30)"
                        ),
                    ),
                              
                  'area_series' => array(
                          'Example_1' => array(
                          'chart_name' => 'Multiple Areas',
                          'icon' => plugins_url().'/LC-JS/images/icons/multipleAreas.png',
                          'html_code' => '<div id="target" class="row content"></div>',
                          'javascript_code' => "const {
                                                    lightningChart,
                                                    AreaSeriesTypes,
                                                    ColorPalettes,
                                                    SolidFill,
                                                    UIOrigins,
                                                    UIElementBuilders,
                                                    LegendBoxBuilders,
                                                    UIButtonPictures,
                                                    Themes
                                                } = lcjs
                                                
                                                // ----- Cache styles -----
                                                const palette = ColorPalettes.fullSpectrum(10)
                                                const solidFills = [3, 0].map(palette).map(color => new SolidFill({ color }))
                                                const opaqueFills = solidFills.map(fill => fill.setA(150))
                                                
                                                // Create a XY Chart.
                                                const xyChart = lightningChart(license_key).ChartXY({
                                                    // theme: Themes.dark 
                                                    container: 'target'
                                                })
                                                    .setTitle('Expected Profits To Expenses')
                                                    .setPadding({ right: 2 })
                                                
                                                // Create a LegendBox as part of the chart.
                                                const legend = xyChart.addLegendBox(LegendBoxBuilders.HorizontalLegendBox)
                                                    .setPosition({ x: 5, y: 95 })
                                                    .setOrigin(UIOrigins.LeftTop)
                                                
                                                // ---- Add multiple Area series with different baselines and direction. ----
                                                // Create semi-transparent red area to draw points above the baseline.
                                                const areaProfit = xyChart.addAreaSeries({ type: AreaSeriesTypes.Positive })
                                                    .setFillStyle(opaqueFills[0])
                                                    .setStrokeStyle(stroke => stroke.setFillStyle(solidFills[0]))
                                                    .setName('Profits')
                                                
                                                // Create semi-transparent orange area to draw points below the baseline.
                                                const areaExpense = xyChart.addAreaSeries({ type: AreaSeriesTypes.Negative })
                                                    .setFillStyle(opaqueFills[1])
                                                    .setStrokeStyle(stroke => stroke.setFillStyle(solidFills[1]))
                                                    .setName('Expenses')
                                                
                                                // Set Axis nicely
                                                xyChart.getDefaultAxisX()
                                                    .setTitle('Units Produced')
                                                xyChart.getDefaultAxisY()
                                                    .setTitle('USD')
                                                
                                                const profitData = [
                                                    { x: 0, y: 0 },
                                                    { x: 10, y: 21 },
                                                    { x: 20, y: 59 },
                                                    { x: 30, y: 62 },
                                                    { x: 40, y: 78 },
                                                    { x: 50, y: 85 },
                                                    { x: 60, y: 95 },
                                                    { x: 70, y: 98 },
                                                    { x: 80, y: 103 },
                                                    { x: 90, y: 110 },
                                                    { x: 100, y: 112 },
                                                    { x: 110, y: 126 },
                                                    { x: 120, y: 132 },
                                                    { x: 130, y: 170 },
                                                    { x: 140, y: 172 },
                                                    { x: 150, y: 202 },
                                                    { x: 160, y: 228 },
                                                    { x: 170, y: 267 },
                                                    { x: 180, y: 300 },
                                                    { x: 190, y: 310 },
                                                    { x: 200, y: 320 },
                                                    { x: 210, y: 329 },
                                                    { x: 220, y: 336 },
                                                    { x: 230, y: 338 },
                                                    { x: 240, y: 343 },
                                                    { x: 250, y: 352 },
                                                    { x: 260, y: 355 },
                                                    { x: 270, y: 390 },
                                                    { x: 280, y: 392 },
                                                    { x: 290, y: 418 },
                                                    { x: 300, y: 421 },
                                                    { x: 310, y: 430 },
                                                    { x: 320, y: 434 },
                                                    { x: 330, y: 468 },
                                                    { x: 340, y: 472 },
                                                    { x: 350, y: 474 },
                                                    { x: 360, y: 480 },
                                                    { x: 370, y: 506 },
                                                    { x: 380, y: 545 },
                                                    { x: 390, y: 548 },
                                                    { x: 400, y: 552 },
                                                    { x: 410, y: 584 },
                                                    { x: 420, y: 612 },
                                                    { x: 430, y: 619 },
                                                    { x: 440, y: 627 },
                                                    { x: 450, y: 657 },
                                                    { x: 460, y: 669 },
                                                    { x: 470, y: 673 },
                                                    { x: 480, y: 695 },
                                                    { x: 490, y: 702 },
                                                    { x: 500, y: 710 }
                                                ]
                                                const expensesData = [
                                                    { x: 0, y: 0 },
                                                    { x: 10, y: -58 },
                                                    { x: 20, y: -61 },
                                                    { x: 30, y: -62 },
                                                    { x: 40, y: -66 },
                                                    { x: 50, y: -88 },
                                                    { x: 60, y: -93 },
                                                    { x: 70, y: -124 },
                                                    { x: 80, y: -126 },
                                                    { x: 90, y: -136 },
                                                    { x: 100, y: -152 },
                                                    { x: 110, y: -156 },
                                                    { x: 120, y: -190 },
                                                    { x: 130, y: -199 },
                                                    { x: 140, y: -200 },
                                                    { x: 150, y: -208 },
                                                    { x: 160, y: -210 },
                                                    { x: 170, y: -235 },
                                                    { x: 180, y: -270 },
                                                    { x: 190, y: -299 },
                                                    { x: 200, y: -321 },
                                                    { x: 210, y: -342 },
                                                    { x: 220, y: -350 },
                                                    { x: 230, y: -360 },
                                                    { x: 240, y: -374 },
                                                    { x: 250, y: -413 },
                                                    { x: 260, y: -433 },
                                                    { x: 270, y: -447 },
                                                    { x: 280, y: -449 },
                                                    { x: 290, y: -454 },
                                                    { x: 300, y: -461 },
                                                    { x: 310, y: -461 },
                                                    { x: 320, y: -492 },
                                                    { x: 330, y: -496 },
                                                    { x: 340, y: -518 },
                                                    { x: 350, y: -522 },
                                                    { x: 360, y: -557 },
                                                    { x: 370, y: -562 },
                                                    { x: 380, y: -596 },
                                                    { x: 390, y: -599 },
                                                    { x: 400, y: -609 },
                                                    { x: 410, y: -611 },
                                                    { x: 420, y: -628 },
                                                    { x: 430, y: -635 },
                                                    { x: 440, y: -636 },
                                                    { x: 450, y: -643 },
                                                    { x: 460, y: -643 },
                                                    { x: 470, y: -647 },
                                                    { x: 480, y: -648 },
                                                    { x: 490, y: -659 },
                                                    { x: 500, y: -665 }
                                                ]
                                                
                                                // ---- Generate points using 'xydata'-library and add it to every plot. ----
                                                profitData.forEach((point) => { areaProfit.add(point) })
                                                expensesData.forEach((point) => { areaExpense.add(point) })
                                                
                                                // Set the custom result table for both areaSeries
                                                areaProfit
                                                    .setResultTableFormatter((builder, series, position, highValue, lowValue) => {
                                                        return builder
                                                            .addRow('Profits')
                                                            .addRow('Amount: $' + highValue.toFixed(0))
                                                            .addRow('Units Produced: ' + position.toFixed(0))
                                                    })
                                                areaExpense
                                                    .setResultTableFormatter((builder, series, position, highValue, lowValue) => {
                                                        return builder
                                                            .addRow('Expenses')
                                                            .addRow('Amount: $' + highValue.toFixed(0) * -1)
                                                            .addRow('Units Produced: ' + position.toFixed(0))
                                                    })
                                                
                                                // Add series to LegendBox and style entries.
                                                legend.add(
                                                    areaProfit,
                                                    true,
                                                    'Expected Profits To Expenses',
                                                    UIElementBuilders.CheckBox
                                                        .setPictureOff(UIButtonPictures.Circle)
                                                        .setPictureOn(UIButtonPictures.Circle)
                                                )
                                                legend.add(
                                                    areaExpense,
                                                    true,
                                                    'Expected Profits To Expenses',
                                                    UIElementBuilders.CheckBox
                                                        .setPictureOff(UIButtonPictures.Circle)
                                                        .setPictureOn(UIButtonPictures.Circle)
                                                )"
                          ),
  
                'Example_2' => array(
                      'chart_name' => 'Area Range',
                      'icon' => plugins_url().'/LC-JS/images/icons/areaRange.png',
                      'html_code' => '<div id="target" class="row content"></div>',
                      'javascript_code' => "// Extract required parts from LightningChartJS.
                                                const {
                                                    lightningChart,
                                                    AxisTickStrategies,
                                                    Themes
                                                } = lcjs
                            
                                                // Decide on an origin for DateTime axis.
                                                const dateOrigin = new Date(2018, 0, 1)
                            
                                                // Create a XY Chart.
                                                const chart = lightningChart(license_key).ChartXY({
                                                    // theme: Themes.dark
                                                    container: 'target'
                                                })
                                                //Cache default X Axis for use.
                                                const axisX = chart.getDefaultAxisX()
                                                // Use DateTime TickStrategy, using the origin specified above.
                                                // axisX
                                                //     .setTickStrategy(
                                                //         AxisTickStrategies.DateTime,
                                                //         (tickStrategy) => tickStrategy.setDateOrigin(dateOrigin)
                                                //     )
                                                chart.setTitle('Area Range')
                                                // Add AreaRange Series
                                                const areaRange = chart.addAreaRangeSeries()
                                                // Modify the ResultTable formatting.
                                                areaRange.setResultTableFormatter((builder, series, figure, yMax, yMin) => {
                                                    return builder
                                                        .addRow('Actual vs Expected Share Prices of Company')
                                                        .addRow('Date: ' + axisX.formatValue(figure))
                                                        .addRow('Actual: ' + yMax.toFixed(2) + ' $')
                                                        .addRow('Expected: ' + yMin.toFixed(2) + ' $')
                                                })
                                                chart.getDefaultAxisY()
                                                    .setTitle('Share price ($)')
                                                // Data for the AreaRange Series
                                                const areaRangeData = [
                                                    {
                                                        x: 0, yMax: 24, yMin: -15
                                                    },
                                                    {
                                                        x: 6, yMax: 11, yMin: -13
                                                    },
                                                    {
                                                        x: 13, yMax: 16, yMin: -11
                                                    },
                                                    {
                                                        x: 20, yMax: 26, yMin: -8
                                                    },
                                                    {
                                                        x: 27, yMax: -2, yMin: -4
                                                    },
                                                    {
                                                        x: 34, yMax: -16, yMin: 2
                                                    },
                                                    {
                                                        x: 41, yMax: -10, yMin: 9
                                                    },
                                                    {
                                                        x: 48, yMax: 3, yMin: 17
                                                    },
                                                    {
                                                        x: 55, yMax: -1, yMin: 26
                                                    },
                                                    {
                                                        x: 62, yMax: 6, yMin: 35
                                                    },
                                                    {
                                                        x: 69, yMax: 17, yMin: 39
                                                    },
                                                    {
                                                        x: 76, yMax: -10, yMin: 41
                                                    },
                                                    {
                                                        x: 83, yMax: -8, yMin: 42
                                                    },
                                                    {
                                                        x: 90, yMax: 13, yMin: 41
                                                    },
                                                    {
                                                        x: 97, yMax: -12, yMin: 39
                                                    },
                                                    {
                                                        x: 104, yMax: 6, yMin: 37
                                                    },
                                                    {
                                                        x: 111, yMax: -10, yMin: 36.5
                                                    },
                                                    {
                                                        x: 118, yMax: 27, yMin: 36
                                                    },
                                                    {
                                                        x: 125, yMax: 52, yMin: 38
                                                    },
                                                    {
                                                        x: 132, yMax: 59, yMin: 42
                                                    },
                                                    {
                                                        x: 139, yMax: 64, yMin: 50
                                                    },
                            
                                                    {
                                                        x: 146, yMax: 59, yMin: 53
                                                    },
                                                    {
                                                        x: 153, yMax: 71, yMin: 54
                                                    },
                                                    {
                                                        x: 160, yMax: 63, yMin: 53
                                                    },
                                                    {
                                                        x: 167, yMax: 79, yMin: 52
                                                    },
                                                    {
                                                        x: 174, yMax: 62, yMin: 51
                                                    },
                                                    {
                                                        x: 181, yMax: 81, yMin: 49
                                                    },
                                                    {
                                                        x: 188, yMax: 53, yMin: 48
                                                    },
                                                    {
                                                        x: 195, yMax: 89, yMin: 48.5
                                                    },
                                                    {
                                                        x: 202, yMax: 99, yMin: 49
                                                    },
                                                    {
                                                        x: 209, yMax: 79, yMin: 51
                                                    },
                                                    {
                                                        x: 216, yMax: 51, yMin: 51
                                                    },
                                                    {
                                                        x: 223, yMax: 36, yMin: 51
                                                    },
                                                    {
                                                        x: 230, yMax: 79, yMin: 50
                                                    },
                                                    {
                                                        x: 237, yMax: 49, yMin: 53
                                                    },
                                                    {
                                                        x: 244, yMax: 33, yMin: 55
                                                    },
                                                    {
                                                        x: 251, yMax: 51, yMin: 58
                                                    },
                                                    {
                                                        x: 258, yMax: 43, yMin: 60
                                                    },
                                                    {
                                                        x: 265, yMax: 72, yMin: 61
                                                    },
                                                    {
                                                        x: 272, yMax: 82, yMin: 60.5
                                                    },
                                                    {
                                                        x: 279, yMax: 73, yMin: 59
                                                    },
                            
                                                    {
                                                        x: 286, yMax: 73, yMin: 58
                                                    },
                                                    {
                                                        x: 293, yMax: 67, yMin: 55
                                                    },
                                                    {
                                                        x: 300, yMax: 43, yMin: 53
                                                    },
                                                    {
                                                        x: 307, yMax: 12, yMin: 54
                                                    },
                                                    {
                                                        x: 314, yMax: 26, yMin: 54.5
                                                    },
                                                    {
                                                        x: 321, yMax: 46, yMin: 56
                                                    },
                                                    {
                                                        x: 328, yMax: 43, yMin: 58
                                                    },
                                                    {
                                                        x: 335, yMax: 72, yMin: 60
                                                    },
                                                    {
                                                        x: 342, yMax: 82, yMin: 62.5
                                                    },
                                                    {
                                                        x: 349, yMax: 73, yMin: 64
                                                    },
                                                    {
                                                        x: 356, yMax: 92, yMin: 65
                                                    },
                                                    {
                                                        x: 363, yMax: 107, yMin: 66
                                                    }
                                                ]
                            
                                                areaRangeData.forEach((point, i) => {
                                                    areaRange.add({ position: (point.x * 24 * 60 * 60 * 1000), high: point.yMax, low: point.yMin })
                                                })"       
                                                ),
                      'Example_3' => array(
                      'chart_name' => 'Layered Areas',
                      'icon' => plugins_url().'/LC-JS/images/icons/layeredAreas.png',
                      'html_code' => '<div id="target" class="row content"></div>', 
                      'javascript_code' => "const {
                                                    lightningChart,
                                                    ColorPalettes,
                                                    SolidFill,
                                                    emptyLine,
                                                    emptyFill,
                                                    AxisTickStrategies,
                                                    ColorRGBA,
                                                    Themes
                                                    } = lcjs
                                
                                                    // ----- Cache styles -----
                                                    const palette = ColorPalettes.arctionWarm(2)
                                                    const solidFills = [2, 1, 0].map(palette).map((color) => new SolidFill({ color }))
                                                    const opaqueFills = solidFills.map(fill => fill.setA(150))
                                
                                                    // Create a XY Chart.
                                                    const xyChart = lightningChart(license_key).ChartXY({
                                                    // theme: Themes.dark 
                                                    container: 'target'
                                                    })
                                                    .setTitle('Product version distribution')
                                                    .setMouseInteractions(true)
                                                    .setPadding({ right: 25 })
                                
                                                    // Custom tick labels for X Axis.
                                                    const reportTableXlable = ['Jan-17', 'Feb-17', 'Mar-17', 'Apr-17', 'May-17', 'Jun-17', 'Jul-17',
                                                    'Aug-17', 'Sep-17', 'Oct-17', 'Nov-17', 'Dec-17']
                                
                                                    // Creating AreaSeries
                                                    const createAreaSeries = (versionNumber, index) => {
                                                    return xyChart.addAreaSeries()
                                                        .setName(`Version ${versionNumber}`)
                                                        .setFillStyle(opaqueFills[index])
                                                        .setStrokeStyle((stroke) => stroke.setFillStyle(solidFills[index]))
                                                        .setResultTableFormatter((builder, series, xValue, yValue) => {
                                                            return builder
                                                                .addRow(series.getName())
                                                                .addRow('Date: ', reportTableXlable[parseInt(xValue)])
                                                                .addRow('Distribution: ' + yValue.toFixed(2))
                                                        });
                                                    }
                                
                                                    // Create the area series
                                                    const V1Area = createAreaSeries(1, 0)
                                                    const V2Area = createAreaSeries(2, 2)
                                                    const V3Area = createAreaSeries(3, 2)
                                                    const V4Area = createAreaSeries(4, 0)
                                                    const V5Area = createAreaSeries(5, 1)
                                                    const V6Area = createAreaSeries(6, 2)
                                                    const V7Area = createAreaSeries(7, 0)
                                                    const V8Area = createAreaSeries(8, 1)
                                                    const V9Area = createAreaSeries(9, 2)
                                                    const V10Area = createAreaSeries(10, 0)
                                                    const V11Area = createAreaSeries(11, 1)
                                                    const V12Area = createAreaSeries(12, 2)
                                
                                                    // DataSet for all versions
                                                    const version1 = [{
                                                    x: 'Jan-17', y: 1.3
                                                    }, {
                                                    x: 'Feb-17', y: 1.1
                                                    }, {
                                                    x: 'Mar-17', y: 1
                                                    }, {
                                                    x: 'Apr-17', y: 0.8
                                                    }, {
                                                    x: 'May-17', y: 0.7
                                                    }, {
                                                    x: 'Jun-17', y: 0.6
                                                    }, {
                                                    x: 'Jul-17', y: 0.6
                                                    }, {
                                                    x: 'Aug-17', y: 0.5
                                                    }, {
                                                    x: 'Sep-17', y: 0.4
                                                    }, {
                                                    x: 'Oct-17', y: 0.4
                                                    }, {
                                                    x: 'Nov-17', y: 0.3
                                                    }, {
                                                    x: 'Dec-17', y: 0.3
                                                    }]
                                
                                                    const version2 = [{
                                                    x: 'Jan-17', y: 4.9
                                                    }, {
                                                    x: 'Feb-17', y: 4
                                                    }, {
                                                    x: 'Mar-17', y: 3.7
                                                    }, {
                                                    x: 'Apr-17', y: 3.2
                                                    }, {
                                                    x: 'May-17', y: 2.8
                                                    }, {
                                                    x: 'Jun-17', y: 2.4
                                                    }, {
                                                    x: 'Jul-17', y: 2.3
                                                    }, {
                                                    x: 'Aug-17', y: 2
                                                    }, {
                                                    x: 'Sep-17', y: 1.7
                                                    }, {
                                                    x: 'Oct-17', y: 1.5
                                                    }, {
                                                    x: 'Nov-17', y: 1.2
                                                    }, {
                                                    x: 'Dec-17', y: 1.1
                                                    }]
                                
                                                    const version3 = [{
                                                    x: 'Jan-17', y: 6.8
                                                    }, {
                                                    x: 'Feb-17', y: 5.9
                                                    }, {
                                                    x: 'Mar-17', y: 5.4
                                                    }, {
                                                    x: 'Apr-17', y: 4.6
                                                    }, {
                                                    x: 'May-17', y: 4.1
                                                    }, {
                                                    x: 'Jun-17', y: 3.5
                                                    }, {
                                                    x: 'Jul-17', y: 3.3
                                                    }, {
                                                    x: 'Aug-17', y: 3
                                                    }, {
                                                    x: 'Sep-17', y: 2.6
                                                    }, {
                                                    x: 'Oct-17', y: 2.2
                                                    }, {
                                                    x: 'Nov-17', y: 1.8
                                                    }, {
                                                    x: 'Dec-17', y: 1.5
                                                    }]
                                
                                                    const version4 = [{
                                                    x: 'Jan-17', y: 2
                                                    }, {
                                                    x: 'Feb-17', y: 1.7
                                                    }, {
                                                    x: 'Mar-17', y: 1.5
                                                    }, {
                                                    x: 'Apr-17', y: 1.3
                                                    }, {
                                                    x: 'May-17', y: 1.2
                                                    }, {
                                                    x: 'Jun-17', y: 1
                                                    }, {
                                                    x: 'Jul-17', y: 1
                                                    }, {
                                                    x: 'Aug-17', y: 0.9
                                                    }, {
                                                    x: 'Sep-17', y: 0.7
                                                    }, {
                                                    x: 'Oct-17', y: 0.6
                                                    }, {
                                                    x: 'Nov-17', y: 0.5
                                                    }, {
                                                    x: 'Dec-17', y: 0.4
                                                    }]
                                
                                                    const version5 = [{
                                                    x: 'Jan-17', y: 25.2
                                                    }, {
                                                    x: 'Feb-17', y: 22.6
                                                    }, {
                                                    x: 'Mar-17', y: 20.8
                                                    }, {
                                                    x: 'Apr-17', y: 18.8
                                                    }, {
                                                    x: 'May-17', y: 17.1
                                                    }, {
                                                    x: 'Jun-17', y: 15.1
                                                    }, {
                                                    x: 'Jul-17', y: 14.5
                                                    }, {
                                                    x: 'Aug-17', y: 13.4
                                                    }, {
                                                    x: 'Sep-17', y: 12
                                                    }, {
                                                    x: 'Oct-17', y: 10.3
                                                    }, {
                                                    x: 'Nov-17', y: 8.6
                                                    }, {
                                                    x: 'Dec-17', y: 7.6
                                                    }]
                                
                                                    const version6 = [{
                                                    x: 'Jan-17', y: 11.3
                                                    }, {
                                                    x: 'Feb-17', y: 10.1
                                                    }, {
                                                    x: 'Mar-17', y: 9.4
                                                    }, {
                                                    x: 'Apr-17', y: 8.7
                                                    }, {
                                                    x: 'May-17', y: 7.8
                                                    }, {
                                                    x: 'Jun-17', y: 7.1
                                                    }, {
                                                    x: 'Jul-17', y: 6.7
                                                    }, {
                                                    x: 'Aug-17', y: 6.1
                                                    }, {
                                                    x: 'Sep-17', y: 5.4
                                                    }, {
                                                    x: 'Oct-17', y: 4.8
                                                    }, {
                                                    x: 'Nov-17', y: 3.8
                                                    }, {
                                                    x: 'Dec-17', y: 3.5
                                                    }]
                                
                                                    const version7 = [{
                                                    x: 'Jan-17', y: 22.8
                                                    }, {
                                                    x: 'Feb-17', y: 23.3
                                                    }, {
                                                    x: 'Mar-17', y: 23.1
                                                    }, {
                                                    x: 'Apr-17', y: 23.3
                                                    }, {
                                                    x: 'May-17', y: 23.3
                                                    }, {
                                                    x: 'Jun-17', y: 21.7
                                                    }, {
                                                    x: 'Jul-17', y: 21
                                                    }, {
                                                    x: 'Aug-17', y: 20.2
                                                    }, {
                                                    x: 'Sep-17', y: 19.2
                                                    }, {
                                                    x: 'Oct-17', y: 17.6
                                                    }, {
                                                    x: 'Nov-17', y: 15.4
                                                    }, {
                                                    x: 'Dec-17', y: 14.4
                                                    }]
                                
                                                    const version8 = [{
                                                    x: 'Jan-17', y: 24
                                                    }, {
                                                    x: 'Feb-17', y: 29.6
                                                    }, {
                                                    x: 'Mar-17', y: 31.3
                                                    }, {
                                                    x: 'Apr-17', y: 31.2
                                                    }, {
                                                    x: 'May-17', y: 31.8
                                                    }, {
                                                    x: 'Jun-17', y: 32.2
                                                    }, {
                                                    x: 'Jul-17', y: 32
                                                    }, {
                                                    x: 'Aug-17', y: 29.7
                                                    }, {
                                                    x: 'Sep-17', y: 28.1
                                                    }, {
                                                    x: 'Oct-17', y: 25.5
                                                    }, {
                                                    x: 'Nov-17', y: 22.7
                                                    }, {
                                                    x: 'Dec-17', y: 21.3
                                                    }]
                                
                                                    const version9 = [{
                                                    x: 'Jan-17', y: 0.3
                                                    }, {
                                                    x: 'Feb-17', y: 0.5
                                                    }, {
                                                    x: 'Mar-17', y: 2.4
                                                    }, {
                                                    x: 'Apr-17', y: 6.6
                                                    }, {
                                                    x: 'May-17', y: 10.6
                                                    }, {
                                                    x: 'Jun-17', y: 14.2
                                                    }, {
                                                    x: 'Jul-17', y: 15.8
                                                    }, {
                                                    x: 'Aug-17', y: 19.3
                                                    }, {
                                                    x: 'Sep-17', y: 22.3
                                                    }, {
                                                    x: 'Oct-17', y: 22.9
                                                    }, {
                                                    x: 'Nov-17', y: 20.3
                                                    }, {
                                                    x: 'Dec-17', y: 18.1
                                                    }]
                                
                                                    const version10 = [{
                                                    x: 'Jan-17', y: 0
                                                    }, {
                                                    x: 'Feb-17', y: 0.2
                                                    }, {
                                                    x: 'Mar-17', y: 0.4
                                                    }, {
                                                    x: 'Apr-17', y: 0.5
                                                    }, {
                                                    x: 'May-17', y: 0.9
                                                    }, {
                                                    x: 'Jun-17', y: 1.6
                                                    }, {
                                                    x: 'Jul-17', y: 2
                                                    }, {
                                                    x: 'Aug-17', y: 4
                                                    }, {
                                                    x: 'Sep-17', y: 6.2
                                                    }, {
                                                    x: 'Oct-17', y: 8.2
                                                    }, {
                                                    x: 'Nov-17', y: 10.5
                                                    }, {
                                                    x: 'Dec-17', y: 10.1
                                                    }]
                                
                                                    const version11 = [{
                                                    x: 'Jan-17', y: 0
                                                    }, {
                                                    x: 'Feb-17', y: 0
                                                    }, {
                                                    x: 'Mar-17', y: 0
                                                    }, {
                                                    x: 'Apr-17', y: 0
                                                    }, {
                                                    x: 'May-17', y: 0
                                                    }, {
                                                    x: 'Jun-17', y: 0
                                                    }, {
                                                    x: 'Jul-17', y: 0.2
                                                    }, {
                                                    x: 'Aug-17', y: 0.5
                                                    }, {
                                                    x: 'Sep-17', y: 0.8
                                                    }, {
                                                    x: 'Oct-17', y: 4.9
                                                    }, {
                                                    x: 'Nov-17', y: 11.4
                                                    }, {
                                                    x: 'Dec-17', y: 14
                                                    }]
                                
                                                    const version12 = [{
                                                    x: 'Jan-17', y: 0
                                                    }, {
                                                    x: 'Feb-17', y: 0
                                                    }, {
                                                    x: 'Mar-17', y: 0
                                                    }, {
                                                    x: 'Apr-17', y: 0
                                                    }, {
                                                    x: 'May-17', y: 0
                                                    }, {
                                                    x: 'Jun-17', y: 0
                                                    }, {
                                                    x: 'Jul-17', y: 0
                                                    }, {
                                                    x: 'Aug-17', y: 0
                                                    }, {
                                                    x: 'Sep-17', y: 0.3
                                                    }, {
                                                    x: 'Oct-17', y: 0.8
                                                    }, {
                                                    x: 'Nov-17', y: 3.2
                                                    }, {
                                                    x: 'Dec-17', y: 7.5
                                                    }]
                                
                                                    const axisX = xyChart.getDefaultAxisX()
                                                    .setMouseInteractions(false)
                                                    .setScrollStrategy(undefined)
                                                    // Disable default ticks.
                                                    .setTickStrategy(AxisTickStrategies.Empty)
                                
                                                    // Create Custom Axis
                                                    const margin = 2;
                                                    const customAxisX = (data, index) => {
                                                    axisX.addCustomTick()
                                                        .setValue(index)
                                                        .setGridStrokeLength(0)
                                                        .setTextFormatter((_) => data[index].x)
                                                        .setMarker((marker) => marker
                                                            .setPadding(margin)
                                                            .setBackground((background) => background
                                                                .setStrokeStyle(emptyLine)
                                                                .setFillStyle(emptyFill)
                                                            )
                                                            .setTextFillStyle(new SolidFill({ color: ColorRGBA(150, 150, 150) }))
                                                        )
                                
                                                    }
                                
                                                    // Generate Chart
                                                    const generateChart = (data, area, createTicks) => {
                                                    data
                                                        .map((p, index) => ({ x: index, y: p.y }))
                                                        .forEach((point, index) => {
                                                            area.add(point)
                                                            if(createTicks){
                                                                customAxisX(data, index)
                                                            }
                                                        })
                                                    }
                                
                                                    // generating different layered for charts
                                                    generateChart(version1, V1Area, true)
                                                    generateChart(version2, V2Area)
                                                    generateChart(version3, V3Area)
                                                    generateChart(version4, V4Area)
                                                    generateChart(version5, V5Area)
                                                    generateChart(version6, V6Area)
                                                    generateChart(version7, V7Area)
                                                    generateChart(version8, V8Area)
                                                    generateChart(version9, V9Area)
                                                    generateChart(version10, V10Area)
                                                    generateChart(version11, V11Area)
                                                    generateChart(version12, V12Area)
                                
                                                    // Enable AutoCursor auto-fill.
                                                    xyChart.setAutoCursor((cursor) => cursor
                                                    .setResultTableAutoTextStyle(false)
                                                    .disposeTickMarkerX()
                                                    )
                                
                                                    xyChart.getDefaultAxisY()
                                                    .setTitle('Percentage')
                                                    .setScrollStrategy(undefined)
                                                    .setInterval(0, 50)
                                                    .setMouseInteractions(false)"
                                                ),
                                                ),
                  
  
    'Heatmaps' => array(
              'Example_1' => array(
              'chart_name' => 'Heatmap Grid',
              'icon' => plugins_url().'/LC-JS/images/icons/heatmapGrid.png',
              'html_code' => '<div id="target" class="row content"></div>',
              'javascript_code' => "// Extract required parts from LightningChartJS.
                                                    const {
                                                        lightningChart,
                                                        PalettedFill,
                                                        LUT,
                                                        ColorRGBA,
                                                        Themes
                                                    } = lcjs
                                                    
                                                    
                                                    function WaterDropGenerator(
                                                        sizeX,
                                                        sizeZ,
                                                        xPositionsNormalized,
                                                        zPositionsNormalized,
                                                        amplitudes,
                                                        offsetLevel,
                                                        volatility
                                                    ) {
                                                    
                                                        function CalculateWavesAtPoint(
                                                            x,
                                                            z
                                                        ) {
                                                            let resultValue = 0
                                                            const iOscillatorCount = oscillators.length
                                                            for (let i = 0; i < iOscillatorCount; i++) {
                                                                const oscillator = oscillators[i]
                                                                const distX = x - oscillator.centerX
                                                                const distZ = z - oscillator.centerZ
                                                                const dist = Math.sqrt(distX * distX + distZ * distZ)
                                                                resultValue += oscillator.gain * oscillator.amplitude * Math.cos(dist * volatility) * Math.exp(-dist * 3.0)
                                                            }
                                                            return resultValue
                                                        }
                                                    
                                                        const iOscCount = amplitudes.length
                                                        const oscillators = []
                                                    
                                                        for (let iOsc = 0; iOsc < iOscCount; iOsc++) {
                                                            oscillators[iOsc] = {
                                                                amplitude: amplitudes[iOsc],
                                                                centerX: xPositionsNormalized[iOsc],
                                                                centerZ: zPositionsNormalized[iOsc],
                                                                gain: 1,
                                                                offsetY: 0
                                                            }
                                                        }
                                                    
                                                        const result = Array.from(Array(sizeZ)).map(() => Array(sizeX))
                                                        const dTotalX = 1
                                                        const dTotalZ = 1
                                                        const stepX = (dTotalX / sizeX)
                                                        const stepZ = (dTotalZ / sizeZ)
                                                    
                                                        // calculate the data
                                                        for (let row = 0, z = 0; row < sizeZ; row++, z += stepZ) {
                                                            for (let col = 0, x = 0; col < sizeX; col++, x += stepX) {
                                                                result[col][row] = CalculateWavesAtPoint(x, z) + offsetLevel
                                                            }
                                                        }
                                                        return result
                                                    }
                                                    
                                                    // Create a XY Chart.
                                                    const chart = lightningChart(license_key).ChartXY({
                                                        // theme: Themes.dark
                                                        container: 'target'
                                                        
                                                    })
                                                        .setTitle('Heatmap using IntensityGrid')
                                                    
                                                    // Specify the resolution used for the heatmap.
                                                    const sqRes = 200
                                                    const resolutionX = sqRes
                                                    const resolutionY = sqRes
                                                    
                                                    // Generate data to use for heatmap
                                                    const data = WaterDropGenerator(
                                                        resolutionX,     // size of nodes in X
                                                        resolutionY,     // size of nodes in Z
                                                        [0.2, 0.5, 0.7], // Drop X positions in scale 0...1
                                                        [0.6, 0.5, 0.3], // Drop Z positions in scale 0...1
                                                        [15, 50, 3],     // Amplitudes, as Y axis values
                                                        47,              // Offset level (mid-Y)
                                                        25               // Volatility, wave generating density
                                                    )
                                                    // Create LUT and FillStyle
                                                    const palette = new LUT({
                                                        steps: [
                                                            { value: 0, color: ColorRGBA(0, 0, 0) },
                                                            { value: 30, color: ColorRGBA(255, 255, 0) },
                                                            { value: 45, color: ColorRGBA(255, 204, 0) },
                                                            { value: 60, color: ColorRGBA(255, 128, 0) },
                                                            { value: 100, color: ColorRGBA(255, 0, 0) }
                                                        ],
                                                        interpolate: false
                                                    })
                                                    
                                                    // Add a Heatmap to the Chart. By default IntensityGrid Series Type is used.
                                                    const heatmap = chart.addHeatmapSeries({
                                                        rows: resolutionX,
                                                        columns: resolutionX,
                                                        start: { x: 10, y: 10 },
                                                        end: { x: 90, y: 90 },
                                                        pixelate: false
                                                    })
                                                        // Add data and invalidate the Series based on added data.
                                                        .invalidateValuesOnly(data)
                                                        // Use created Paletted FillStyle for the Heatmap.
                                                        .setFillStyle(new PalettedFill({ lut: palette }))
                                                    "
                                                    ),
                        'Example_2' => array(
                                'chart_name' => 'Heatmap Spectrogram',
                                'icon' => plugins_url().'/LC-JS/images/icons/heatmapSpectrogram.png',
                                'html_code' => '<div id="target" class="row content"></div>',
                                'javascript_code' => "const {
                                    lightningChart,
                                    IntensitySeriesTypes,
                                    PalettedFill,
                                    LUT,
                                    emptyFill,
                                    emptyLine,
                                    AxisScrollStrategies,
                                    AxisTickStrategies,
                                    ColorHSV,
                                    Themes
                                } = lcjs
                                
                                const AudioContext = window.AudioContext || window.webkitAudioContext
                                // Create a new audio context,
                                // for most part this context is not used for other than creating audiobuffer from audio data
                                const audioCtx = new AudioContext()
                                
                                // General configuration for common settings
                                const config = {
                                    /**
                                     * The resolution of the FFT calculations
                                     * Higher value means higher resolution decibel domain..
                                     */
                                    fftResolution: 4096,
                                    /**
                                     * Smoothing value for FFT calculations
                                     */
                                    smoothingTimeConstant: 0.1,
                                    /**
                                     * The size of processing buffer,
                                     * determines how often FFT is run
                                     */
                                    processorBufferSize: 2048
                                }
                                
                                // Initialize LightningChart JS
                                const lc = lightningChart(license_key)
                                
                                /**
                                 * Fetch audio file and create audio buffer from it.
                                 * @param   {string}         waveformUrl    URL to the WaveForm to load
                                 * @returns {AudioBuffer}                   The audio file as an AudioBuffer
                                 */
                                const loadWaveForm = async (waveformUrl) => {
                                    // Fetch waveform
                                    const resp = await fetch(waveformUrl)
                                    // Convert fetch to array buffer
                                    const waveDataBuffer = await resp.arrayBuffer()
                                    // Convert array buffer to audio buffer
                                    const audioBuffer = await audioCtx.decodeAudioData(waveDataBuffer)
                                    return audioBuffer
                                }
                                
                                /**
                                 * @typedef WaveFormData
                                 * @type {object}
                                 * @property {Uint8Array[]} channels    FFT Data for each channel
                                 * @property {number}       stride      Number of data points in a data block
                                 * @property {number}       rowCount    Number of rows of data
                                 * @property {number}       maxFreq     Maximum frequency of the data
                                 * @property {number}       duration    Audio buffer duration in seconds
                                 */
                                
                                /**
                                 * Process a AudioBuffer and create FFT Data for Spectrogram from it.
                                 * @param   {AudioBuffer}     audioBuffer   AudioBuffer to process into FFT data.
                                 * @returns {WaveFormData}                  Processed data
                                 */
                                const processWaveForm = async (audioBuffer) => {
                                    // Create a new OfflineAudioContext with information from the pre-created audioBuffer
                                    // The OfflineAudioContext can be used to process a audio file as fast as possible.
                                    // Normal AudioContext would process the file at the speed of playback.
                                    const offlineCtx = new OfflineAudioContext(audioBuffer.numberOfChannels, audioBuffer.length, audioBuffer.sampleRate)
                                    // Create a new source, in this case we have a AudioBuffer to create it for, so we create a buffer source
                                    const source = offlineCtx.createBufferSource()
                                    // Set the buffer to the audio buffer we are using
                                    source.buffer = audioBuffer
                                    // Set source channel count to the audio buffer channel count, if this wasn't set, the source would default to 2 channels.
                                    source.channelCount = audioBuffer.numberOfChannels
                                
                                    // We want to create spectrogram for each channel in the buffer, so we need to separate the channels to separate outputs.
                                    const splitter = offlineCtx.createChannelSplitter(source.channelCount)
                                    // Create a analyzer node for the full context
                                    const generalAnalyzer = offlineCtx.createAnalyser()
                                    generalAnalyzer.fftSize = config.fftResolution
                                    generalAnalyzer.smoothingTimeConstant = config.smoothingTimeConstant
                                
                                    // Prepare buffers and analyzers for each channel
                                    const channelFFtDataBuffers = []
                                    const analyzers = []
                                    for (let i = 0; i < source.channelCount; i += 1) {
                                        channelFFtDataBuffers[i] = new Uint8Array((audioBuffer.length / config.processorBufferSize) * (config.fftResolution / 2))
                                        // Setup analyzer for this channel
                                        analyzers[i] = offlineCtx.createAnalyser()
                                        analyzers[i].smoothingTimeConstant = config.smoothingTimeConstant
                                        analyzers[i].fftSize = config.fftResolution
                                        // Connect the created analyzer to a single channel from the splitter
                                        splitter.connect(analyzers[i], i)
                                    }
                                    // Script processor is used to process all of the audio data in fftSize sized blocks
                                    // Script processor is a deprecated API but the replacement APIs have really poor browser support
                                    offlineCtx.createScriptProcessor = offlineCtx.createScriptProcessor || offlineCtx.createJavaScriptNode
                                    const processor = offlineCtx.createScriptProcessor(config.processorBufferSize, 1, 1)
                                    let offset = 0
                                    processor.onaudioprocess = (ev) => {
                                        // Run FFT for each channel
                                        for (let i = 0; i < source.channelCount; i += 1) {
                                            const freqData = new Uint8Array(channelFFtDataBuffers[i].buffer, offset, analyzers[i].frequencyBinCount)
                                            analyzers[i].getByteFrequencyData(freqData)
                                        }
                                        offset += generalAnalyzer.frequencyBinCount
                                    }
                                    // Connect source buffer to correct nodes,
                                    // source feeds to:
                                    // splitter, to separate the channels
                                    // processor, to do the actual processing
                                    // generalAanalyzer, to get collective information
                                    source.connect(splitter)
                                    source.connect(processor)
                                    processor.connect(offlineCtx.destination)
                                    source.connect(generalAnalyzer)
                                    // Start the source, other wise start rendering would not process the source
                                    source.start(0)
                                
                                    // Process the audio buffer
                                    await offlineCtx.startRendering()
                                    return {
                                        channels: channelFFtDataBuffers,
                                        stride: config.fftResolution / 2,
                                        tickCount: Math.ceil(audioBuffer.length / config.processorBufferSize),
                                        maxFreq: offlineCtx.sampleRate / 2, // max freq is always half the sample rate
                                        duration: audioBuffer.duration
                                    }
                                }
                                
                                /**
                                 * Create data matrix for heatmap from one dimensional array
                                 * @param {Uint8Array}  data        FFT Data
                                 * @param {number}      strideSize  Single data block width
                                 * @param {number}      tickCount    Data row count
                                 */
                                const remapDataToTwoDimensionalMatrix = (data, strideSize, tickCount) => {
                                    /**
                                     * @type {Array<number>}
                                     */
                                    const arr = Array.from(data)
                                
                                    // Map the one dimensional data to two dimensional data where data goes from right to left
                                    // [1, 2, 3, 4, 5, 6]
                                    // -> strideSize = 2
                                    // -> rowCount = 3
                                    // maps to
                                    // [1, 4]
                                    // [2, 5]
                                    // [3, 6]
                                    const output = Array.from(Array(strideSize)).map(() => Array.from(Array(tickCount)))
                                    for (let row = 0; row < strideSize; row += 1) {
                                        for (let col = 0; col <= tickCount; col += 1) {
                                            output[row][col] = arr[col * strideSize + row]
                                        }
                                    }
                                
                                    return output
                                }
                                
                                /**
                                 * Create a chart for a channel
                                 * @param {lcjs.Dashboard}  dashboard       Dashboard to create the chart in
                                 * @param {number}          channelIndex    Current channel index
                                 * @param {number}          rows            Data row count
                                 * @param {number}          columns         Data column count
                                 * @param {number}          maxFreq         Maximum frequency for data
                                 * @param {number}          duration        Duration in seconds
                                 */
                                const createChannel = (dashboard, channelIndex, rows, columns, maxFreq, duration) => {
                                    // Create a new chart in a specified row
                                    const chart = dashboard.createChartXY({
                                        columnIndex: 0,
                                        columnSpan: 1,
                                        rowIndex: channelIndex,
                                        rowSpan: 1
                                    })
                                        // Hide the chart title
                                        .setTitleFillStyle(emptyFill)
                                
                                    // Start position of the heatmap
                                    const start = {
                                        x: 0,
                                        y: 0
                                    }
                                    // End position of the heatmap
                                    const end = {
                                        x: duration,
                                        // Use half of the fft data range
                                        y: (Math.ceil(maxFreq / 2))
                                    }
                                    // Create the series
                                    const series = chart.addHeatmapSeries({
                                        // Data columns, defines horizontal resolution
                                        columns: columns,
                                        // Data rows, defines vertical resolution
                                        // Use half of the fft data range
                                        rows: Math.ceil(rows / 2),
                                        // Start position, defines where one of the corners for hetmap is
                                        start,
                                        // End position, defines the opposite corner of the start corner
                                        end,
                                        // Smoothly render the heatmap data
                                        pixelate: true,
                                        container: 'target',
                                        // Using IntensityGrid, it supports rectangular heatmaps and is simpler than the IntensityMesh type
                                        type: IntensitySeriesTypes.Grid
                                    })
                                        // Use palletted fill style, intensity values define the color for each data point based on the LUT
                                        .setFillStyle(new PalettedFill({
                                            lut: new LUT({
                                                steps: [
                                                    { value: 0, color: ColorHSV(0, 1, 0) },
                                                    { value: 255 * (1 / 6), color: ColorHSV(270, 0.84, 0.2) },
                                                    { value: 255 * (2 / 6), color: ColorHSV(289, 0.86, 0.35) },
                                                    { value: 255 * (3 / 6), color: ColorHSV(324, 0.97, 0.56) },
                                                    { value: 255 * (4 / 6), color: ColorHSV(1, 1, 1) },
                                                    { value: 255 * (5 / 6), color: ColorHSV(44, 0.64, 1) }
                                                ],
                                                interpolate: true
                                            })
                                        }))
                                
                                    // Set default X axis settings
                                    series.axisX.setInterval(start.x, end.x)
                                        .setTickStrategy(AxisTickStrategies.Empty)
                                        .setTitleMargin(0)
                                        .setScrollStrategy(undefined)
                                        .setMouseInteractions(false)
                                    // Set default chart settings
                                    chart.setPadding({ left: 0, top: 8, right: 8, bottom: 1 })
                                        .setMouseInteractions(false)
                                    // Set default X axis settings
                                    series.axisY.setInterval(start.y, end.y)
                                        .setTitle(`Channel {channelIndex + 1} (Hz)`)
                                        .setScrollStrategy(AxisScrollStrategies.fitting)
                                
                                    return {
                                        chart,
                                        series
                                    }
                                }
                                
                                /**
                                 * Render a spectrogram for given data set
                                 * @param {WaveFormData} data Data set to render
                                 */
                                const renderSpectrogram = async (data) => {
                                    // Create a dashboard with enough rows for the number of channels in data set
                                    const dashboard = lc.Dashboard({
                                        // theme: Themes.dark 
                                        numberOfColumns: 1,
                                        container: 'target',
                                        numberOfRows: data.channels.length
                                    })
                                        // Hide the dashboard splitter
                                        .setSplitterStyle(emptyLine)
                                
                                    // Collection of created charts
                                    const charts = []
                                
                                    // Create channels and set data for each channel
                                    for (let i = 0; i < data.channels.length; i += 1) {
                                        // Create a chart for the channel
                                        const ch = createChannel(dashboard, i, data.stride, data.tickCount, data.maxFreq, data.duration)
                                        // Setup the data for the chart
                                        const remappedData = remapDataToTwoDimensionalMatrix(data.channels[i], data.stride, data.tickCount)
                                        // Set the heatmap data
                                        ch.series.invalidateValuesOnly(remappedData)
                                        // Add the created chart and series to collection
                                        charts.push(ch)
                                    }
                                
                                    // Style to bottom most chart axis to use it as the common axis for each chart
                                    charts[charts.length - 1]
                                        .series
                                        .axisX
                                        .setTickStrategy(AxisTickStrategies.Numeric)
                                        .setScrollStrategy(AxisScrollStrategies.fitting)
                                        .setTitle(`Duration (s)`)
                                        .setMouseInteractions(true)
                                
                                    // Link chart X axis scales
                                    charts[charts.length - 1].series.axisX.onScaleChange((start, end) => {
                                        charts.forEach((c, i) => i < charts.length - 1 ? c.series.axisX.setInterval(start, end, false, false) : undefined)
                                    })
                                
                                    return dashboard
                                }
                                
                                (async () => {
                                    // Remove loading spinner
                                    document.querySelectorAll('.loading').forEach(item => {
                                        item.parentElement.removeChild(item)
                                    })
                                    const run = async () => {
                                        // Load waveform from url
                                        const waveform = await loadWaveForm('".plugins_url()."/LC-JS/images/icons/Truck_driving_by-Jason_Baker-2112866529_edit.wav')
                                        // Process the loaded wave form to prepare it for being added to the chart
                                        const processed = await processWaveForm(waveform)
                                        // Create a dashboard from the processed waveform data
                                        const dashboard = renderSpectrogram(processed)
                                    }
                                    // Check if audio context was started
                                    if (audioCtx.state === 'suspended') {
                                        // Show a large play button
                                        const resumeElement = document.createElement('div')
                                        resumeElement.style.position = 'absolute'
                                        resumeElement.style.top = '0'
                                        resumeElement.style.left = '0'
                                        resumeElement.style.right = '0'
                                        resumeElement.style.bottom = '0'
                                
                                        const resumeImg = document.createElement('img')
                                        resumeImg.src =  '".plugins_url()."/LC-JS/images/icons/play_circle_outline-24px.svg'
                                        resumeImg.style.width = '100%'
                                        resumeImg.style.height = '100%'
                                
                                        resumeElement.onclick = () => {
                                            audioCtx.resume()
                                        }
                                        resumeElement.appendChild(resumeImg)
                                
                                        const innerElement = document.querySelector('.inner')
                                        let target
                                        if (!innerElement) {
                                            target = document.createElement('div')
                                            target.classList.add('inner')
                                            document.body.appendChild(target)
                                        }
                                        const targetElement = innerElement || target
                                        targetElement.appendChild(resumeElement)
                                
                                        // Attach a listener to the audio context to remove the play button as soon as the context is running
                                        audioCtx.onstatechange = () => {
                                            if (audioCtx.state === 'running') {
                                                run()
                                                audioCtx.onstatechange = void 0
                                                targetElement.removeChild(resumeElement)
                                            }
                                        }
                                    } else {
                                        // Audio context is running so run the example
                                        run()
                                    }
                                })()
                                
                                "
                                                    ),
                        'Example_3' => array(
                            'chart_name' => 'Scrolling Heatmap',
                            'icon' => plugins_url().'/LC-JS/images/icons/scrollingHeatmap.png',
                            'html_code' => '<div id="target" class="row content"></div>',
                            'javascript_code' => "const {
                                lightningChart,
                                PalettedFill,
                                LUT,
                                ColorHEX,
                                UIElementBuilders,
                                UIOrigins,
                                UIDraggingModes,
                                Themes
                            } = lcjs
                            
                            const {
                                createProgressiveFunctionGenerator
                            } = xydata
                            
                            /**
                             * Create data matrix for heatmap from one dimensional array
                             * @param {Uint8Array}  data        FFT Data
                             * @param {number}      strideSize  Single data block width
                             * @param {number}      tickCount    Data row count
                             */
                            const remapDataToTwoDimensionalMatrix = (data, strideSize, tickCount) => {
                                /**
                                 * @type {Array<number>}
                                 */
                                const arr = Array.from(data)
                            
                                // Map the one dimensional data to two dimensional data where data goes from right to left
                                // [1, 2, 3, 4, 5, 6]
                                // -> strideSize = 2
                                // -> rowCount = 3
                                // maps to
                                // [1, 4]
                                // [2, 5]
                                // [3, 6]
                                const output = Array.from(Array(strideSize)).map(() => Array.from(Array(tickCount)))
                                for (let row = 0; row < strideSize; row += 1) {
                                    for (let col = 0; col <= tickCount; col += 1) {
                                        output[row][col] = arr[col * strideSize + row]
                                    }
                                }
                            
                                return output
                            }
                            
                            // Dimensions for the Heatmap. Also used to generate correct size array.
                            const resolution = 100
                            const historyLen = 500
                            
                            // Create colorpalette for the LUT. The colors should interpolate between values.
                            const lut = new LUT({
                                steps: [
                                    { value: 0, color: ColorHEX('#1000') },
                                    { value: 40, color: ColorHEX('#1000') },
                                    { value: 50, color: ColorHEX('#f00') }
                                ],
                                interpolate: true
                            })
                            
                            const paletteFill = new PalettedFill({ lut })
                            
                            // Create intensity grid
                            const chartXY = lightningChart(license_key).ChartXY({
                                // theme: Themes.dark
                                container: 'target',
                            })
                            const intensityOptions = {
                                rows: resolution,
                                columns: historyLen,
                                start: { x: 0, y: 0 },
                                end: { x: 100, y: 50 },
                                pixelate: false
                            }
                            const grid = chartXY.addHeatmapSeries(intensityOptions)
                                .setFillStyle(paletteFill)
                            
                            // Index for sweeping mode.
                            let ind = 0
                            
                            // Add a button to the top left of the chart to toggle between
                            // sweeping update and scrolling update for the intensity grid.
                            const toggleButton = chartXY.addUIElement(UIElementBuilders.CheckBox)
                                .setText('Toggle sweeping on / off')
                                .setOn(false)
                                .setPosition({ x: 5, y: 99 })
                                .setOrigin(UIOrigins.LeftTop)
                                .setDraggingMode(UIDraggingModes.notDraggable)
                            
                            toggleButton
                                .onSwitch(() => {
                                    ind = 0
                                    grid.reset(intensityOptions)
                                })
                            
                            // Update the heatmap by sweeping the columns
                            const sweepColumns = (arr, ind) => {
                                const remappedData = remapDataToTwoDimensionalMatrix(arr, resolution, 1)
                                grid.invalidateValuesOnly(remappedData, { column: { start: ind, end: ind + 1 }, row: { start: 0, end: resolution - 1 } })
                            }
                            
                            createProgressiveFunctionGenerator()
                                .setSamplingFunction((x) => ((Math.sin(x)) * resolution) / 2)
                                .setStep(0.01)
                                .setStart(0)
                                .setEnd(Math.PI * 2)
                                .generate()
                                .setStreamRepeat(true)
                                .setStreamInterval(1000 / 60)
                                .setStreamBatchSize(1)
                                .toStream()
                                .forEach((data) => {
                                    let values = []
                                    const y = data.y
                                    for (let i = 0; i < resolution; i++) {
                                        values[i] = Math.min(i - y, resolution - (i - y))
                                    }
                                    // State for the sweeping toggle.
                                    if (!toggleButton.getOn()) {
                                        grid.addColumn(1, 'value', [values])
                                    } else {
                                        // Sweeping mode. Add data to heatmap by invalidating values
                                        // with given data.
                                        sweepColumns(values, ind)
                                        ind += 1
                                        if (ind >= historyLen - 1)
                                            ind = 0
                                    }
                                })"
                                                ),
                                                    ),
                                        
                       '3D_Charts' => array(
                               'Example_1' => array(
                                    'chart_name' => '3D Scatter Chart',
                                    'icon' => plugins_url().'/LC-JS/images/icons/3dScatter.png',
                                    'html_code' => '<div id="target" class="row content"></div>',
                                    'javascript_code' =>"const {
                                        lightningChart,
                                        SolidFill,
                                        ColorRGBA,
                                        UIElementBuilders,
                                        UILayoutBuilders,
                                        Themes
                                    } = lcjs
                                    
                                     
                                    
                                    // Extract required parts from xyData.
                                    const {
                                        createProgressiveRandomGenerator
                                    } =xydata
                                    
                                     
                                    
                                    // Define colors
                                    const red = new SolidFill({ color: ColorRGBA(255, 100, 100) })
                                    const blue = new SolidFill({ color: ColorRGBA(100, 100, 255) })
                                    
                                     
                                    
                                    // Initiate chart
                                    const chart3D = lightningChart(license_key).Chart3D({
                                        // theme: Themes.dark
                                        container: 'target'
                                    })
                                        .setTitle('3D Scatter Chart')
                                    
                                     
                                    
                                    // Set Axis titles
                                    chart3D.getDefaultAxisX().setTitle('Axis X')
                                    chart3D.getDefaultAxisY().setTitle('Axis Y')
                                    chart3D.getDefaultAxisZ().setTitle('Axis Z')
                                    
                                     
                                    
                                    // Create two new Point Series
                                    const blueSeries = chart3D.addPointSeries({ pointShape: 'sphere' })
                                        .setPointStyle((pointStyle) => pointStyle
                                            // Change the point fillStyle.
                                            .setFillStyle(blue)
                                            // Change point size.
                                            .setSize(30))
                                    const redSeries = chart3D.addPointSeries({ pointShape: 'sphere' })
                                        .setPointStyle((pointStyle) => pointStyle
                                            .setFillStyle(red)
                                            .setSize(30))
                                    
                                     
                                    
                                    // Add layout UI Element for checkboxes.
                                    const layout = chart3D.addUIElement(UILayoutBuilders.Column)
                                        .setPosition({ x: 90, y: 90 })
                                        .setOrigin({ x: 1, y: 1 })
                                    
                                     
                                    
                                    // Flag for camera rotation
                                    let rotateCamera = false
                                    // Add button for toggling camera rotation into the layout UI Element
                                    const rotateCameraButton = layout.addElement(UIElementBuilders.CheckBox)
                                        .setText('Rotate camera')
                                    rotateCameraButton.onSwitch((_, state) => {
                                        rotateCamera = state
                                    })
                                    rotateCameraButton.setOn(rotateCamera)
                                    
                                     
                                    
                                    // Method to handle animating camera rotation.
                                    let cameraAngle = 0
                                    const dist = 1
                                    const animateCameraRotation = () => {
                                        if (rotateCamera) {
                                            chart3D.setCameraLocation(
                                                {
                                                    x: Math.cos(cameraAngle) * dist,
                                                    y: 0.50,
                                                    z: Math.sin(cameraAngle) * dist
                                                }
                                            )
                                            cameraAngle += 0.005
                                        }
                                        requestAnimationFrame(animateCameraRotation)
                                    }
                                    animateCameraRotation()
                                    
                                     
                                    
                                    // Generate points for the red series.
                                    createProgressiveRandomGenerator()
                                        .setNumberOfPoints(30)
                                        .generate()
                                        .toPromise()
                                        .then((d) => d.map((p) => ({
                                            x: p.x - 1,
                                            y: p.y * 1,
                                            z: Math.random()
                                        })))
                                        .then((d) => {
                                            setInterval(() => {
                                                redSeries.add(d.splice(0, 10))
                                    
                                     
                                    
                                            })
                                        })
                                    
                                     
                                    
                                    // Generate points for the blue series.
                                    createProgressiveRandomGenerator()
                                        .setNumberOfPoints(30)
                                        .generate()
                                        .toPromise()
                                        .then((d) => d.map((p) => ({
                                            x: p.x - 1,
                                            y: p.y * 1,
                                            z: Math.random()
                                        })))
                                        .then((d) => {
                                            setInterval(() => {
                                                blueSeries.add(d.splice(0, 10))
                                    
                                     
                                    
                                            })
                                        })"
                                                ),
                                                'Example_2' => array(
                                                    'chart_name' => '3D Line Series',
                                                    'icon' => plugins_url().'/LC-JS/images/icons/3dLine.png',
                                                    'html_code' => '<div id="target" class="row content"></div>',
                                                    'javascript_code' =>" const {
                                                        lightningChart,
                                                        SolidFill,
                                                        ColorRGBA,
                                                        UIElementBuilders,
                                                        UILayoutBuilders,
                                                        Themes
                                                    } = lcjs
                                                    
                                                    // Define colors used for the Series
                                                    const red = new SolidFill({ color: ColorRGBA(255, 100, 100) });
                                                    const blue = new SolidFill({ color: ColorRGBA(100, 100, 255) });
                                                    const green = new SolidFill({ color: ColorRGBA(100, 255, 100) });
                                                    
                                                    // Initiate chart
                                                    const chart3D = lightningChart(license_key).Chart3D({
                                                    container: 'target'
                                                    })
                                                        .setTitle('3D Line Series')
                                                        .setBoundingBox({ x: 1.0, y: 1.0, z: 2.0 })
                                                    
                                                    // Set Axis titles
                                                    chart3D.getDefaultAxisX().setTitle('Axis X')
                                                    chart3D.getDefaultAxisY().setTitle('Axis Y')
                                                    chart3D.getDefaultAxisZ().setTitle('Axis Z')
                                                    
                                                    // Add a layout UI element for checkboxes
                                                    const layout = chart3D.addUIElement(UILayoutBuilders.Column)
                                                        .setPosition({ x: 90, y: 90 })
                                                        .setOrigin({ x: 1, y: 1 })
                                                    
                                                    // Flag for camera rotation
                                                    let rotateCamera = false
                                                    // Add button for toggling camera rotation into the layout UI Element
                                                    const rotateCameraButton = layout.addElement(UIElementBuilders.CheckBox)
                                                        .setText('Rotate camera')
                                                    rotateCameraButton.onSwitch((_, state) => {
                                                        rotateCamera = state
                                                    })
                                                    rotateCameraButton.setOn(rotateCamera)
                                                    
                                                    // Method to handle animating camera rotation.
                                                    let cameraAngle = 0
                                                    const dist = 1
                                                    const animateCameraRotation = () => {
                                                        if (rotateCamera) {
                                                            chart3D.setCameraLocation(
                                                                {
                                                                    x: Math.cos(cameraAngle) * dist,
                                                                    y: 0.50,
                                                                    z: Math.sin(cameraAngle) * dist
                                                                }
                                                            )
                                                            cameraAngle += 0.005
                                                        }
                                                        requestAnimationFrame(animateCameraRotation)
                                                    }
                                                    animateCameraRotation()
                                                    
                                                    // Add new series and style them
                                                    const blueSeries = chart3D.addLineSeries()
                                                        .setLineStyle((lineStyle) => lineStyle.setFillStyle(blue).setThickness(30))
                                                    const redSeries = chart3D.addLineSeries()
                                                        .setLineStyle((lineStyle) => lineStyle.setFillStyle(red).setThickness(30))
                                                    const greenSeries = chart3D.addLineSeries()
                                                        .setLineStyle((lineStyle) => lineStyle.setFillStyle(green).setThickness(100))
                                                    
                                                    // Create data for the blue and red Series rotating around the green Series.
                                                    let z = 0
                                                    let z2 = 0
                                                    let ang = -5
                                                    
                                                    for (let i = 0; i < 100; i++) {
                                                        cameraAngle -= 5
                                                        ang += 5
                                                        z -= 0.5
                                                        z2 += 0.5
                                                        blueSeries.add({ x: Math.sin(cameraAngle * Math.PI / 180), y: Math.cos(cameraAngle * Math.PI / 180), z })
                                                        redSeries.add({ x: Math.sin(ang * Math.PI / 180), y: Math.cos(ang * Math.PI / 180), z: z2 })
                                                    }
                                                    // Add data for the green Series.
                                                    greenSeries.add({ x: 0, y: 0, z: -51 })
                                                    greenSeries.add({ x: 0, y: 0, z: 51 })"),
                                'Example_3' => array(
                                    'chart_name' => '3D 50k Points',
                                    'icon' => plugins_url().'/LC-JS/images/icons/3d50kPoints.png',
                                    'html_code' => '<div id="target" class="row content"></div>',
                                    'javascript_code' =>"const {
                                        lightningChart,
                                        UIElementBuilders,
                                        UILayoutBuilders,
                                        AxisTickStrategies,
                                        Themes
                                    } = lcjs
                                    
                                    // Initiate chart
                                    const chart3D = lightningChart(license_key).Chart3D({
                                        // theme: Themes.dark
                                        container: 'target',
                                    })
                                    
                                    // Get axes for later use
                                    const axes = { x: chart3D.getDefaultAxisX(), y: chart3D.getDefaultAxisY(), z: chart3D.getDefaultAxisZ() }
                                    // Create new point series
                                    const cubeSeries = chart3D.addPointSeries({ pointShape: 'cube' })
                                    const sphereSeries = chart3D.addPointSeries({ pointShape: 'sphere' })
                                    
                                    // Layout for UI controls
                                    const layout = chart3D.addUIElement(UILayoutBuilders.Column)
                                        .setPosition({ x: 100, y: 100 })
                                        .setOrigin({ x: 1, y: 1 })
                                    
                                    // Scale changing
                                    const changeScale = layout.addElement(UIElementBuilders.CheckBox)
                                        .setText('Change scale')
                                    let normal
                                    changeScale.onSwitch((_, state) => {
                                        if (state) {
                                            normal = { min: axes.y.scale.getInnerStart(), max: axes.y.scale.getInnerEnd() }
                                            axes.y.setInterval(-5, 15, 2000, state)
                                        } else
                                            axes.y.setInterval(normal.min, normal.max, 2000, state)
                                    })
                                    
                                    // Shape changing
                                    const changeShape = layout.addElement(UIElementBuilders.CheckBox)
                                        .setText('Change point shape')
                                        .setOn(true)
                                    
                                    // Set the button behavior.
                                    changeShape
                                        .onSwitch((_, state) => {
                                            if (state) {
                                                cubeSeries.restore()
                                                sphereSeries.dispose()
                                            } else {
                                                cubeSeries.dispose()
                                                sphereSeries.restore()
                                            }
                                        })
                                    
                                    // Camera rotating
                                    let rotateCamera = false
                                    const rotateCameraButton = layout.addElement(UIElementBuilders.CheckBox)
                                        .setText('Rotate camera')
                                    rotateCameraButton.onSwitch((_, state) => {
                                        rotateCamera = state
                                    })
                                    rotateCameraButton.setOn(rotateCamera)
                                    
                                    let ang = 0
                                    const dist = 1.5
                                    const animateCameraRotation = () => {
                                        if (rotateCamera) {
                                            chart3D.setCameraLocation(
                                                {
                                                    x: Math.cos(ang) * dist,
                                                    y: 0.50,
                                                    z: Math.sin(ang) * dist
                                                }
                                            )
                                    
                                            ang += 0.005
                                        }
                                    
                                        requestAnimationFrame(animateCameraRotation)
                                    }
                                    animateCameraRotation()
                                    
                                    // Axis ticks switching
                                    const ticksButton = layout.addElement(UIElementBuilders.CheckBox)
                                        .setText('Axis ticks enabled')
                                    ticksButton.onSwitch((_, state) => {
                                        axes.x.setTickStrategy(state ? AxisTickStrategies.Numeric : AxisTickStrategies.Empty)
                                        axes.y.setTickStrategy(state ? AxisTickStrategies.Numeric : AxisTickStrategies.Empty)
                                        axes.z.setTickStrategy(state ? AxisTickStrategies.Numeric : AxisTickStrategies.Empty)
                                    })
                                    ticksButton.setOn(false)
                                    
                                    // Set titles for each Axis.
                                    axes.x.setTitle('Axis X')
                                    axes.y.setTitle('Axis Y')
                                    axes.z.setTitle('Axis Z')
                                    
                                    // Generate test data.
                                    const generateSeries = () => {
                                        const points = []
                                        const pointsMax = 50000
                                        let angleRadians = 0
                                        const centerX = 0.5
                                        const centerZ = -0.5
                                        const radius = 40
                                        const stepRadians = Math.PI / 1000
                                        const a = 20
                                        const b = 0.025
                                        const c = 5.0
                                        const pScale = 10
                                        for (let j = 0; j < pointsMax; j++) {
                                            const randomRadius = radius + (Math.random() - 1.0) * radius
                                            const x = centerX + randomRadius * Math.cos(angleRadians) + a * (Math.random() - 0.5)
                                            const y = randomRadius * randomRadius * b + a * Math.sin(angleRadians * c) + (Math.random() - 0.5) * a + a
                                            const z = centerZ + randomRadius * Math.sin(angleRadians) + a * (Math.random() - 0.5)
                                            const p = { x: 0 + x / pScale, y: 0 + y / pScale - 0.5, z: 0 + z / pScale }
                                            angleRadians += stepRadians
                                            points.push(p)
                                        }
                                        cubeSeries.add(points)
                                        sphereSeries.add(points)
                                        // Hide the sphere series when done adding points to it.
                                        sphereSeries.dispose()
                                    }
                                    
                                    generateSeries()
                                    "),
                                )
                                        
    );
    
    $chart_types = array_merge(
      $additional,
      array(
        'dashboard' => array(
          'name'    => esc_html__( 'Dashboard', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/path 265.svg' ,
          'description' => plugins_url().'/LC-JS/images/icons/Union 4.svg',
        ),
        'bar_chart'         => array(
          'name'    => esc_html__( 'Bar Chart', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/Group 5.svg',
          'description' =>  plugins_url().'/LC-JS/images/icons/Union 4.svg',
        ),
        'radial_charts'      => array(
          'name'    => esc_html__( 'Radial Charts', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/path 279.svg',
          'description' =>  plugins_url().'/LC-JS/images/icons/Union 4.svg',
        ),
        'line_series'        => array(
          'name'    => esc_html__( 'Line Series', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/path 271.svg',
          'description' =>  plugins_url().'/LC-JS/images/icons/Union 4.svg',
        ),
        'trading' => array(
          'name'    => esc_html__( 'Trading', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/Group 2.svg',
          'description' =>  plugins_url().'/LC-JS/images/icons/Union 4.svg',
        ),
        'statistics'         => array(
          'name'    => esc_html__( 'Statistics', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/Group 3.svg',
          'description' =>  plugins_url().'/LC-JS/images/icons/Union 4.svg',
        ),
        'area_series'        => array(
          'name'    => esc_html__( 'Area Series', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/path 273.svg',
          'description' =>  plugins_url().'/LC-JS/images/icons/Union 4.svg',
        ),
        'Heatmaps'         => array(
          'name'    => esc_html__( 'Heatmaps', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/Group 4.svg',
          'description' =>  plugins_url().'/LC-JS/images/icons/Union 4.svg',
        ),
        '3D_Charts'         => array(
          'name'    => esc_html__( '3D Charts', 'visualizer' ),
          'enabled' => true,
          'icon' => plugins_url().'/LC-JS/images/icons/path 264.svg',
          'description' =>  plugins_url().'/LC-JS/images/icons/Union 4.svg',
        )
      )
    );
  }
  add_action( 'init', 'lcjs_global_vars' );
  



add_shortcode( 'Lcjs_charts' , 'lcjs_chart_creation' );
function lcjs_chart_creation( $atts ) {
  
  extract( shortcode_atts( array(
    'id' => ''
  ), $atts ) );
 
   if ( !$chart = get_post( $id ) )
    return '';
  // get meta
  $html       =  get_post_meta( $id, 'html_code', true );
  $javascript =  get_post_meta( $id, 'javascript_code', true ) ;
  $height1    =  get_post_meta( $id, 'height', true );
  $width1     =  get_post_meta( $id, 'width', true ) ;
  
  $resources =plugins_url().'/LC-JS/includes/lcjs.iife.js';

  //$javascript = str_replace("container: 'target'","container: 'target_$id'",$javascript);
 
  lc_charts_enqueue_javascript($javascript);
 
  //$html  = str_replace('$CHARTID',$id,$html);
  echo $html;
  echo '<style>
  #target{
    width:  ' . $width1 . 'px;
    height: ' . $height1 . 'px;
  }
  #chart-1
  {
    width:  ' . $width1 . 'px;
    height: ' . $height1 . 'px;
  }
  </style>';

}
add_action( 'wp_ajax_delete_list', 'lcjs_do_delete_list' );

function lcjs_do_delete_list() {
	global $wpdb; // this is how you get access to the database
  if(!empty($_POST['delete_id'])){
  $delete_id =  sanitize_text_field($_POST['delete_id']) ;
  if($delete_id)
  {
    $res = array();
     $result = wp_delete_post($delete_id);
     if($result)
     {
      $res['delete'] = '1';
     }else
     {
      $res['delete'] = '0';
     }
  }
  echo  json_encode($res);
}
	wp_die(); // this is required to terminate immediately and return a proper response
}



add_action( 'wp_ajax_edit_get_data', 'lcjs_do_edit_get_data' );

function lcjs_do_edit_get_data() {
	global $wpdb; // this is how you get access to the database
  global $wp_query;
  if(!empty($_POST['edit_id'])){
   $edit_id =  sanitize_text_field($_POST['edit_id']) ;
   $postid = $edit_id ;
   $post_title =  get_the_title( $postid );
   $html_code =  get_post_meta($postid, 'html_code', true);
   $javascript_code =  get_post_meta($postid, 'javascript_code', true);
   $resources =  get_post_meta($postid, 'resources', true);
   $height =  get_post_meta($postid, 'height', true);
   $width =  get_post_meta($postid, 'width', true);
   $result = array();
   $result['post_title'] = $post_title;
   $result['html_code'] = $html_code;
   $result['javascript_code'] = $javascript_code;
   $result['resources'] = $resources;
   $result['height'] = $height;
   $result['width'] = $width;
   echo json_encode($result);
   wp_reset_query();
  }
    wp_die(); // this is required to terminate immediately and return a proper response
}



add_action( 'wp_ajax_get_example', 'lcjs_get_example' );
 
function lcjs_get_example() {
  global $wpdb; 
  $ddata_chart = $GLOBALS['abc'];
  $example_name =  $_POST['Example_name'] ;
  $example_array = explode('#',$example_name);
  if(count($example_array) > 0)
  {
      $result = array();
      $chart_type_name = $example_array[0];
      $example_title = $example_array[1];
      if(count($ddata_chart)>0)
      {
        $example_data = $ddata_chart[$chart_type_name][$example_title];
        if(count($example_data) > 0)
        {
           $html_code = $example_data['html_code'];
           $javascript_code = $example_data['javascript_code'];
           $result['example_title'] = $example_data['chart_name'];
           $result['html_code'] = $html_code ;
           $result['javascript_code'] = $javascript_code ;
        }
      }
   echo json_encode($result);
  }

  wp_die(); 
}

function lcjs_app_output_buffer() {
  ob_start();
} // soi_output_buffer
add_action('init', 'lcjs_app_output_buffer');

function lcjs_remove_footer_admin() {



if (is_admin() && isset($_GET['page']) && 'LC-JS/includes/mfp-chart-listing-page.php' == $_GET['page']
 || 'LC-JS/includes/License.php' == $_GET['page'] || 'LC-JS/includes/get_help.php' == $_GET['page']) {

        echo '
        <div class="row plugin-footer">
            <div class="col-md-10"></div>
            <div class="col-md-2 logo-footer"> 
            <a href="https://www.arction.com/" target="_blank"> 
                <img src="'.plugins_url().'/LC-JS/includes/images/Footer_1.png"> </div>
            </a>
            </div>
        </div>
            ';
    }
   
  }
  add_filter('admin_footer_text', 'lcjs_remove_footer_admin');



  function lcjs_selectively_enqueue_admin_script( $hook ) {
    wp_enqueue_script( 'bootsrap-js', plugin_dir_url( __FILE__ ).'js/' . 'bootstrap.min.js', array(), '3.4.1' );
    wp_register_style( 'LC-JS', plugins_url( 'LC-JS/includes/css/bootstrap.min.css' ) );
    wp_enqueue_style( 'LC-JS' );
    wp_register_style( 'LC-JS-1', plugins_url( 'LC-JS/includes/css/style.css' ) );
    wp_enqueue_style( 'LC-JS-1' );
    //wp_enqueue_style( 'bootsrap-css', plugin_dir_url( __FILE__ ).'css/','bootstrap.min.css', array(), '3.4.1' );
}
add_action( 'admin_enqueue_scripts', 'lcjs_selectively_enqueue_admin_script' );
