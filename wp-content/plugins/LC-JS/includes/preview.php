<?php 
global $wpdb; 
  if(isset($_POST['html_code'])){
    $html_code =$_POST['html_code'];
 }  
 if(isset($_POST['javascript_code'])){
    $javascript_code =$_POST['javascript_code'];
 }  
?>
    <style>
        html,
        body {
            height: 100%;
            margin: 0;
        }

        .box {
            display: flex;
            flex-flow: column;
            height: 100%;
        }

        .box .row.header {
            flex: 0 1 auto;
        }

        .box .row.content {
            flex: 1 1 auto;
        }
        
    </style>
	<body class="box">
		
        <!-- HTML -->
        
        <?php echo $html_code; ?>
        <script>
        
        function get_chart()
        { 
            <?php 
            echo $javascript_code;
            ?>
        }
        get_chart();
        </script>
	</body>
