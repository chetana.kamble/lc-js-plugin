<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
<style>
#TB_ajaxContent
{
 /* background-image: url('<?php echo plugins_url().'/LC-JS/images/download.jpg'; ?>'); */
 background: #202020 0% 0% no-repeat padding-box;
opacity: 1;
top: 32px;
left: 160px;
/*width: 2000px;
height: 1085px;*/
float:left;
}
.logo-box{

height: 238px;
background: transparent url('<?php echo plugins_url().'/LC-JS/includes/images/Mask_Group_2.png'; ?>') 0% 0% no-repeat padding-box;
}
.lazy-logo{
position:relative;
top: 60px;
left: 45px;
width: 408px;
height: 84px;
background: transparent url('<?php echo plugins_url().'/LC-JS/includes/images/lightningchart-js-logo.svg'; ?>') 0% 0% no-repeat padding-box;
opacity: 1;
}

#sea_butt{
  background-image: url('<?php echo plugins_url().'/LC-JS/images/icons/Union 23.svg'?>');
  background-position: left 5px bottom 10px;
  border-color: #363636;

}

#toggleAll{
  background-image: url('<?php echo plugins_url().'/LC-JS/images/icons/Union 22.svg';?>');
  background-position: right 5px bottom 10px;
}

</style>
<div class="lc-main-header">
  <div class="row">
    <div class="col-md-8 logo-box"> 
        <div class="lazy-logo"></div>
        <span class="tag-line-span">The highest-performance <span class="js-tag">JavaScript</span> charting library focusing on real-time data visualization</span>
    </div>
    <div class="col-md-4 flex-menu"> 
      <div class="menu-item">
        <a href="#TB_inline?&width=1200&height=900&inlineId=chart-library" class="thickbox">
          <div class="menu-icon">
            <img src="<?php echo plugins_url('images/chart-library-icon.svg', __FILE__ ) ?>"/> 
          </div>
          <p class="item-txt">Chart Library</p>
        </a>
      </div>
      <div class="menu-item"> 
        <a href="#TB_inline?&width=1200&height=900&inlineId=my-content-id" class="thickbox">
          <div class="menu-icon bg-yellow">
        
            <img src="<?php echo plugins_url('images/add-chart-icon.svg', __FILE__); ?>"/>
          </div>
          <p class="item-txt">Add new</p>
        </a>
      </div>
    </div>
  </div>






    <?php add_thickbox(); ?>
  <div id="my-content-id" style="display:none;">
  

 
    <form action="<?php echo admin_url(); ?>admin.php?page=LC-JS%2Fincludes%2Fmfp-chart-listing-page.php" method="post" onsubmit="return validate_me();" id="myForm">
      
    <div class="inner-page-header ">
      <div class="row">
        <div class="col-12 col-md-4 flex-section">
            <button id="cancel" class="thickbox">
              <div class="menu-icon">
                <img src="<?php echo plugins_url('images/back-icon.svg', __FILE__);?>"> 
              </div>         
            </button> 
            <div class="inner-title"> Code Editor</div>
        </div>

        <div class="col-12 col-md-8 flex-section controls top10">       
            <button type="button" class="prevbtn" onclick= "preview_value()" value="preview">Preview</button>
            
            <input type="submit" class="submitbtn" name="submit">
        </div>   
      </div>
    </div> 
    
    
   

      <div class="row">
        <div class="lc-code-editor col-md-6">           
                <div class="title flex-section">                   
                  <label for="Chart-label">Title</label>
                  <input type="text" id="Title_name" name="Title_name" autocomplete="off">
                  <input type="hidden" name="edit_id" id="edit_id" value="">
                </div>
          
                <div class="editor_section" id="section1">
                <div class="tab">
                <button class="tablinks" type="button" onclick="tabs(event, 'html')"><b>Html</b></button>
                <button class="tablinks" type="button" onclick="tabs(event, 'javascript')"><b>JavaScript</b></button>
                <button class="tablinks" type="button" onclick="tabs(event, 'setting')"><b>Settings</b></button>
                </div>

              <div id="html" class="tabcontent" value="">
               <textarea id="html_code" name="html" class="Textarea" onchange="textarea_change();"></textarea>
              </div>

              <div id="javascript" class="tabcontent">
                <textarea id="javascript_code" name="javascript" onchange="textarea_change();" class="Textarea"></textarea>
                <input type="hidden" id="save_status" name="save_status" value=""> 
              </div>

              <div id="setting" class="tabcontent Textarea">
                <label for="height" style="width:60px;color:black;">Height:</label><br>
                <input type="text" onkeypress="return isNumber(event)" id="height" name="height" > (Use only numberic value)<br><br>
                
                <label for="width" style="width:60px;color:black;">Width:</label><br>
                <input type="text" onkeypress="return isNumber(event)" id="width" name="width" > (Use only numberic value)<br>
              </div>
            </div> 
        </div>

        <div class="lc-chart-preview col-md-6"> 
          <div class="chart_section" id="demo"></div>
          <?php 
        $user = wp_get_current_user();
        $User_id = $user->ID;
        $prod_key1 =  get_user_meta( $User_id, 'prod_key' , true );
        if($prod_key1==null)
        {
          ?>
          <div class="watermark-alert">
            <p>To remove watermark in the chart, purchase license key referring to:</p>
            <a href="https://portal.arction.com/product/details?product_id=MTU=" target="_blank">Arction Portal</a>
          </div>
          <?php
        }
       ?>
        </div>
       
     
         
        
        <!-- footer -->  
        <div class="row plugin-footer">
          <div class="col-md-10"></div>
          <div class="col-md-2 logo-footer"> 
            <a href="https://www.arction.com/" target="_blank"> 
              <img src="<?php echo plugins_url(); ?>/LC-JS/includes/images/Footer_1.png"> </div>
            </a>
          </div>
        </div>

    </form>
  </div>


 <?php
    if (isset($_POST['submit'])){
      $html1 =  $_POST['html'];
      $javascript1 =  $_POST['javascript'];
      $resource1 = $_POST['resource'];
      $Title_name1 = $_POST['Title_name'];
      $height =  $_POST['height'];
      $width =  $_POST['width'];

      if($height != null && $width != null){
        $height1 =  $_POST['height'];
        $width1 =  $_POST['width'];
      }else{
        $height1 =  "800";
        $width1 =  "1000";
      }
      if(isset($_POST['edit_id']) && $_POST['edit_id'] != '')
      {
        $edit_id =  $_POST['edit_id'];
        
        $my_post = array(
          'ID'           => $edit_id,
          'post_title'   =>  $Title_name1,
          'post_type'=>'LC-JS-Charts', 
      );
      wp_update_post( $my_post );
      update_post_meta( $edit_id, 'html_code', $html1, false ); 
      update_post_meta( $edit_id, 'javascript_code', $javascript1, false );
      update_post_meta( $edit_id, 'height', $height1, false );
      update_post_meta( $edit_id, 'width', $width1, false );
         // Update the post into the database
        $referer = $_SERVER['REQUEST_URI'];
        
        header("Location: $referer"."&editmeassage=success");

      }else
      {
            $id = wp_insert_post(array(
              'post_title'=>$Title_name1, 
              'post_type'=>'LC-JS-Charts', 
              'post_status'   => 'publish',
            'post_author'   => 1,
          ));
            
      
         add_post_meta( $id, 'html_code', $html1, true ); 
         add_post_meta( $id, 'javascript_code', $javascript1, true );
        add_post_meta( $id, 'height', $height1, true );
        add_post_meta( $id, 'width', $width1, true );
         $referer = $_SERVER['REQUEST_URI'];
         header("Location: $referer"."&addmeassage=success");
      }
      }
 ?>


 <!--listing of user created charts starts here -->
 <div class="chart-list">
    <div class="row">
        <div class="col-md-12 table-box">
              <?php 
            $meassage = '';
            if(isset($_GET['addmeassage'])&& $_GET['addmeassage']=='success')
            {
              $meassage = "Chart is added successfully.";
            } elseif(isset($_GET['editmeassage'])&& $_GET['editmeassage']=='success')
            {
              $meassage = "Chart is modified successfully.";
            }elseif(isset($_GET['Delete'])&& $_GET['Delete']=='Success')
            {
              $meassage = "Chart is deleted successfully.";
            }elseif(isset($_GET['meassage'])&& $_GET['Delete']=='Error')
            {
              $meassage = "Something went wrong.try again later";
            }
            if($meassage != '')
            {
              ?>
              <div id="alert-box" class="alert  alert-box">
                <span><?php echo $meassage; ?> </span>
                </div>
            <?php } ?>
                  <table class="listing-table"  >
                  <thead>
                  <th width="100px">SR No </th>
                  <th>Title</th>
                  <th>Shortcode</th>
                  <th>Created Date</th>
                  <th>Action </th>
                  </thead>
                 
                  <?php 
            global $post,$wpdb,$wp_query;
            $paged = ($_GET['paged']) ? $_GET['paged'] : 1;
            $limit = 10;
              $args = array(
                'numberposts' => -1,
                'posts_per_page'=>10,
                'paged' => $paged,
                'post_type'   => 'LC-JS-Charts'
              );
            
            $latest = new WP_Query($args);
            $latest_charts = $latest->posts;
            $slug = plugin_basename( __FILE__ );
          
            if ($latest->post_count > 0) {
              ?>
               <tbody>
              <?php 
                $i = ($paged -1) * $limit + 1;
                foreach ($latest_charts as $charts) {
                    ?>
                  <tr>
                  <td width="100px"><?php echo $i; ?> </td>
                  <td><?php echo $charts->post_title; ?> </td>
                  <td><?php echo '[Lcjs_charts id='.$charts->ID.']' ?> </td>
                  <td><?php echo $charts->post_date; ?> </td>
                  <td> 
                    <a class="thickbox changeStatus" href="#TB_inline?&width=1200&height=900&inlineId=my-content-id" data-id="<?php echo $charts->ID; ?>">
                      <button class="yello-btn"><span class="">Edit</span></button>
                    </a>  
                    <a class="deletestatus"  data-id="<?php echo $charts->ID; ?>">
                      <button class="yello-btn"><span class="">Delete </span> </button> 
                    </a> 
                  </td>
                  </tr>
                  <?php
                  $i++;
                } ?>

            </tbody>

            
                </table>
                <?php
                $big = 999999999;
                $pages = paginate_links(array(
                      'base' => str_replace($big, '%#%', get_pagenum_link($big)),
                      'format' => '?paged=%#%',
                      'current' => max(1,  $_GET['paged']),
                      'total' => $latest->max_num_pages,
                      'prev_text' => '&laquo;',
                      'next_text' => '&raquo;',
                      'type'      => 'array'
                ));

                if ($pages) {
                  ?>
                <div class="row col-md-12 count-pages">
                    <ul class="chart-pagination">
                      <?php
                        foreach ($pages as $pgl) {
                            echo '<li class="page-item">'.$pgl.'</li>';
                        }
                    ?>
                    </ul>
                </div>
            <?php

                } ?>
      
          <?php
          }else
          {
            ?>
              <tbody>
               <tr><td colspan="5">No records found</td></tr>
              </tbody>
              </table>
            <?php

          }
          ?>
          </div>  
          </div> 
      </div> 
  <!--listing of user created charts ends here -->
    <div id="chart-library" style="display:none">
    <div class="inner-page-header">
      <div class="row">  
        <div class="col-lg-2 col-md-0"></div>
        <div class="col-lg-8 col-md-12">
          <div class="row">        
            <div class="col-12 col-md-6 flex-section">
                <button id="cancel" class="thickbox">
                  <div class="menu-icon">
                    <img src="<?php echo plugins_url();?>/LC-JS/includes/images/back-icon.svg"> 
                  </div>         
                </button> 
                <div class="inner-title"> Chart Library </div>
            </div>
            <div class="col-12 col-md-6 flex-section controls">
              <button  class="open_all toggle-all" >
                <div class="open_name" >Open all</div></button>
            </div> 
          </div>  
        </div>  
        <div class="col-lg-2 col-md-0"></div>
      </div> 
    </div>  
    <!--start -->
    <div class="col-lg-2 col-md-0"></div>
    <div class="panel-group col-lg-8 col-md-12 chart-lybrary-container" id="accordion" role="tablist" aria-multiselectable="true">
        <?php 
            $data_chart = $GLOBALS['chart_types'];
            $ddata_chart = $GLOBALS['abc'];
            if (count($data_chart) > 0) {
              $i=1;
                foreach ($data_chart as $key=>$value) { ?>
            <div class="">
              <!--<div class="" role="tab" id="collapse-heading-<?php echo $i; ?>">-->
              <button role="button" class="accordion collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-category-<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse-category-<?php echo $i; ?>">
                <div class="flex-section"> 
                  <div class="category-icon">
                    <img src="<?php echo $value['icon'];  ?>"> 
                  </div>
                  <?php echo $value['name']; ?> 
                  <p class="accord-p" >
                    <img src="<?php echo $value['description']; ?>">
                  </p>                 
                </div>    
              </button>
               
              <!--</div>-->
          <div class="panel-collapse collapse" id="collapse-category-<?php echo $i; ?>" role="tabpanel" aria-labelledby="collapse-heading-<?php echo $i; ?>">
              <div class="multiCollapseExample2 flex-chart-examples">
              <?php 
               if(count($ddata_chart) > 0)
               {
                 $text = $key;
               
                 $emp = $ddata_chart[$text];
                 if (is_array($emp)) {
                     if (count($emp) > 0) {
                         foreach ($emp as $key=>$val) {
                             ?>
                 
                      <div class="accordion_menu">

                      <a class="thickbox getexample"  data-example="<?php echo $text.'#'.$key; ?>">
                          <img src="<?php  echo $val['icon'] ; ?>">
                          <p><?php echo $val['chart_name'] ; ?></span>
                      </a>
                      </div>
                  
                         <?php
                         }
                     }
                 }
                     } ?>
                     </div>
          </div>
        </div>
          <?php
          $i++;
                }
              }
          ?>
    </div>
    <div class="col-lg-2 col-md-0"></div>
    
    <!-- footer -->  
    <div class="row plugin-footer">
      <div class="col-md-10"></div>
      <div class="col-md-2 logo-footer"> 
        <a href="https://www.arction.com/" target="_blank"> 
          <img src="<?php echo plugins_url(); ?>/LC-JS/includes/images/Footer_1.png"> </div>
        </a>
      </div>
    </div>

  
       <!--end -->
</div>
</div>
<div>
<!--html body end -->
<style>
.flip {
  transform: rotate(-180deg);
}
</style>
<script>

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


jQuery('.accordion').click(function() {
  jQuery(this).find('.accord-p img').css({'transform': ''});
  jQuery(this).find('.accord-p img').toggleClass('flip');
 
});
jQuery('.toggle-all').click(function(){
  
    if (jQuery(this).data("lastState") === null || jQuery(this).data("lastState") === 0) {
      jQuery('.collapse.in').collapse('hide');
      jQuery(this).data("lastState",1);
      jQuery('.open_name').text("Open all");
      jQuery(".accordion").addClass("collapsed");
      jQuery(".accord-p img").removeClass("flip");
    }
    else {
     
      jQuery('.panel-collapse').removeData('bs.collapse')
        .collapse({parent:false, toggle:false})
        .collapse('show')
        .removeData('bs.collapse')
        .collapse({parent:'#accordion', toggle:false});
        
        jQuery(this).data("lastState",0);
        jQuery('.open_name').text("Close all");
        jQuery(".accordion").removeClass("collapsed");
        jQuery(".accord-p img").addClass("flip");
    }

    });
		
    jQuery(document).ready(function() {
      jQuery('a.getexample').on('click', function(e) {
                var getExample = jQuery(this).data('example');
                jQuery("#TB_window").remove();
                jQuery("body").append("<div id='TB_window'></div>");
                var TB_WIDTH = (window.innerWidth-100);
                  TB_HEIGHT = (window.innerHeight-100);
                tb_show("", "#TB_inline?height="+TB_HEIGHT+"&width="+TB_WIDTH+"&inlineId=my-content-id");
                
                var data = {
                  'action': 'get_example',
                  'Example_name': getExample,
                  asynchronous: false,
                 
                };
                  jQuery.post(ajaxurl, data, function(response) {
                   
                    var obj = jQuery.parseJSON(response);
                    document.getElementsByClassName('tablinks')[0].click();
                    jQuery("#html_code").val(obj.html_code);
                    jQuery("#javascript_code").val(obj.javascript_code);
                    jQuery('#Title_name').val(obj.example_title);
                    preview_value();
                  });
            });
        });

        jQuery( 'body' ).on( 'thickbox:removed', function() {
          location.reload();
        });
  
        jQuery('button#cancel').on('click', function(e){
            e.preventDefault();
            //jQuery('#save_status').val('unsave');
            var status = jQuery('#save_status').val();
            if(status=='unsave')
            {
              var x = confirm("Changes that you made may not be saved");
              if(x)
              {
               
                location.reload();
              }else{
                
                e.preventDefault(); 
              }
            }else
            {
              location.reload();
            }
           
            
        });


        function preview_value() {
              var html_code = document.getElementById("myForm").elements.namedItem("html").value;
              var javascript_code = document.getElementById("myForm").elements.namedItem("javascript").value;
              document.getElementById("demo").innerHTML = html_code + javascript_code ;

              if(html_code=='' || javascript_code=='')
              {
                alert("Enter Html and javascript code");
              }else
              {
                var data = {
                        'html_code': html_code,
                        'javascript_code': javascript_code,
                      };
                jQuery.ajax({
                  url: "<?php echo plugins_url(); ?>/LC-JS/includes/preview.php",
                  data:data,
                  type : 'POST',
                  success: function(response){
                      jQuery("#demo").html('');
                      jQuery("#demo").html(response);
                  }
                });
            }

            }

            function tabs(evt, tab) {
              
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
              tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
              tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tab).style.display = "block";
            evt.currentTarget.className += " active";
          }
          function removeURLParameter(url, parameter) {
                  //prefer to use l.search if you have a location/link object
                  var urlparts= url.split('?');   
                  if (urlparts.length>=2) {

                      var prefix= encodeURIComponent(parameter)+'=';
                      var pars= urlparts[1].split(/[&;]/g);

                      //reverse iteration as may be destructive
                      for (var i= pars.length; i-- > 0;) {    
                          //idiom for string.startsWith
                          if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                              pars.splice(i, 1);
                          }
                      }

                      url= urlparts[0]+'?'+pars.join('&');
                      return url;
                  } else {
                      return url;
                  }
              }
          jQuery(document).ready(function() {
            jQuery('a.changeStatus').on('click', function(e) {
                var rowid = jQuery(this).data('id');
                jQuery('#edit_id').val(rowid);

                var data = {
                  'action': 'edit_get_data',
                  'edit_id': rowid
                };
                  jQuery.post(ajaxurl,data, function(response) {
                    var obj = jQuery.parseJSON(response);
                    jQuery("#html_code").val(obj.html_code);
                    jQuery("#javascript_code").val(obj.javascript_code);
                    jQuery('#resource_code').val(obj.resources);
                    jQuery('#height').val(obj.height);
                    jQuery('#width').val(obj.width);
                    jQuery('#Title_name').val(obj.post_title);
                    document.getElementsByClassName('tablinks')[0].click();
                    preview_value();
                  });

            });
            
          
             

            jQuery('a.deletestatus').on('click', function(e) {
              
                var x = confirm("Are you sure you want to delete?");
              if(x)
              {
                var rowid = jQuery(this).data('id');
                     
                  var data = {
                    'action': 'delete_list',
                    'delete_id': rowid,
                  };
                  jQuery.post(ajaxurl, data, function(response) {
                    var obj = jQuery.parseJSON(response);
                    key = 'addmeassage';
                    url = window.location.href;
                        
                    // Remove specific parameter from query string
                    filteredURL =removeURLParameter(url, 'addmeassage');
                     if(obj.delete==1)
                     {
                      window.location.href = filteredURL + "&Delete=Success";
                     }else
                     {
                      window.location.href = filteredURL + "&meassage=Error" ;
                     }
                  });

                }else
                {
                  return false;
                }
            });

          
        });

         function validate_me()
         {
           var str = '';
          
           html_code = jQuery('#html_code').val();
           javascript_code = jQuery('#javascript_code').val();
           resource_code = jQuery('#resource_code').val();
           if(html_code=='')
           {
             str += 'Please, enter html.\n';
           }
           if(javascript_code=='')
           {
             str += 'Please, enter javascript code.\n';
           }
            if(str == '')
            {
              return true;
            }else
            {
              alert(str);
              return false;
            }

         }
         document.getElementsByClassName('tablinks')[0].click();
        
        function textarea_change()
        {
          jQuery('#save_status').val('unsave');
        }  
        jQuery(document).ready(function() {
        setTimeout(function() {
          key = 'addmeassage';
          url = window.location.href;
          if (window.location.href.indexOf("editmeassage") > -1) {
             filteredURL =removeURLParameter(url, 'addmeassage');
              window.location.href = filteredURL;
          }
          if (window.location.href.indexOf("Delete") > -1) {
            filteredURL =removeURLParameter(url, 'Delete');
            window.location.href = filteredURL;
          }
          if (window.location.href.indexOf("editmeassage") > -1) {
            filteredURL =removeURLParameter(url, 'editmeassage');
            window.location.href = filteredURL;
          }    
        }, 2000);
        clearInterval() ;
      });

        
  </script>  