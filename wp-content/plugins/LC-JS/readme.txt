=== LC JS Charts ===

Contributors:      chetanakamble
Plugin Name:       LC JS Charts
Plugin URI:        https://wordpress.org/plugins/lC-jS-charts
Tags:              comments, spam
Author URI:        https://www.arction.com/
Author:            arction
Donate link:       https://www.arction.com/contact/
Requires at least: 4.7
Tested up to:      5.5.1
Requires PHP: 5.6
Stable tag:        4.3
Version:           1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

LCJS Charts is a lightningchart JS plugin that can easily be used to create, update, manage 
and embed interactive charts into your WordPress posts and pages.
== Description ==
LightningChart® JS’s exceptionally powerful rendering ensures smooth animations exceeding all 
industry standards in amount of data per chart. The graphing library provides intuitive touch 
screen interactivity with zooming, panning, moving data cursors and so on.

Dashboard control makes it easy and convenient to manage dozens of charts, 
legend boxes, buttons, check boxes and other UI elements. 
The dashboard is rendered resource‑efficiently in single GPU‑scene which 
also makes resizing columns and rows very fast.

You can also use a built-in PHP function to invoke the chart anywhere in your template:
`<?php code(do_shortcode('[Lcjs_charts id=$chart_id]')); ?>`


The plugin has also many helpful functions:
 * Default working example for reference in each chart type
 * Required libraries and available resources load automatically.
== Installation ==
1.Use WordPress Plugin page to search and install the LC JS Charts plugin.

2.If you choose to install in manually, make sure all the files from the downloaded archive 
are placed into your /wp-content/plugins/LC-JS/ directory.
== Upgrade Notice ==
= 1.0 =
Upgrade notices describe the reason a user should upgrade.
== Screenshots ==
1. screenshot-1
2. screenshot-2
== Changelog ==
= 1.0 =
* LightningChart JS bundle is distributed containing CommonJS and IIFE builds with types declaration file 
for convenient development experience for both JavaScript and TypeScript developers.
== Upgrade Notice ==
== Frequently Asked Questions ==
=1.What is LightningChart JS best at? =
LightningChart JS shines at building applications with the following properties:
Applications that deal with extensive datasets.
Applications that deal with high-intensity data streams.
Applications that deploy on multiple platforms.
=2.What LightningChart JS can do? For what it can be used for? =
LightningChart JS is developed to provide high-performance data-visualization solutions for websites and web applications.
LightningChart JS can draw:

Charts and Graphs (Line plot, Scatter plot, Area plot, etc.);
UI elements (Buttons, Checkboxes, Annotations, etc.);
Dashboard with multiple charts and UI inside one pane.
== Donations ==
`<?php code(); // goes in backticks ?>`