type StreamContinueHandler = () => boolean;
interface StreamOptions {
    interval: number;
    batchSize: number;
    repeat: boolean | number | StreamContinueHandler;
}
declare class Stream<T> {
    private readonly options;
    private streamActive;
    private outputStream?;
    private mapHandler?;
    private forEachHandler?;
    private streamHandler?;
    private data;
    private readonly interval;
    private readonly batchSize;
    private infiniteReset;
    private batchesLeft;
    private continueHandler?;
    constructor(options: StreamOptions, infiniteReset: (value: T) => T);
    private consume;
    private checkStreamContinue;
    private runStream;
    private activateStream;
    push(newData: T | T[]): void;
    map(handler: (value: T, index: number, array: T[]) => T): Stream<T>;
    private _map;
    forEach(handler: (value: T, index: number, array: T[]) => void): void;
    private _forEach;
}
interface Point {
    x: number;
    y: number;
}
type OHLCData = [number, number, number, number, number];
declare class DataHost<T> {
    private data;
    private derivativeDataHosts;
    protected frozenData?: T[];
    private promisesToResolve;
    private streamsToPush;
    private readonly infiniteResetHandler;
    private streamOptions;
    constructor(infiniteResetHandler: (dataToReset: T, data: T[]) => T, streamOptions: StreamOptions);
    toStream(): Stream<T>;
    toPromise(): Promise<T[]>;
    private infiniteReset;
    push(data: T[] | T): void;
    setData(newData: T[]): void;
    freeze(): void;
    getPointCount(): number;
    private handleDerivativeDataHosts;
    setStreamInterval(interval: number): DataHost<T>;
    setStreamBatchSize(batchSize: number): DataHost<T>;
    setStreamRepeat(repeat: boolean | number | StreamContinueHandler): DataHost<T>;
}
declare abstract class DataGenerator<T, K> {
    protected options: Readonly<K>;
    constructor(args: K);
    generate(): DataHost<T>;
    private generateChunks;
    protected abstract getPointCount(): number;
    protected abstract generateDataPoint(index: number): T;
    protected abstract infiniteReset(dataToReset: T, data: ReadonlyArray<T>): T;
}
interface ProgressiveRandomOptions {
    numberOfPoints: number;
    offsetStep: number;
    offsetDeltaMax: number;
    offsetDeltaMin: number;
    dataMax: number;
}
declare function createProgressiveRandomGenerator(): ProgressiveRandomGenerator;
declare class ProgressiveRandomGenerator extends DataGenerator<Point, ProgressiveRandomOptions> {
    constructor(args: ProgressiveRandomOptions);
    setNumberOfPoints(numberOfPoints: number): ProgressiveRandomGenerator;
    setOffsetStep(offsetStep: number): ProgressiveRandomGenerator;
    setOffsetDeltaMax(offsetDeltaMax: number): ProgressiveRandomGenerator;
    setOffsetDeltaMin(offsetDeltaMin: number): ProgressiveRandomGenerator;
    setDataMax(dataMax: number): ProgressiveRandomGenerator;
    protected getPointCount(): number;
    private offset;
    protected generateDataPoint(i: number): {
        x: number;
        y: number;
    };
    protected infiniteReset(dataToReset: Point, data: Point[]): Point;
}
interface ProgressiveTraceGeneratorOptions {
    numberOfPoints: number;
}
declare function createProgressiveTraceGenerator(): ProgressiveTraceGenerator;
declare class ProgressiveTraceGenerator extends DataGenerator<Point, ProgressiveTraceGeneratorOptions> {
    constructor(args: ProgressiveTraceGeneratorOptions);
    setNumberOfPoints(numberOfPoints: number): ProgressiveTraceGenerator;
    protected getPointCount(): number;
    private previousPoint;
    protected generateDataPoint(i: number): {
        x: number;
        y: number;
    };
    protected infiniteReset(dataToReset: Point, data: Point[]): Point;
}
interface ProgressiveFunctionOptions {
    samplingFunction: (x: number) => number;
    start: number;
    end: number;
    step: number;
}
declare function createProgressiveFunctionGenerator(): ProgressiveFunctionGenerator;
declare class ProgressiveFunctionGenerator extends DataGenerator<Point, ProgressiveFunctionOptions> {
    private x;
    private readonly numberOfPoints;
    constructor(args: ProgressiveFunctionOptions);
    setSamplingFunction(handler: (x: number) => number): ProgressiveFunctionGenerator;
    setStart(start: number): ProgressiveFunctionGenerator;
    setEnd(end: number): ProgressiveFunctionGenerator;
    setStep(step: number): ProgressiveFunctionGenerator;
    protected getPointCount(): number;
    protected generateDataPoint(): {
        x: number;
        y: number;
    };
    protected infiniteReset(dataToReset: Point, data: Point[]): Point;
}
interface TraceGeneratorOptions {
    numberOfPoints: number;
}
declare function createTraceGenerator(): TraceGenerator;
declare class TraceGenerator extends DataGenerator<Point, TraceGeneratorOptions> {
    constructor(args: TraceGeneratorOptions);
    setNumberOfPoints(numberOfPoints: number): TraceGenerator;
    protected getPointCount(): number;
    private previous;
    protected generateDataPoint(): {
        x: number;
        y: number;
    };
    protected infiniteReset(dataToReset: Point, data: Point[]): Point;
}
interface OHLCGeneratorOptions {
    numberOfPoints: number;
    startTimestamp: number;
    dataFreq: number;
    start: number;
    volatility: number;
}
declare function createOHLCGenerator(): OHLCGenerator;
declare class OHLCGenerator extends DataGenerator<OHLCData, OHLCGeneratorOptions> {
    constructor(args: OHLCGeneratorOptions);
    setNumberOfPoints(numberOfPoints: number): OHLCGenerator;
    setStartTimestamp(startTimestamp: number): OHLCGenerator;
    setDataFrequency(dataFreq: number): OHLCGenerator;
    setStart(start: number): OHLCGenerator;
    setVolatility(volatility: number): OHLCGenerator;
    protected getPointCount(): number;
    private prevPoint;
    protected generateDataPoint(i: number): OHLCData;
    protected infiniteReset(dataToReset: OHLCData, data: OHLCData[]): OHLCData;
}
interface DeltaFunctionOptions {
    numberOfPoints: number;
    minGap: number;
    maxGap: number;
    minAmplitude: number;
    maxAmplitude: number;
    probability: number;
}
declare function createDeltaFunctionGenerator(): DeltaFunctionGenerator;
declare class DeltaFunctionGenerator extends DataGenerator<Point, DeltaFunctionOptions> {
    constructor(args: DeltaFunctionOptions);
    setNumberOfPoints(numberOfPoints: number): DeltaFunctionGenerator;
    setMinGap(minGap: number): DeltaFunctionGenerator;
    setMaxGap(maxGap: number): DeltaFunctionGenerator;
    setMinAmplitude(minAmplitude: number): DeltaFunctionGenerator;
    setMaxAmplitude(maxAmplitude: number): DeltaFunctionGenerator;
    setProbability(probability: number): DeltaFunctionGenerator;
    protected getPointCount(): number;
    private lastSpike;
    protected generateDataPoint(i: number): {
        x: number;
        y: number;
    };
    protected infiniteReset(dataToReset: Point, data: Point[]): Point;
}
interface WhiteNoiseGeneratorOptions {
    numberOfPoints: number;
}
declare function createWhiteNoiseGenerator(): WhiteNoiseGenerator;
declare class WhiteNoiseGenerator extends DataGenerator<Point, WhiteNoiseGeneratorOptions> {
    constructor(args: WhiteNoiseGeneratorOptions);
    setNumberOfPoints(numberOfPoints: number): WhiteNoiseGenerator;
    protected getPointCount(): number;
    protected generateDataPoint(i: number): {
        x: number;
        y: number;
    };
    protected infiniteReset(dataToReset: Point, data: Point[]): Point;
}
interface SampledDataGeneratorOptions<T> {
    inputData: T[];
    samplingFrequency: number;
    step: number;
}
interface SampledPoint<T> {
    timestamp: number;
    data: T;
}
declare function createSampledDataGenerator<T>(): SampledDataGenerator<T>;
declare class SampledDataGenerator<T> extends DataGenerator<SampledPoint<T>, SampledDataGeneratorOptions<T>> {
    private interval;
    constructor(args: SampledDataGeneratorOptions<T>);
    setInputData(inputData: T[]): SampledDataGenerator<T>;
    setSamplingFrequency(samplingFrequency: number): SampledDataGenerator<T>;
    setStep(step: number): SampledDataGenerator<T>;
    protected getPointCount(): number;
    protected generateDataPoint(i: number): SampledPoint<T>;
    protected infiniteReset(dataToReset: SampledPoint<T>, data: SampledPoint<T>[]): SampledPoint<T>;
}
type ParametricFunction = (t: number) => number;
interface ParametricFunctionOptions {
    xFunction: ParametricFunction;
    yFunction: ParametricFunction;
    start: number;
    end: number;
    step: number;
}
declare function createParametricFunctionGenerator(): ParametricFunctionGenerator;
declare class ParametricFunctionGenerator extends DataGenerator<Point, ParametricFunctionOptions> {
    private t;
    private readonly numberOfPoints;
    constructor(args: ParametricFunctionOptions);
    setXFunction(handler: ParametricFunction): ParametricFunctionGenerator;
    setYFunction(handler: ParametricFunction): ParametricFunctionGenerator;
    setStart(start: number): ParametricFunctionGenerator;
    setEnd(end: number): ParametricFunctionGenerator;
    setStep(step: number): ParametricFunctionGenerator;
    protected getPointCount(): number;
    protected generateDataPoint(): {
        x: number;
        y: number;
    };
    protected infiniteReset(dataToReset: Point, data: Point[]): Point;
}
export { createProgressiveRandomGenerator, createProgressiveTraceGenerator, createProgressiveFunctionGenerator, createTraceGenerator, createOHLCGenerator, createDeltaFunctionGenerator, createWhiteNoiseGenerator, createSampledDataGenerator, createParametricFunctionGenerator, Stream, DataGenerator, DataHost };
