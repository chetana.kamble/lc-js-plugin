'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

var Stream = (function () {
    function Stream(options, infiniteReset) {
        this.options = options;
        this.streamActive = false;
        this.data = [];
        this.interval = this.options.interval || 1000;
        this.batchSize = this.options.batchSize || 1;
        this.batchesLeft = -2;
        this.runStream = this.runStream.bind(this);
        this.infiniteReset = infiniteReset;
        if (options.repeat !== undefined) {
            if (typeof options.repeat === 'boolean') {
                this.batchesLeft = options.repeat ? -1 : -2;
            }
            else if (typeof options.repeat === 'number') {
                this.batchesLeft = options.repeat + 1;
            }
            else if (typeof options.repeat === 'function') {
                this.continueHandler = options.repeat;
            }
        }
    }
    Stream.prototype.consume = function () {
        var _this = this;
        var cutCount = this.batchSize;
        if (this.data.length < this.batchSize) {
            cutCount = this.data.length;
        }
        var consumed = this.data.splice(0, cutCount);
        if ((this.batchesLeft > 0 || this.batchesLeft === -1) && consumed.length > 0) {
            this.data = this.data.concat(consumed.map(function (dataPoint) { return _this.infiniteReset(dataPoint); }));
            if (consumed.length < this.batchSize) {
                while (consumed.length < this.batchSize) {
                    var nextPoint = this.data.splice(0, 1)[0];
                    this.data = this.data.concat(this.infiniteReset(nextPoint));
                    consumed.push(nextPoint);
                }
            }
        }
        return consumed;
    };
    Stream.prototype.checkStreamContinue = function () {
        var continueStream = (this.batchesLeft > 0 ||
            this.batchesLeft === -1 ||
            (this.batchesLeft === -2 && this.data.length > 0))
            ? true : false;
        if (this.continueHandler) {
            continueStream = this.continueHandler() === true;
        }
        return continueStream;
    };
    Stream.prototype.runStream = function () {
        var continueStream = this.checkStreamContinue();
        if (this.data && this.data.length > 0 && continueStream) {
            if (this.streamHandler) {
                var curData = this.consume();
                this.streamHandler(curData);
            }
            setTimeout(this.runStream, this.interval);
        }
        else {
            this.streamActive = false;
        }
        if (this.batchesLeft > 0)
            this.batchesLeft--;
    };
    Stream.prototype.activateStream = function () {
        if (!this.streamActive) {
            this.streamActive = true;
            this.runStream();
        }
    };
    Stream.prototype.push = function (newData) {
        if (Array.isArray(newData)) {
            this.data = this.data.concat(newData);
        }
        else {
            this.data.push(newData);
        }
        this.activateStream();
    };
    Stream.prototype.map = function (handler) {
        this.outputStream = new Stream(__assign(__assign({}, this.options), { repeat: false }), this.infiniteReset);
        this.mapHandler = handler;
        this.streamHandler = this._map;
        this.activateStream();
        return this.outputStream;
    };
    Stream.prototype._map = function (data) {
        if (this.mapHandler && this.outputStream) {
            var mapped = data.map(this.mapHandler);
            this.outputStream.push(mapped);
        }
    };
    Stream.prototype.forEach = function (handler) {
        this.forEachHandler = handler;
        this.streamHandler = this._forEach;
        this.activateStream();
    };
    Stream.prototype._forEach = function (data) {
        if (this.forEachHandler)
            data.forEach(this.forEachHandler);
    };
    return Stream;
}());

var DataHost = (function () {
    function DataHost(infiniteResetHandler, streamOptions) {
        this.data = [];
        this.derivativeDataHosts = [];
        this.promisesToResolve = [];
        this.streamsToPush = [];
        this.infiniteReset = this.infiniteReset.bind(this);
        this.infiniteResetHandler = infiniteResetHandler;
        var opts = {
            interval: streamOptions.interval,
            batchSize: streamOptions.batchSize,
            repeat: streamOptions.repeat !== undefined ? streamOptions.repeat : false
        };
        this.streamOptions = Object.freeze(opts);
    }
    DataHost.prototype.toStream = function () {
        var stream = new Stream(this.streamOptions, this.infiniteReset);
        if (this.frozenData) {
            stream.push(this.frozenData);
        }
        else {
            this.streamsToPush.push(stream);
        }
        return stream;
    };
    DataHost.prototype.toPromise = function () {
        var _this = this;
        var pr;
        if (this.frozenData) {
            pr = Promise.resolve(this.frozenData);
        }
        else {
            pr = new Promise(function (resolve) { _this.promisesToResolve.push(resolve); });
        }
        return pr;
    };
    DataHost.prototype.infiniteReset = function (data) {
        return this.infiniteResetHandler(data, this.frozenData ? this.frozenData : []);
    };
    DataHost.prototype.push = function (data) {
        var e_1, _a;
        if (!this.frozenData) {
            if (Array.isArray(data)) {
                try {
                    for (var data_1 = __values(data), data_1_1 = data_1.next(); !data_1_1.done; data_1_1 = data_1.next()) {
                        var d = data_1_1.value;
                        this.data.push(d);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (data_1_1 && !data_1_1.done && (_a = data_1.return)) _a.call(data_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            else {
                this.data.push(data);
            }
        }
    };
    DataHost.prototype.setData = function (newData) {
        this.data = newData;
    };
    DataHost.prototype.freeze = function () {
        var _this = this;
        if (!this.frozenData) {
            this.frozenData = this.data;
            setTimeout(function () {
                _this.promisesToResolve.forEach(function (p) { return p(_this.frozenData); });
                _this.promisesToResolve = [];
            }, 0);
            setTimeout(function () {
                _this.streamsToPush.forEach(function (s) { return s.push(_this.frozenData || []); });
                _this.streamsToPush = [];
            }, 0);
            setTimeout(function () {
                _this.handleDerivativeDataHosts();
            }, 0);
            this.data = [];
        }
    };
    DataHost.prototype.getPointCount = function () {
        return this.frozenData ? this.frozenData.length : 0;
    };
    DataHost.prototype.handleDerivativeDataHosts = function () {
        var _this = this;
        if (this.frozenData && this.derivativeDataHosts.length > 0) {
            this.derivativeDataHosts.forEach(function (host) {
                if (_this.frozenData) {
                    host.setData(_this.frozenData);
                }
                host.freeze();
            });
            this.derivativeDataHosts = [];
        }
    };
    DataHost.prototype.setStreamInterval = function (interval) {
        var dataHost = new DataHost(this.infiniteResetHandler, __assign(__assign({}, this.streamOptions), { interval: interval }));
        this.derivativeDataHosts.push(dataHost);
        this.handleDerivativeDataHosts();
        return dataHost;
    };
    DataHost.prototype.setStreamBatchSize = function (batchSize) {
        var dataHost = new DataHost(this.infiniteResetHandler, __assign(__assign({}, this.streamOptions), { batchSize: batchSize }));
        this.derivativeDataHosts.push(dataHost);
        this.handleDerivativeDataHosts();
        return dataHost;
    };
    DataHost.prototype.setStreamRepeat = function (repeat) {
        var dataHost = new DataHost(this.infiniteResetHandler, __assign(__assign({}, this.streamOptions), { repeat: repeat }));
        this.derivativeDataHosts.push(dataHost);
        this.handleDerivativeDataHosts();
        return dataHost;
    };
    return DataHost;
}());

var DataGenerator = (function () {
    function DataGenerator(args) {
        this.options = args;
    }
    DataGenerator.prototype.generate = function () {
        var dataHost = new DataHost(this.infiniteReset, {
            interval: 500,
            batchSize: 10,
            repeat: false
        });
        var points = this.getPointCount();
        var nextChunk = this.generateChunks.bind(this, 0, points, dataHost);
        setTimeout(nextChunk, 0);
        return dataHost;
    };
    DataGenerator.prototype.generateChunks = function (baseIndex, total, dataHost) {
        var startTime = window.performance.now();
        var points = [];
        for (var i = 0; window.performance.now() - startTime < 15 && baseIndex < total; i++) {
            var point = this.generateDataPoint(baseIndex);
            baseIndex++;
            points.push(point);
        }
        dataHost.push(points);
        if (baseIndex < total) {
            var nextChunk = this.generateChunks.bind(this, baseIndex, total, dataHost);
            setTimeout(nextChunk, 0);
        }
        else {
            dataHost.freeze();
        }
    };
    return DataGenerator;
}());

function createProgressiveRandomGenerator() {
    return new ProgressiveRandomGenerator({
        numberOfPoints: 1000,
        offsetStep: 10,
        offsetDeltaMax: 0.3,
        offsetDeltaMin: 0.1,
        dataMax: 0.5
    });
}
var ProgressiveRandomGenerator = (function (_super) {
    __extends(ProgressiveRandomGenerator, _super);
    function ProgressiveRandomGenerator(args) {
        var _this = _super.call(this, args) || this;
        _this.offset = 0.5;
        var opts = {
            numberOfPoints: args.numberOfPoints,
            offsetStep: args.offsetStep === 0 ? 0 : args.offsetStep,
            offsetDeltaMax: Math.min(args.offsetDeltaMax, 1),
            offsetDeltaMin: Math.max(args.offsetDeltaMin === 0 ? 0 : args.offsetDeltaMin, 0),
            dataMax: Math.min(args.dataMax, 1)
        };
        _this.options = Object.freeze(opts);
        return _this;
    }
    ProgressiveRandomGenerator.prototype.setNumberOfPoints = function (numberOfPoints) {
        return new ProgressiveRandomGenerator(__assign(__assign({}, this.options), { numberOfPoints: numberOfPoints }));
    };
    ProgressiveRandomGenerator.prototype.setOffsetStep = function (offsetStep) {
        return new ProgressiveRandomGenerator(__assign(__assign({}, this.options), { offsetStep: offsetStep }));
    };
    ProgressiveRandomGenerator.prototype.setOffsetDeltaMax = function (offsetDeltaMax) {
        return new ProgressiveRandomGenerator(__assign(__assign({}, this.options), { offsetDeltaMax: offsetDeltaMax }));
    };
    ProgressiveRandomGenerator.prototype.setOffsetDeltaMin = function (offsetDeltaMin) {
        return new ProgressiveRandomGenerator(__assign(__assign({}, this.options), { offsetDeltaMin: offsetDeltaMin }));
    };
    ProgressiveRandomGenerator.prototype.setDataMax = function (dataMax) {
        return new ProgressiveRandomGenerator(__assign(__assign({}, this.options), { dataMax: dataMax }));
    };
    ProgressiveRandomGenerator.prototype.getPointCount = function () {
        return this.options.numberOfPoints;
    };
    ProgressiveRandomGenerator.prototype.generateDataPoint = function (i) {
        if (i % this.options.offsetStep === 0 || i === 0) {
            var newOffset = Math.random() * (this.options.offsetDeltaMax - this.options.offsetDeltaMin) + this.options.offsetDeltaMin;
            this.offset = Math.random() > 0.5 ? this.offset + newOffset : this.offset - newOffset;
        }
        if (this.offset + this.options.dataMax > 1) {
            this.offset = 1 - this.options.dataMax;
        }
        else if (this.offset < 0) {
            this.offset = 0;
        }
        return {
            x: i,
            y: this.offset + Math.random() * this.options.dataMax
        };
    };
    ProgressiveRandomGenerator.prototype.infiniteReset = function (dataToReset, data) {
        return { x: dataToReset.x + data.length, y: dataToReset.y };
    };
    return ProgressiveRandomGenerator;
}(DataGenerator));

function createProgressiveTraceGenerator() {
    return new ProgressiveTraceGenerator({
        numberOfPoints: 1000
    });
}
var ProgressiveTraceGenerator = (function (_super) {
    __extends(ProgressiveTraceGenerator, _super);
    function ProgressiveTraceGenerator(args) {
        var _this = _super.call(this, args) || this;
        _this.previousPoint = { x: 0, y: 0 };
        var opts = {
            numberOfPoints: args.numberOfPoints
        };
        _this.options = Object.freeze(opts);
        return _this;
    }
    ProgressiveTraceGenerator.prototype.setNumberOfPoints = function (numberOfPoints) {
        return new ProgressiveTraceGenerator(__assign(__assign({}, this.options), { numberOfPoints: numberOfPoints }));
    };
    ProgressiveTraceGenerator.prototype.getPointCount = function () {
        return this.options.numberOfPoints;
    };
    ProgressiveTraceGenerator.prototype.generateDataPoint = function (i) {
        var point = {
            x: i,
            y: this.previousPoint.y + (Math.random() - 0.5) * 2
        };
        this.previousPoint = point;
        return point;
    };
    ProgressiveTraceGenerator.prototype.infiniteReset = function (dataToReset, data) {
        return { x: dataToReset.x + data.length, y: dataToReset.y };
    };
    return ProgressiveTraceGenerator;
}(DataGenerator));

function createProgressiveFunctionGenerator() {
    return new ProgressiveFunctionGenerator({
        samplingFunction: function (x) { return x * x; },
        start: 0,
        end: 100,
        step: 1
    });
}
var ProgressiveFunctionGenerator = (function (_super) {
    __extends(ProgressiveFunctionGenerator, _super);
    function ProgressiveFunctionGenerator(args) {
        var _this = _super.call(this, args) || this;
        _this.x = _this.options.start;
        var opts = {
            samplingFunction: args.samplingFunction,
            start: args.start,
            end: args.end,
            step: args.step
        };
        _this.options = Object.freeze(opts);
        _this.numberOfPoints = Math.ceil(Math.abs(opts.end - opts.start) / opts.step);
        return _this;
    }
    ProgressiveFunctionGenerator.prototype.setSamplingFunction = function (handler) {
        return new ProgressiveFunctionGenerator(__assign(__assign({}, this.options), { samplingFunction: handler }));
    };
    ProgressiveFunctionGenerator.prototype.setStart = function (start) {
        return new ProgressiveFunctionGenerator(__assign(__assign({}, this.options), { start: start }));
    };
    ProgressiveFunctionGenerator.prototype.setEnd = function (end) {
        return new ProgressiveFunctionGenerator(__assign(__assign({}, this.options), { end: end }));
    };
    ProgressiveFunctionGenerator.prototype.setStep = function (step) {
        return new ProgressiveFunctionGenerator(__assign(__assign({}, this.options), { step: step }));
    };
    ProgressiveFunctionGenerator.prototype.getPointCount = function () {
        return this.numberOfPoints;
    };
    ProgressiveFunctionGenerator.prototype.generateDataPoint = function () {
        var point = {
            x: this.x,
            y: this.options.samplingFunction(this.x)
        };
        this.x = this.x + this.options.step;
        return point;
    };
    ProgressiveFunctionGenerator.prototype.infiniteReset = function (dataToReset, data) {
        return { x: dataToReset.x + data.length * (data[data.length - 1].x - data[data.length - 2].x), y: dataToReset.y };
    };
    return ProgressiveFunctionGenerator;
}(DataGenerator));

function createTraceGenerator() {
    return new TraceGenerator({
        numberOfPoints: 1000
    });
}
var TraceGenerator = (function (_super) {
    __extends(TraceGenerator, _super);
    function TraceGenerator(args) {
        var _this = _super.call(this, args) || this;
        _this.previous = { x: 0, y: 0 };
        var opts = {
            numberOfPoints: args.numberOfPoints
        };
        _this.options = Object.freeze(opts);
        return _this;
    }
    TraceGenerator.prototype.setNumberOfPoints = function (numberOfPoints) {
        return new TraceGenerator(__assign(__assign({}, this.options), { numberOfPoints: numberOfPoints }));
    };
    TraceGenerator.prototype.getPointCount = function () {
        return this.options.numberOfPoints;
    };
    TraceGenerator.prototype.generateDataPoint = function () {
        var point = {
            x: this.previous.x + (Math.random() - 0.5) * 2,
            y: this.previous.y + (Math.random() - 0.5) * 2
        };
        this.previous = point;
        return point;
    };
    TraceGenerator.prototype.infiniteReset = function (dataToReset, data) {
        return { x: dataToReset.x + data[data.length - 1].x, y: dataToReset.y + data[data.length - 1].y };
    };
    return TraceGenerator;
}(DataGenerator));

function createOHLCGenerator() {
    return new OHLCGenerator({
        numberOfPoints: 1000,
        startTimestamp: 0,
        dataFreq: 1,
        start: 100,
        volatility: 0.1
    });
}
var OHLCGenerator = (function (_super) {
    __extends(OHLCGenerator, _super);
    function OHLCGenerator(args) {
        var _this = _super.call(this, args) || this;
        _this.prevPoint = [_this.options.startTimestamp, _this.options.start, _this.options.start, _this.options.start, _this.options.start];
        return _this;
    }
    OHLCGenerator.prototype.setNumberOfPoints = function (numberOfPoints) {
        return new OHLCGenerator(__assign(__assign({}, this.options), { numberOfPoints: numberOfPoints }));
    };
    OHLCGenerator.prototype.setStartTimestamp = function (startTimestamp) {
        return new OHLCGenerator(__assign(__assign({}, this.options), { startTimestamp: startTimestamp }));
    };
    OHLCGenerator.prototype.setDataFrequency = function (dataFreq) {
        return new OHLCGenerator(__assign(__assign({}, this.options), { dataFreq: dataFreq }));
    };
    OHLCGenerator.prototype.setStart = function (start) {
        return new OHLCGenerator(__assign(__assign({}, this.options), { start: start }));
    };
    OHLCGenerator.prototype.setVolatility = function (volatility) {
        return new OHLCGenerator(__assign(__assign({}, this.options), { volatility: volatility }));
    };
    OHLCGenerator.prototype.getPointCount = function () {
        return this.options.numberOfPoints;
    };
    OHLCGenerator.prototype.generateDataPoint = function (i) {
        var _this = this;
        var dataPoint = [0, 0, 0, 0, 0];
        var timeStamp = (this.options.startTimestamp + this.options.dataFreq * i);
        var dir = Math.random() > 0.5 ? 1 : -1;
        var newPoints = Array.from(Array(4)).map(function (v) {
            var change = Math.random() * _this.options.volatility * dir;
            if (_this.prevPoint[4] + change < 0) {
                change = change * -1;
            }
            return _this.prevPoint[4] + change;
        }).sort(function (a, b) { return a - b; });
        if (dir === -1) {
            newPoints = [newPoints[0], newPoints[2], newPoints[1], newPoints[3]];
        }
        dataPoint = [timeStamp, newPoints[1], newPoints[3], newPoints[0], newPoints[2]];
        this.prevPoint = dataPoint;
        return dataPoint;
    };
    OHLCGenerator.prototype.infiniteReset = function (dataToReset, data) {
        var newPoint = [
            dataToReset[0] + data.length * (data[data.length - 1][0] - data[data.length - 2][0]),
            dataToReset[1],
            dataToReset[2],
            dataToReset[3],
            dataToReset[4]
        ];
        return newPoint;
    };
    return OHLCGenerator;
}(DataGenerator));

function createDeltaFunctionGenerator() {
    return new DeltaFunctionGenerator({
        numberOfPoints: 1000,
        minGap: 1,
        maxGap: -1,
        minAmplitude: 0.1,
        maxAmplitude: 1,
        probability: 0.02
    });
}
var DeltaFunctionGenerator = (function (_super) {
    __extends(DeltaFunctionGenerator, _super);
    function DeltaFunctionGenerator(args) {
        var _this = _super.call(this, args) || this;
        _this.lastSpike = 0;
        var opts = {
            numberOfPoints: args.numberOfPoints,
            minGap: args.minGap,
            maxGap: args.maxGap,
            minAmplitude: args.minAmplitude,
            maxAmplitude: args.maxAmplitude,
            probability: args.probability
        };
        _this.options = Object.freeze(opts);
        return _this;
    }
    DeltaFunctionGenerator.prototype.setNumberOfPoints = function (numberOfPoints) {
        return new DeltaFunctionGenerator(__assign(__assign({}, this.options), { numberOfPoints: numberOfPoints }));
    };
    DeltaFunctionGenerator.prototype.setMinGap = function (minGap) {
        return new DeltaFunctionGenerator(__assign(__assign({}, this.options), { minGap: minGap }));
    };
    DeltaFunctionGenerator.prototype.setMaxGap = function (maxGap) {
        return new DeltaFunctionGenerator(__assign(__assign({}, this.options), { maxGap: maxGap }));
    };
    DeltaFunctionGenerator.prototype.setMinAmplitude = function (minAmplitude) {
        return new DeltaFunctionGenerator(__assign(__assign({}, this.options), { minAmplitude: minAmplitude }));
    };
    DeltaFunctionGenerator.prototype.setMaxAmplitude = function (maxAmplitude) {
        return new DeltaFunctionGenerator(__assign(__assign({}, this.options), { maxAmplitude: maxAmplitude }));
    };
    DeltaFunctionGenerator.prototype.setProbability = function (probability) {
        return new DeltaFunctionGenerator(__assign(__assign({}, this.options), { probability: probability }));
    };
    DeltaFunctionGenerator.prototype.getPointCount = function () {
        return this.options.numberOfPoints;
    };
    DeltaFunctionGenerator.prototype.generateDataPoint = function (i) {
        var sinceLast = i - this.lastSpike;
        var value = { x: i, y: 0 };
        if (sinceLast > this.options.minGap || this.options.minGap === -1) {
            if (sinceLast < this.options.maxGap || this.options.maxGap === -1) {
                var doSpike = Math.random() > (1 - this.options.probability);
                if (doSpike) {
                    value.y = Math.random() * (this.options.maxAmplitude - this.options.minAmplitude) + this.options.minAmplitude;
                    this.lastSpike = i;
                }
            }
            else if (sinceLast >= this.options.maxGap) {
                value.y = Math.random() * (this.options.maxAmplitude - this.options.minAmplitude) + this.options.minAmplitude;
                this.lastSpike = i;
            }
        }
        return value;
    };
    DeltaFunctionGenerator.prototype.infiniteReset = function (dataToReset, data) {
        return { x: dataToReset.x + data.length, y: dataToReset.y };
    };
    return DeltaFunctionGenerator;
}(DataGenerator));

function createWhiteNoiseGenerator() {
    return new WhiteNoiseGenerator({
        numberOfPoints: 1000
    });
}
var WhiteNoiseGenerator = (function (_super) {
    __extends(WhiteNoiseGenerator, _super);
    function WhiteNoiseGenerator(args) {
        var _this = _super.call(this, args) || this;
        var opts = {
            numberOfPoints: args.numberOfPoints
        };
        _this.options = Object.freeze(opts);
        return _this;
    }
    WhiteNoiseGenerator.prototype.setNumberOfPoints = function (numberOfPoints) {
        return new WhiteNoiseGenerator(this.options ? __assign(__assign({}, this.options), { numberOfPoints: numberOfPoints }) : { numberOfPoints: numberOfPoints });
    };
    WhiteNoiseGenerator.prototype.getPointCount = function () {
        return this.options.numberOfPoints;
    };
    WhiteNoiseGenerator.prototype.generateDataPoint = function (i) {
        var point = {
            x: i,
            y: (Math.random() - 0.5) * 2
        };
        return point;
    };
    WhiteNoiseGenerator.prototype.infiniteReset = function (dataToReset, data) {
        return { x: dataToReset.x + data[data.length - 1].x, y: dataToReset.y + data[data.length - 1].y };
    };
    return WhiteNoiseGenerator;
}(DataGenerator));

function createSampledDataGenerator() {
    return new SampledDataGenerator({
        inputData: [],
        samplingFrequency: 50,
        step: 0
    });
}
var SampledDataGenerator = (function (_super) {
    __extends(SampledDataGenerator, _super);
    function SampledDataGenerator(args) {
        var _this = _super.call(this, args) || this;
        _this.interval = 1 / (_this.options.samplingFrequency || 10);
        var opts = {
            inputData: args.inputData,
            samplingFrequency: args.samplingFrequency,
            step: args.step
        };
        _this.options = Object.freeze(opts);
        return _this;
    }
    SampledDataGenerator.prototype.setInputData = function (inputData) {
        return new SampledDataGenerator(__assign(__assign({}, this.options), { inputData: inputData }));
    };
    SampledDataGenerator.prototype.setSamplingFrequency = function (samplingFrequency) {
        return new SampledDataGenerator(__assign(__assign({}, this.options), { samplingFrequency: samplingFrequency }));
    };
    SampledDataGenerator.prototype.setStep = function (step) {
        return new SampledDataGenerator(__assign(__assign({}, this.options), { step: step }));
    };
    SampledDataGenerator.prototype.getPointCount = function () {
        return this.options.inputData.length;
    };
    SampledDataGenerator.prototype.generateDataPoint = function (i) {
        var point = {
            timestamp: i * this.interval + i * this.options.step,
            data: this.options.inputData[i]
        };
        return point;
    };
    SampledDataGenerator.prototype.infiniteReset = function (dataToReset, data) {
        return { timestamp: dataToReset.timestamp + data[data.length - 1].timestamp, data: dataToReset.data };
    };
    return SampledDataGenerator;
}(DataGenerator));

function createParametricFunctionGenerator() {
    return new ParametricFunctionGenerator({
        xFunction: function (t) { return 3 * Math.cos(3 * t); },
        yFunction: function (t) { return 3 * Math.sin(4 * t); },
        start: 0,
        end: 1000,
        step: 0.5
    });
}
var ParametricFunctionGenerator = (function (_super) {
    __extends(ParametricFunctionGenerator, _super);
    function ParametricFunctionGenerator(args) {
        var _this = _super.call(this, args) || this;
        _this.t = _this.options.start;
        var opts = {
            xFunction: args.xFunction,
            yFunction: args.yFunction,
            start: args.start,
            end: args.end,
            step: args.step
        };
        _this.options = Object.freeze(opts);
        _this.numberOfPoints = Math.ceil(Math.abs(opts.end - opts.start) / opts.step);
        return _this;
    }
    ParametricFunctionGenerator.prototype.setXFunction = function (handler) {
        return new ParametricFunctionGenerator(__assign(__assign({}, this.options), { xFunction: handler }));
    };
    ParametricFunctionGenerator.prototype.setYFunction = function (handler) {
        return new ParametricFunctionGenerator(__assign(__assign({}, this.options), { yFunction: handler }));
    };
    ParametricFunctionGenerator.prototype.setStart = function (start) {
        return new ParametricFunctionGenerator(__assign(__assign({}, this.options), { start: start }));
    };
    ParametricFunctionGenerator.prototype.setEnd = function (end) {
        return new ParametricFunctionGenerator(__assign(__assign({}, this.options), { end: end }));
    };
    ParametricFunctionGenerator.prototype.setStep = function (step) {
        return new ParametricFunctionGenerator(__assign(__assign({}, this.options), { step: step }));
    };
    ParametricFunctionGenerator.prototype.getPointCount = function () {
        return this.numberOfPoints;
    };
    ParametricFunctionGenerator.prototype.generateDataPoint = function () {
        var point = {
            x: this.options.xFunction(this.t),
            y: this.options.yFunction(this.t)
        };
        this.t = this.t + this.options.step;
        return point;
    };
    ParametricFunctionGenerator.prototype.infiniteReset = function (dataToReset, data) {
        return { x: dataToReset.x, y: dataToReset.y };
    };
    return ParametricFunctionGenerator;
}(DataGenerator));

exports.DataGenerator = DataGenerator;
exports.DataHost = DataHost;
exports.Stream = Stream;
exports.createDeltaFunctionGenerator = createDeltaFunctionGenerator;
exports.createOHLCGenerator = createOHLCGenerator;
exports.createParametricFunctionGenerator = createParametricFunctionGenerator;
exports.createProgressiveFunctionGenerator = createProgressiveFunctionGenerator;
exports.createProgressiveRandomGenerator = createProgressiveRandomGenerator;
exports.createProgressiveTraceGenerator = createProgressiveTraceGenerator;
exports.createSampledDataGenerator = createSampledDataGenerator;
exports.createTraceGenerator = createTraceGenerator;
exports.createWhiteNoiseGenerator = createWhiteNoiseGenerator;
//# sourceMappingURL=xydata.js.map
