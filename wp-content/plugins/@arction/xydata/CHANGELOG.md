# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - yyyy-mm-dd
### Added

### Changed

### Deprecated

### Fixed

### Security

## [1.1.0] - 2019-01-31
### Added

- Parametric Function Generator

### Changed

- Upgraded dependencies to latest released versions
- Migrated from rollup-plugin-typescript2 to @wessberg/rollup-plugin-ts
- Updated documentation using latest TypeDoc generator

## [1.0.1] - 2019-08-05
### Added
- Changelog

### Changed
- Upgraded dependencies to latest released versions.
- Updated documentation using latest TypeDoc generator.

### Fixed
- Typo in README.md

## [1.0.0] - 2019-07-04
### Added
- Progressive Random Generator.
- Progressive Function Generator.
- Progressive Trace Generator.
- Delta Function Generator.
- OHLC Generator.
- Sampled Data Generator.
- White Noise Generator.
- Trace Generator.
