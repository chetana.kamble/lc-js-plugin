/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

/**
 * Eventer error message, adds 'EventError:' prefix to error message
 */
var EventError = /** @class */ (function (_super) {
    __extends(EventError, _super);
    function EventError(m) {
        var _this = this;
        var errorMessage = "EventError: " + m + "}";
        _this = _super.call(this, errorMessage) || this;
        // Stupid workaround for a smooth support of ES5
        Object.setPrototypeOf(_this, EventError.prototype);
        return _this;
    }
    return EventError;
}(Error));
/**
 * @param ownerIndex Index of Eventer
 * @param id         Index of current subscription
 * @return           Unique Token
 */
var Token = function (ownerIndex, id) { return ({
    token: ownerIndex + "-" + id,
    ownerIndex: ownerIndex,
    id: id
}); };
// Every eventer has to have unique id across an app
var lastId = 0;
/**
 * Creates Error for non-existing event listener.
 * The Error message is used in multiple occasions.
 * @param token Token of non-existing event
 * @param id    Eventer id
 */
var notExistReport = function (token, id) {
    return new EventError("Event listener with " + token.token + " id does not exist at Eventer with " + id + " id.");
};
/**
 * Pub/Sub for internal event scheduling.
 * All functions are for protected usage.
 * Sub class can use them for implementation of event dispaching.
 * @property  id  Unique Eventer id
 */
var Eventer = /** @class */ (function () {
    /**
     *
     */
    function Eventer() {
        /**
         * Unique id
         */
        this.id = ++lastId;
        /**
         * Index of last issued token
         */
        this.lastEventIndex = 0;
        /**
         * Collection of topics
         */
        this.topics = new Map();
    }
    /**
     * Register subscription on the topic with provided listener
     * @param topic     Topic name
     * @param listener  Event listener
     * @param oldToken  Old token, if it has to be reused
     */
    Eventer.prototype.on = function (topic, listener, oldToken) {
        var listeners = this.topics.get(topic);
        // use old token or create unique one
        var token = oldToken ? oldToken : Token(this.id, ++this.lastEventIndex);
        if (listeners)
            //add listener to the topic
            listeners.set(token, listener);
        else
            //create new topic with the listener
            this.topics.set(topic, new Map([[token, listener]]));
        return token;
    };
    /**
     * Check if listener with provided token exists
     * @param token Token of desired listener
     * @param topic Topic which has to contain the listener
     * @return      Error with message or Listener
     */
    Eventer.prototype.has = function (token, topic) {
        var e_1, _a;
        //if topic is specified
        if (topic) {
            // request all listeners subscribed to the topic
            var listeners = this.topics.get(topic);
            if (listeners) {
                // request listener with the token
                var listener = listeners.get(token);
                if (listener)
                    // return the listener if it is defined
                    return listener;
                else
                    // return error if it is not defined
                    return notExistReport(token, this.id);
            }
            else
                // return error if topic is empty
                return new EventError("Eventer with " + token.ownerIndex + " does not have " + topic + ".");
        }
        else {
            try {
                //iterate over all topics
                for (var _b = __values(this.topics), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var _d = __read(_c.value, 2), _ = _d[0], listeners = _d[1];
                    //check if it has listener with desired token
                    var listener = listeners.get(token);
                    if (listener)
                        //return the listener
                        return listener;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            // return an error with none of topics contains listeners with desired token.
            return notExistReport(token, this.id);
        }
    };
    /**
     * Remove listener with provided token
     * @param token Token of the listener
     * @param topic Topic which has to contain the listener
     * @return      True if the listener is successfully removed and false if it is not found
     */
    Eventer.prototype.off = function (token, topic) {
        var e_2, _a;
        //if topic is specified
        if (topic) {
            // request all listeners subscribed to the topic
            var listeners = this.topics.get(topic);
            if (listeners)
                // check listener with the token existance
                if (listeners.delete(token))
                    return true;
            // return false with the topic does not contain listener with specified token
            return false;
        }
        else {
            try {
                //itarate over all topics
                for (var _b = __values(this.topics), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var _d = __read(_c.value, 2), _ = _d[0], listeners = _d[1];
                    // check listener with the token existance
                    if (listeners.delete(token))
                        return true;
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_2) throw e_2.error; }
            }
            // return false with all topics do not contain listener with specified token
            return false;
        }
    };
    /**
     * Remove all topics with all listeners
     */
    Eventer.prototype.allOff = function () {
        this.topics.clear();
        return this;
    };
    /**
     * Remove topic with all listeners
     * @param topic Topic name
     * @return      True if the topic is successfully removed and false if it is not found
     */
    Eventer.prototype.topicOff = function (topic) {
        return this.topics.delete(topic);
    };
    /**
     * Emit event and call subscribed listeners
     * @param   topic Topic name
     * @param   args  Array of arguments
     * @return        Number of listeners called
     */
    Eventer.prototype.emit = function (topic) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        // request all listeners subscribed to the topic
        var listeners = this.topics.get(topic);
        var numberOfListeners = 0;
        // if listerens exist
        if (listeners)
            // iterate over
            listeners.forEach(function (listener) {
                // call listener
                listener.apply(void 0, __spread(args));
                numberOfListeners++;
            });
        return numberOfListeners;
    };
    /**
     * Get All listeners at the topic
     * @param topic Topic name
     */
    Eventer.prototype.listeners = function (topic) {
        var listeners = this.topics.get(topic);
        if (listeners)
            return listeners;
        else
            return new Map();
    };
    return Eventer;
}());

export { EventError, Eventer, Token };
//# sourceMappingURL=eventer.es.js.map
