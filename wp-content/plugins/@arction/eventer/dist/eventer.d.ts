/**
 * Eventer error message, adds 'EventError:' prefix to error message
 */
export declare class EventError extends Error {
    constructor(m: string);
}
/**
 * Event Listener interface
 */
export declare type Listener = (...args: any[]) => void;
/**
 * Subscription representation
 */
export interface Token {
    /**
     * Unique token ID
     */
    readonly token: string;
    /**
     * ID of Eventer issued the token
     */
    readonly ownerIndex: number;
    /**
     * Internal token ID, might be in collision with another Eventer instance
     */
    readonly id: number;
}
/**
 * @param ownerIndex Index of Eventer
 * @param id         Index of current subscription
 * @return           Unique Token
 */
export declare const Token: (ownerIndex: number, id: number) => Token;
/**
 * Pub/Sub for internal event scheduling.
 * All functions are for protected usage.
 * Sub class can use them for implementation of event dispaching.
 * @property  id  Unique Eventer id
 */
export declare class Eventer {
    /**
     * Unique id
     */
    readonly id: number;
    /**
     * Index of last issued token
     */
    private lastEventIndex;
    /**
     * Collection of topics
     */
    private readonly topics;
    /**
     *
     */
    constructor();
    /**
     * Register subscription on the topic with provided listener
     * @param topic     Topic name
     * @param listener  Event listener
     * @param oldToken  Old token, if it has to be reused
     */
    on(topic: string, listener: Listener, oldToken?: Token): Token;
    /**
     * Check if listener with provided token exists
     * @param token Token of desired listener
     * @param topic Topic which has to contain the listener
     * @return      Error with message or Listener
     */
    has(token: Token, topic?: string): EventError | Listener;
    /**
     * Remove listener with provided token
     * @param token Token of the listener
     * @param topic Topic which has to contain the listener
     * @return      True if the listener is successfully removed and false if it is not found
     */
    off(token: Token, topic?: string): boolean;
    /**
     * Remove all topics with all listeners
     */
    allOff(): this;
    /**
     * Remove topic with all listeners
     * @param topic Topic name
     * @return      True if the topic is successfully removed and false if it is not found
     */
    topicOff(topic: string): boolean;
    /**
     * Emit event and call subscribed listeners
     * @param   topic Topic name
     * @param   args  Array of arguments
     * @return        Number of listeners called
     */
    emit(topic: string, ...args: Array<any>): number;
    /**
     * Get All listeners at the topic
     * @param topic Topic name
     */
    listeners(topic: string): Map<Token, Listener>;
}
